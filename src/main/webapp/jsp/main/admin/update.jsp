<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@ taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="sustainerCheckbox" prefix="sustainer"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>Insert title here</title>
<script language="Javascript">
	<!--
	function Click_Sub2() {
	    if (document.all.chk2.checked==false) {
	        document.all.c1.disabled=false
	        document.all.c2.disabled=false
	        document.all.c3.disabled=false
	    } else {
	        document.all.c1.disabled=true
	        document.all.c2.disabled=true
	        document.all.c3.disabled=true
	    }
	}
	// -->
	</script>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>
<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">社員更新</div>
		</td>
		<td>${requestScope.success}</td>
	</tr>
</table>
<form action="admin_update.do">
<table border="0">
	<tr>
		<td>社員ID</td>
		<td>
		<div align="center"><strong>${requestScope.beanMaster.empCode}${param.empCode}</strong></div>
		</td>
	</tr>
	<tr>
		<td>社員名</td>
		<td><input type="text" size="24" name="empName"
			value="${requestScope.beanMaster.empName}${param.empName}"}></td>
		<td><font size="2" color="red"><em:em key="empName" /></font></td>
	</tr>
	<tr>
		<td>権限コード</td>
		<td><sustainer:checkbox categorys="管理者,受注担当者,出荷担当者,経理担当者"
			categoryNames="c0,c1,c2,c3" categoryValues="15,8,4,2" id="chk2"
			method="Click_Sub2()" /></td>
		<td><font size="2" color="red">${requestScope.errorMessage}</font></td>
	</tr>
	<tr>
		<td><input type="hidden" name="empCode"
			value="${requestScope.beanMaster.empCode}${param.empCode}"></td>
		<td><input type="hidden" name="send" value="true"></td>
	</tr>
</table>
<table>
	<tr>
		<td width="215">
		<div align="right"><input type="submit" value="送信" /></div>
		</td>
	</tr>
</table>
</form>
</body>
</html>