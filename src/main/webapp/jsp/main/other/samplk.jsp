<%@ page language="java" contentType="text/html; charset=windows-31j"
    pageEncoding="windows-31j"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE></TITLE>
<SCRIPT language="JavaScript">
<!--
function fmTurn(){
  trn = new Array("name","email","addr","comment");
  for(i=0; i<trn.length; i++) {
    if(document.nForm.elements[trn[i]].value != "") {
      document.nForm.elements[trn[i+1]].disabled = false;
    } else {
      document.nForm.elements[trn[i+1]].disabled = true;
    }
  }
}
//-->
</SCRIPT>
</HEAD>
<BODY>
「氏名 → Mail → 住所 → コメント」の順番でしか入力できません。
<FORM name="nForm">
<!-- 最初に入力するもの以外には、「disabled」をつける -->
氏名：<INPUT type="text" name="name" onChange="fmTurn()"><BR>
Mail：<INPUT type="text" name="email" onChange="fmTurn()" disabled><BR>
住所：<INPUT type="text" name="addr" onChange="fmTurn()" disabled><BR>
コメント<BR>
<!-- 最後に入力するものには、「onChange="fmTurn()"」は不要 -->
<TEXTAREA rows="3" size="45" name="comment" disabled></TEXTAREA>
</FORM>
</BODY>
</HTML>