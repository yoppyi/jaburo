<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>Insert title here</title>
<SCRIPT language="JavaScript">
<!--
// 数値のみを入力可能にする
function numOnly() {
code = event.keyCode ;
m = String.fromCharCode( code );

if(code == 46 || code == 37 || code == 39 ) return true ; // del ← →

if(!event.shiftKey) {
if("0123456789\d\b\r\t".indexOf(m, 0) < 0) return false;
else return true;
} else {
if("\t".indexOf(m, 0) >= 0) return true;
else return false ;
}
}
//-->
</SCRIPT>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>

<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">倉庫別在庫検索</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table>
<form action="searchWith.do">
<table border="0">
	<tr>
		<td>商品コード</td>
		<td><input type="text" maxlength="5" name="goodsCode" value=""><em:em
			key="goodsCode" /></td>
	</tr>
	<tr>
		<td width="190" colspan="2"><input type="hidden" name="empCode"
			value="${requestScope.beanMaster.empCode}${param.empCode}"><font
			size="2" color="red">${requestScope.errorMessage}</font>
		<div align="right"><input type="submit" value="送信" /></div>
		</td>
	</tr>
</table>
<br>
<br>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table border="0">
	<tr height="44pix">
		<td width="180pix">
		<div align="center">倉庫別在庫更新</div>
		</td>
	</tr>
</table>
</form>
<form action="searchWith.do">
<table border="0">
	<tr>
		<td>倉庫コード</td>
		<td><input type="text" maxlength="5" name="wareHouseCode"></td>
	</tr>
	<tr>
		<td>期首在庫数</td>
		<td><input type="text" name="numberOfPeriodNeckStocks"
			value="<%request.getParameter("numberOfPeriodNeckStocks"); %>"
			maxlength="5" onkeyDown="return numOnly()"> <em:em key="" />
		</td>
	</tr>
	<tr>
		<td>現在庫数</td>
		<td><input type="text" name="presentInventoryGoods"
			value="<%request.getParameter("presentInventoryGoods"); %>"
			maxlength="5" onkeyDown="return numOnly()"> <em:em key="" />
		</td>
	</tr>
	<tr>
		<td>引当済数</td>
		<td><input type="text" name="numberThatHasBeenDrawn"
			value="<%request.getParameter("numberThatHasBeenDrawn"); %>"
			maxlength="5" onkeyDown="return numOnly()"> <em:em key="" />
		</td>
	</tr>
	<!--
			loop:llct 
			tableList="List" inclusions="goodsCode,wareHouseCode,numberOfPeriodNeckStocks,
			presentInventoryGoods,numberThatHasBeenDrawn"  />
			-->
</table>
<table>
	<tr>
		<td width="215"><input type="hidden" name="empCode"
			value="${requestScope.beanMaster.empCode}${param.empCode}"> <font
			size="2" color="red">${requestScope.errorMessage}</font>
		<div align="right"><input type="submit" value="送信" /></div>
		</td>
	</tr>
</table>
</form>
</body>
</html>