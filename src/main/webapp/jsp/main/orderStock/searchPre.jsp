<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>在庫推移一覧</title>
<SCRIPT language="JavaScript">
	<!-- // 数値のみを入力可能にする
		function numOnly() {
			code = event.keyCode ;
			m = String.fromCharCode( code );
	
			if(code == 46 || code == 37 || code == 39 ) return true ; // del ← →
	
			if(!event.shiftKey) {
				if("0123456789\d\b\r\t".indexOf(m, 0) < 0) return false;
				else return true;
			} else {
				if("\t".indexOf(m, 0) >= 0) return true;
				else return false ;
			}
		}
	//-->
		</SCRIPT>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>
<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">在庫推移一覧</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table>
<form action="orderStock_searchPre.do">
<table border="0">
	<tr>
		<td>受注No</td>
		<td><input type="text" name="primary" maxlength="5" value="">
		<em:em key="OrdIndexNo" /></td>
		<td width="190" colspan="2"><input type="hidden" name="send"
			value="true"> <input type="hidden" name=""
			value="${requestScope.beanMaster.empCode}${param.empCode}"> <font
			size="2" color="red">${requestScope.errorMessage}</font></td>
	</tr>
	<tr>
		<td>
		<input type="submit" value="検索">
		</td>
	</tr>
</table>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table border="0">
	<tr height="44pix">
		<td width="180pix">
		<div align="center">検索結果</div>
		</td>
	</tr>
</table>
</form>
<table border="1">
	<tr>
		<td>受注No</td>
		<td>商品名</td>
		<td>受注数</td>
		<td>引当可能数</td>
		<td>希望納期</td>
		<loop:llct tableList="stockSearcherList"
			inclusions="ordIndexNo,goodsCode,ordNumberOfOrder,numberThatHasBeenDrawn,ordHopeDeliveryDate" />

	</tr>
	<tr>

	</tr>
</table>
</body>
</html>