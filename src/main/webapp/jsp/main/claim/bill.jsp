<%@ page language="java" contentType="text/html; charset=windows-31j" pageEncoding="windows-31j"%>
<%@ taglib uri="date" prefix="date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<html xmlns:vCard="http://www.w3.org/2001/vcard-rdf/3.0#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<head>
<META http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<link href="skin.css" type="text/css" rel="StyleSheet">
<title>請求書発行</title>

<STYLE type="text/css">
<!--
.break { page-break-before: always; }
-->
</STYLE>


</head>
<body>


<% 
//請求先企業の情報を取得
	String denpyo = request.getParameter("denpyou");
	String date = request.getParameter("date");
	String ccode = request.getParameter("ccode");
	String cadd = request.getParameter("cadd");
	String ctel = request.getParameter("ctel");
	String cname = request.getParameter("cname");
	try {
		   denpyo = new String(denpyo.getBytes("iso-8859-1"), "Shift_JIS");
		   ccode = new String(ccode.getBytes("iso-8859-1"), "Shift_JIS");
		   cadd = new String(cadd.getBytes("iso-8859-1"), "Shift_JIS");
		   cname = new String(cname.getBytes("iso-8859-1"), "Shift_JIS");
	    } catch (UnsupportedEncodingException e) {
	    	
	    }


//テーブル情報を取得	
	String a1 = request.getParameter("11");
	String ai2 = request.getParameter("12");
	String ai3 = request.getParameter("13");
	Integer a2 = Integer.parseInt(ai2);
	Integer a3 = Integer.parseInt(ai3);
	Integer a4 = a2 * a3;
	String a5 = request.getParameter("15");
	
	String b1 = request.getParameter("21");
	String bi2 = request.getParameter("22");
	String bi3 = request.getParameter("23");
	Integer b2 = Integer.parseInt(bi2);
	Integer b3 = Integer.parseInt(bi3);
	Integer b4 = b2 * b3;
	String b5 = request.getParameter("25");
	
	String c1 = request.getParameter("31");
	String ci2 = request.getParameter("32");
	String ci3 = request.getParameter("33");
	Integer c2 = Integer.parseInt(ci2);
	Integer c3 = Integer.parseInt(ci3);
	Integer c4 = c2 * c3;
	String c5 = request.getParameter("35");
	
	String d1 = request.getParameter("41");
	String di2 = request.getParameter("42");
	String di3 = request.getParameter("43");
	Integer d2 = Integer.parseInt(di2);
	Integer d3 = Integer.parseInt(di3);
	Integer d4 = d2 * d3;
	String d5 = request.getParameter("45");
	
	String e1 = request.getParameter("51");
	String ei2 = request.getParameter("52");
	String ei3 = request.getParameter("53");
	Integer e2 = Integer.parseInt(ei2);
	Integer e3 = Integer.parseInt(ei3);
	Integer e4 = e2 * e3;
	String e5 = request.getParameter("55");
	
	String f1 = request.getParameter("61");
	String fi2 = request.getParameter("62");
	String fi3 = request.getParameter("63");
	Integer f2 = Integer.parseInt(fi2);
	Integer f3 = Integer.parseInt(fi3);
	Integer f4 = f2 * f3;
	String f5 = request.getParameter("65");
	
	String g1 = request.getParameter("71");
	String gi2 = request.getParameter("72");
	String gi3 = request.getParameter("73");
	Integer g2 = Integer.parseInt(gi2);
	Integer g3 = Integer.parseInt(gi3);
	Integer g4 = e2 * e3;
	String g5 = request.getParameter("75");
	
	String h1 = request.getParameter("81");
	String hi2 = request.getParameter("82");
	String hi3 = request.getParameter("83");
	Integer h2 = Integer.parseInt(hi2);
	Integer h3 = Integer.parseInt(hi3);
	Integer h4 = h2 * h3;
	String h5 = request.getParameter("85");
	
	String i1 = request.getParameter("91");
	String ii2 = request.getParameter("92");
	String ii3 = request.getParameter("93");
	Integer i2 = Integer.parseInt(ii2);
	Integer i3 = Integer.parseInt(ii3);
	Integer i4 = i2 * i3;
	String i5 = request.getParameter("95");
	

	Integer result = a4 + b4 + c4 + d4 + e4 + f4 + g4 + h4 + i4;
	
	NumberFormat f = NumberFormat.getCurrencyInstance(Locale.JAPAN);
	try {
		   a1 = new String(a1.getBytes("iso-8859-1"), "Shift_JIS");
		   b1 = new String(b1.getBytes("iso-8859-1"), "Shift_JIS");
		   c1 = new String(c1.getBytes("iso-8859-1"), "Shift_JIS");
		   d1 = new String(d1.getBytes("iso-8859-1"), "Shift_JIS");
		   e1 = new String(e1.getBytes("iso-8859-1"), "Shift_JIS");
		   f1 = new String(f1.getBytes("iso-8859-1"), "Shift_JIS");
		   g1 = new String(g1.getBytes("iso-8859-1"), "Shift_JIS");
		   h1 = new String(h1.getBytes("iso-8859-1"), "Shift_JIS");
		   i1 = new String(i1.getBytes("iso-8859-1"), "Shift_JIS");
	    } catch (UnsupportedEncodingException e) {
	    	
	    }
	
%>
<DIV class="break">
<div style="font-size: 23pt; text-align: center; margin-bottom: 1em;">御請求書</div>
<div style="position: absolute; right: 8pt; width: 140px">


<!-- 伝票番号 -->
<div>No.<span style="text-decoration: underline;"><%= denpyo %></span>
<!-- 本日の日付-->
</div><div><%= date %></div></div>


<div>
<!-- 請求先企業コード -->
<div>企業コード：<%= ccode%></div>

<!--  請求先企業住所-->
<div><%= cadd%></div>
<div>TEL :<%= ctel%></div>
<div style="margin-top: 0.5em; font-size: 14pt; font-weight: bold;">

<!--  請求先企業名-->
<span style="text-decoration: underline;"><%= cname %></span>御中</div></div>




<!--  自社の内容の入力欄-->
<div style="position: absolute; right: 0; height: 699px; width: 398px">
<div>〒 532-0011</div>
<div>	大阪府大阪市淀川区西中島4丁目13番22号</div>
<div> TEL:06-4806-3470</div>
<div style="text-align: left;">□株式会社　ジャブロ<br>関西第1営業部 　　ロッキー江島ま〜と</div></div>
<!-- 自社内容はここまで -->

<!-- 契約金内容　 -->
<div style="margin-top: 1em;">
<span style="text-decoration: underline; font-size: 18pt;"><%= f.format(result) %>円</span>(税込み)</div>
<div style="margin-top: 4em;">下記のとおり御請求いたします。</div>






<!--  ここからテーブル内容へ突入 -->
<table border="2" style="width: 100%;">
<tr>
	<th></th>
	<th align="center" valign="middle">商品名</th>
	<th align="center" valign="middle">単価</th>
	<th align="center" valign="middle">数量</th>
	<th align="center" valign="middle">金額</th>
	<th align="center" valign="middle">備考</th>
</tr>
<tr>
	<td>1</td><td><%= a1 %></td><td align="right" ><%= f.format(a2) %></td><td><%= a3 %></td><td align="right"><%= f.format(a4) %></td><td><%= a5 %></td>
</tr>
<tr>
	<td>2</td><td><%= b1 %></td><td align="right"><%= f.format(b2) %></td><td><%= b3 %></td><td align="right"><%= f.format(b4) %></td><td><%= b5 %></td>
</tr>
<tr>
	<td>3</td><td><%= c1 %></td><td align="right"><%= f.format(c2) %></td><td><%= c3 %></td><td align="right"><%= f.format(c4) %></td><td><%= c5 %></td>
</tr>
<tr>
	<td>4</td><td><%= d1 %></td><td align="right"><%= f.format(d2) %></td><td><%= d3 %></td><td align="right"><%= f.format(d4) %></td><td><%= d5 %></td>
</tr>
<tr>
	<td>5</td><td><%= e1 %></td><td align="right"><%= f.format(e2) %></td><td><%= e3 %></td><td align="right"><%= f.format(e4) %></td><td><%= e5 %></td>
</tr>
<tr>
	<td>6</td><td><%= f1 %></td><td align="right"><%= f.format(f2) %></td><td><%= f3 %></td><td align="right"><%= f.format(f4) %></td><td><%= f5 %></td>
</tr>
<tr>
	<td>7</td><td><%= g1 %></td><td align="right"><%= f.format(g2) %></td><td><%= g3 %></td><td align="right"><%= f.format(g4) %></td><td><%= g5 %></td>
</tr>
<tr>
	<td>8</td><td><%= h1 %></td><td align="right"><%= f.format(h2) %></td><td><%= h3 %></td><td align="right"><%= f.format(h4) %></td><td><%= h5 %></td>
</tr>
<tr>
	<td>9</td><td><%= i1 %></td><td align="right"><%= f.format(i2) %></td><td><%= i3 %></td><td align="right"><%= f.format(i4) %></td><td><%= i5 %></td>
</tr>
</table>

<div style="margin-top: 2em; font-size: 12pt;">振込先</div>
<table>
<tr>
<td>
<table　border="2">
<tr>
	<th>金融機関</th>
	<th>支店名</th>
	<th>口座種別</th>
	<th>口座番号</th>
	<th>名義人</th>
</tr>
<tr>
	<td>江島銀行</td>
	<td>沖縄支店</td>
	<td>普通</td>
	<td>9999999</td>
	<td>株式会社Jaburo</td>
</tr>
</table>
</td>
</tr>
</table>
</DIV>
</body>
</html>
