<%@ page language="java" contentType="text/html; charset=windows-31j" pageEncoding="windows-31j"%>
<%@ taglib uri="date" prefix="date"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:vCard="http://www.w3.org/2001/vcard-rdf/3.0#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<head>
<META http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<link href="skin.css" type="text/css" rel="StyleSheet">
<title>請求書発行</title>
</head>
<% 
//請求先企業の情報を取得
	String denpyo = request.getParameter("denpyou");
	String date = request.getParameter("date");
	String ccode = request.getParameter("ccode");
	String cadd = request.getParameter("cadd");
	String ctel = request.getParameter("ctel");
	String cname = request.getParameter("cname");
	String result = request.getParameter("result");
%>
<body>

<div style="font-size: 23pt; text-align: center; margin-bottom: 1em;">御請求書</div>
<div style="position: absolute; right: 8pt; width: 140px">


<!-- 伝票番号 -->
<div>No.<span style="text-decoration: underline;"><%= denpyo %></span>
<!-- 本日の日付-->
</div><div><%= date %></div></div>


<div>
<!-- 請求先企業コード -->
<div>企業コード：<%= ccode %></div>

<!--  請求先企業住所-->
<div><%= cadd %></div>
<div>TEL :<%= ctel %></div>
<div style="margin-top: 0.5em; font-size: 14pt; font-weight: bold;">

<!--  請求先企業名-->
<span style="text-decoration: underline;"><%= cname %></span>御中</div></div>




<!--  自社の内容の入力欄-->
<div style="position: absolute; right: 0; height: 699px; width: 398px">
<div>〒 532-0011</div>
<div>	大阪府大阪市淀川区西中島4丁目13番22号</div>
<div> TEL:06-4806-3470</div>
<div style="text-align: left;">□株式会社　ジャブロ<br>関西第1営業部 　　ロッキー江島ま〜と</div></div>
<!-- 自社内容はここまで -->

<!-- 契約金内容　 -->
<div style="margin-top: 1em;">
<span style="text-decoration: underline; font-size: 18pt;"><%= result %>円</span>(税込み)</div>
<div style="margin-top: 4em;">下記のとおり御請求いたします。</div>






<!--  ここからテーブル内容へ突入 -->
<table border="2" style="width: 100%;">
<tr>
	<th></th>
	<th align="center" valign="middle">商品名</th>
	<th align="center" valign="middle">単価</th>
	<th align="center" valign="middle">数量</th>
	<th align="center" valign="middle">金額</th>
	<th align="center" valign="middle">備考</th>
</tr>
<c:forEach var="i" begin="1" end="9" >

<tr>
	<td>${i}</td>
	<td>${param.pname}</td>
	<td>${paramValues[tanka]}</td>
	<td>${paramValues[suuryou]}</td>
	<td>${paramValues[kingaku]}</td>
	<td>${paramValues[biko]}</td>
</tr>
</c:forEach>
</table>

<div style="margin-top: 2em; font-size: 12pt;">振込先</div>
<table>
<tr>
<td>
<table　border="2">
<tr>
	<th>金融機関</th>
	<th>支店名</th>
	<th>口座種別</th>
	<th>口座番号</th>
	<th>名義人</th>
</tr>
<tr>
	<td>江島銀行</td>
	<td>沖縄支店</td>
	<td>普通</td>
	<td>9999999</td>
	<td>株式会社Jaburo</td>
</tr>
</table>