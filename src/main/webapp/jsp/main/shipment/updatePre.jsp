<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@taglib uri="loopListCreateTable" prefix="loop"%>
<%@taglib uri="sustainerSupport" prefix="sustainer"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<%@ taglib uri="date" prefix="date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>出荷実績報告</title>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>
<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">出荷明細検索</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table>
<form name="calf1" action="shipment_updatePre.do">
<table border="0">
	<tr>
		<td>出荷No</td>
		<td><input type="text" name="ordIndexNo" value="">
	</tr>
</table>
<input type="submit" value="検索"></form>
<form name="calf1" action="shipment_updatePre.do">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">出荷明細登録</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table><table>
	<tr>
		<td>受注No</td>
		<td>${param.ordIndexNo}</td>
	</tr>
	<tr>
		<td>出荷実績数量</td>
		<td><input type="text" name="shipNumberOfAmount" value=""
			maxlength="8" onkeyDown="return numOnly()"> <em:em key="" />
		</td>
	</tr>
	<tr>
		<td>出荷実績倉庫コード</td>
		<td><input type="text" maxlength="5"
			name="shipNumberOfWarHouseCode" value=""></td>
	</tr>
	<tr>
		<td>売り上げソースNo</td>
		<td><input type="text" maxlength="5" name="salseSourceNo"
			value=""></td>
	</tr>
	<loop:llct tableList="searchedList"
		inclusions="ordIndexNo,ordDetRowNo,shipInstallmentsNo,shipHeadNo,shipScheduleDay,
						shipScheduleAmount,shipNumberOfAmount,shipScheduleWarHouseCode,shipNumberOfWarHouseCode,
						salseSourceNo,shipStatus" />
</table>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">売上見出登録</div>
		</td>
	</tr>
</table>
<table border="0">
	<tr>
		<td>売上No</td>
		<td><input type="text" maxlength="5" name="salHeadNo" value=""></td>
	</tr>
	<tr>
		<td>売上区分</td>
		<td><input type="text" maxlength="5" name="salDivision" value=""></td>
	</tr>
	<tr>
		<td>得意先コード</td>
		<td><input type="text" maxlength="5" name="salCustomeCode"
			value=""></td>
	</tr>
	<tr>
		<td>売上日</td>
		<td><input name="e4" type="text" readonly="readonly"
			value="<date:date />"> <input type="button" value="参照"
			onclick="wrtCalendarLay(this.form.e4,event)"></td>
	</tr>
</table>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">売上明細登録</div>
		</td>
	</tr>
</table>
<table border="0">
	<tr>
		<td>売上No</td>
		<td><input type="text" maxlength="5" name="salHeadNo" value=""></td>
	</tr>
	<tr>
		<td>売上行No</td>
		<td><input type="text" maxlength="5" name="salRowNo" value=""></td>
	</tr>
	<tr>
		<td>売上金額</td>
		<td><input type="text" name="salProceeds" value="" maxlength="8"
			onkeyDown="return numOnly()"> <em:em key="" /></td>
	</tr>
	<tr>
		<td>売上ソースNo</td>
		<td><input type="text" maxlength="5" name="salesSourceNo"
			value=""></td>
	</tr>
</table>
<!--
			出荷実績報告では、売り上げ計上（売り上げ見出し明細の生成）および、出荷明細への更新が必要。
			出荷明細の項目では、出荷実績数量・出荷実績倉庫・売り上げソースNoの3つ

			売り上げ見出し・売り上げ明細の項目は全て入力
			入力する順番として、見出し → 明細の順番
			売上見出し(SalesHeadlineのテーブル)(salHeadNo,salDivision,salCustomeCode,salDay)
			売上明細(SalesDetailsのテーブル)(salHeadNo,salRowNo,salProceeds,salesSourceNo)
		-->
<div align="left"><input type="submit" value="送信" name="select"></div>
</form>
</body>
</html>