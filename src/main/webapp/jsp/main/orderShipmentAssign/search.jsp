<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>出荷指示</title>
<SCRIPT language="JavaScript">
<!--
// 数値のみを入力可能にする
function numOnly() {
code = event.keyCode ;
m = String.fromCharCode( code );

if(code == 46 || code == 37 || code == 39 ) return true ; // del ← →

if(!event.shiftKey) {
if("0123456789\d\b\r\t".indexOf(m, 0) < 0) return false;
else return true;
} else {
if("\t".indexOf(m, 0) >= 0) return true;
else return false ;
}
}
//-->
</SCRIPT>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>

<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">出荷指示</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table>
<form action="orderShipmentAssign_oneSearchPre.do">
<table border="0">
	<tr>
		<td>受注No.</td>
		<td><input type="text" name="primary" value=""><em:em
			key="OrdIndexNo" /></td>
		<td><font size="2" color="red">${requestScope.errorMessage}</font></td>
	</tr>
	<tr>
		<td width="180" colspan="2"><input type="hidden" name="send"
			value="true">
		<div align="right"><input type="submit" value="送信" /></div>
		</td>
	</tr>
</table>
<br>
<br>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table border="0">
	<tr height="44pix">
		<td width="180pix">
		<div align="center">出荷見出更新</div>
		</td>
	</tr>
</table>
</form>
<table border="1">
	<tr>
		<td>出荷No.</td>
		<td>出荷日</td>
		<td>運送業者</td>
		<td>問い合わせNo.</td>
		<td>出荷倉庫</td>
		<td>出荷先</td>
	</tr>
	<tr>
		<td>${requestScope.searchValues.shipHeadNo}</td>
		<td>${requestScope.searchValues.shipDay}</td>
		<td>${requestScope.searchValues.carCode}</td>
		<td>${requestScope.searchValues.shipHeadInquiry}</td>
		<td>${requestScope.searchValues.warHouseCode}</td>
		<td>${requestScope.searchValues.shipDesCode}</td>
	</tr>
</table>
</body>
</html>