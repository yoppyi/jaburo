<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>出荷明細一覧</title>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>
<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">出荷明細検索</div>
		</td>
		<td><font color="red">${requestScope.errorMessage}</font></td>
	</tr>
</table>
<form action="">
<table border="0">
	<tr>
		<td>受注No</td>
		<td><input type="text" maxlength="5" name="primary" value=""></td>
	</tr>
</table>
<input type="submit" value="検索"></form>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table border="0">
	<tr height="44pix">
		<td width="180pix">
		<div align="center">出荷明細一覧</div>
		</td>
	</tr>
</table>
<form>
<table border="1">
	<tr>
		<td>受注No</td>
		<td>行</td>
		<td>出荷分納No</td>
		<td>出荷No</td>
		<td>予定日</td>
		<td>予定数量</td>
		<td>売上ソースNo</td>
		<td>ステータス</td>
	</tr>
	<loop:llct tableList="searchedList"
		inclusions="ordIndexNo,ordDetRowNo,shipInstallmentsNo,shipHeadNo,shipScheduledDay,shipScheduleAmount,
		shipScheduleWareHouseCode,salesSourceNo,shipStatus" />
</table>
</form>
</body>
</html>