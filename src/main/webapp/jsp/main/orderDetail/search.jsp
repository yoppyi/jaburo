<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<%@ taglib uri="loopListCreateTable" prefix="loop"%>
<%@ taglib uri="errorMessage" prefix="em"%>
<%@ taglib uri="turnUpMaster" prefix="tum"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-31j">
<title>受注明細検索</title>
<style type="text/css">
<!--
body {
	background-repeat: no-repeat;
}
-->
</style>
</head>

<body background="../../img/main.jpg" bgproperties="fixed">
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>

<!--
	修正時に、ステータスが出荷済み（20）になってしまっているものに関しては、
	修正不可とする！！
	
	どうする？
	検索時に、ステータスが20のものを表示させない？
	それとも、表示はされるが選択できないようにさせる？
	
-->


<table>
	<tr height="47pix">
		<td width="180pix">
		<div align="center">受注明細検索</div>
		</td>
		<td><font color="red">${requestScope.headMessage}</font></td>
	</tr>
</table>
<form action="orderDetail_search.do">
<table border="0">
	<tr>
		<td>受注No</td>
		<td><input type="text" name="ordIndexNo" maxlength="5" value="">	</td>
	</tr>
</table>
<input type="submit" value="検索" name="search" /></form>
<div style="position: relative; width: 738px; height: 0px; -adbe-g: p;">
<img src="../../img/main_01.png"></div>
<table border="0">
	<tr height="44pix">
		<td width="180pix">
		<div align="center">受注明細一覧</div>
		</td>
	</tr>
</table>
<form action="orderDetail_update.do" method="post">
<table border="1">
	<tr>
		<td>受注No</td>
		<td>受注行No</td>
		<td>商品コード</td>
		<td>出荷倉庫コード</td>
		<td>受注個数</td>
		<td>希望納期</td>
		<td>ステータス</td>
	</tr>
	<loop:llct tableList="searchedList"
		inclusions="ordIndexNo,ordDetRowNo,goodsName,warName,ordNumberOfOrder,ordHopeDeliveryDate,oDStatus" />
</table>
</form>
</body>
</html>