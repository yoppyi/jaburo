<%@ page language="java" contentType="text/html; charset=windows-31j"
	pageEncoding="windows-31j"%>
<jsp:useBean id="isLoginError" scope="request" class="java.lang.Object"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<title>Insert title here</title>
		<style type="text/css">
			<!--
			A {
				text-decoration: none;
			}
			
			A:hover {
				color: white;
			}
			-->
		</style>
	
	</head>
	<body leftmargin="0" marginheight="0" marginwidth="0" topmargin="0">
	<noscript><font size="4">インターネットオプションからJavaScriptを有効にして下さい。</font></noscript>  
		<img src="../../img/job.jpg" width="890" height="160">
		<div
			style="position: relative; width: 888px; height: 0px; -adbe-g: p;">
			<div
				style="position: absolute; top: -158px; left: 470px; width: 306px; height: 28px;">
				<form action="authenticationCheck.do" method="post">
					<table border="0" cellspacing="2">
						<tr>
							<td><font color="white" size="2"><strong>ID</strong></font></td>
							<td><input type="text" name="empCode" size="15" /></td>
							<td><font color="white" size="2"><strong>PASS</strong></font></td>
							<td><input type="password" name="empPass" size="15" /></td>
							<td><input type="submit" value="ログイン" /></td>
						</tr>
						<tr><td colspan="4">
							<% if (isLoginError.equals(Boolean.TRUE)) { %>
								<font color="red">ユーザ名, またはパスワードが違います。</font>
							<% } %>
						</td></tr>
					</table>
				</form>
			</div>
			<div style="position: absolute; top: -158px; left: 800px; width: 101px; height: 49px;">
				<table>
					<tr>
						<td>
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp"><img src="../../img/renpo.gif" width="40" border="0"/></a>
							</div>
						</td>
						<td>
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp"><img src="../../img/jion.jpg" width="40" border="0"/></a>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div style="position: absolute; top: -33px; left: 233px; height: 56px;">
				<table border="0">
					<tr>
						<td width="130">
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp" target="job">>
									<font color="white">受注管理</font>
								</a>
							</div>
						</td>
						<td width="130">
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp" target="job">>
									<font color="white">出荷管理</font>
								</a>
							</div>
						</td>
						<td width="130">
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp" target="job">>
									<font color="white">売上管理</font>
								</a>
							</div>
						</td>
						<td width="130">
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp" target="job">>
									<font color="white">請求入金管理</font>
								</a>
							</div>
						</td>
						<td width="130">
							<div align="center" title="先にログインして下さい">
								<a href="../job/blank.jsp" target="job">>
									<font color="white">社員管理</font>
								</a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>