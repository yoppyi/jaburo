package net.kronosjp.account;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.EmployeeMaster;

/**
 * ログイン処理用クラス
 * 
 */
public class AuthenticationChecker {

	private static Hashtable<String, Object> accountTable = new Hashtable<String, Object>();
	
	/**
	 * ログイン処理
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public static String authenticationCheck(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String nextUrl = null;
		String empCode = request.getParameter("empCode");
		String empPass = request.getParameter("empPass");
		
		EmployeeMaster em =DaoBase.keySearch(EmployeeMaster.class, empCode, request);
		
		// ログイン処理
		if (em != null && empPass.equals(em.getEmpPass())) {
			// 成功
			HttpSession session = request.getSession();
			session.setAttribute("EmployeeMaster", em);
			nextUrl = "authenticationCheck.do";
		} else {
			// 失敗
			nextUrl = "blank.do";
			request.setAttribute("isLoginError", Boolean.TRUE);
		}
		return nextUrl;
	}
}