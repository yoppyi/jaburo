package net.kronosjp.util.bean;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;

import net.kronosjp.util.collections.CollectionUtils;

/**
 * @author Norihiko Yoshida
 * 
 * @version 1.5
 * 
 */
public class BeanUtils {

	/**
	 * そのうちorg.apache.commons.beanutilsのPropertyUtilsでも使って改造したいな的な所です。
	 * 
	 * @param <T>
	 * @param klass
	 * @param o
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static <T> T compareAndSetBean(Class<T> klass, Object... o)
			throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		Constructor<T> constructor = klass.getConstructor();
		T bean = constructor.newInstance();

		return compareAndSetBean(bean, o);
	}

	/**
	 * @param <T>
	 * @param bean
	 * @param o
	 * @return
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static <T> T compareAndSetBean(T bean, Object... o)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		Class<T> classOfT = (Class<T>) bean.getClass();
		Class<Object> classOfObject = null;

		Method[] methodsOfT = classOfT.getMethods();
		Method[] methodsOfObject = null;

		List<String> comparedMethods = null;

		List<String> getterMethodNameList = null;
		List<String> setterMethodNameList = null;
		List<String> machedMethodNameList = null;

		for (Object obj : o) {

			classOfObject = (Class<Object>) obj.getClass();
			methodsOfObject = classOfObject.getMethods();

			comparedMethods = ListUtils.intersection(
					createStringMethodNameList(methodsOfT),
					createStringMethodNameList(methodsOfObject));

			getterMethodNameList = new ArrayList<String>();
			setterMethodNameList = new ArrayList<String>();
			machedMethodNameList = new ArrayList<String>();

			for (String methodName : comparedMethods) {
				if (methodName.startsWith("get")
						&& !StringUtils
								.contains(methodName, "SerialVersionUID")) {
					getterMethodNameList.add(methodName);
				}
				if (methodName.startsWith("set")
						&& !StringUtils
								.contains(methodName, "SerialVersionUID")) {
					setterMethodNameList.add(methodName);
				}
			}

			String removedMethodName = null;
			for (String getterMethodName : getterMethodNameList) {
				removedMethodName = StringUtils.remove(getterMethodName, "get");
				for (String setterMethodName : setterMethodNameList) {
					if (removedMethodName.equals(StringUtils.remove(
							setterMethodName, "set"))) {
						machedMethodNameList.add(removedMethodName);
					}
				}
			}

			Method methodOfT = null;
			Method methodOfObject = null;
			for (String machedMethodName : machedMethodNameList) {
				for (Method methodOfTTemp : methodsOfT) {
					if (("set" + machedMethodName).equals(methodOfTTemp
							.getName())) {
						methodOfT = methodOfTTemp;
					}
				}
				for (Method methodOfObjectTemp : methodsOfObject) {
					if (("get" + machedMethodName).equals(methodOfObjectTemp
							.getName())) {
						methodOfObject = methodOfObjectTemp;
					}
				}
				methodOfT.invoke(bean, methodOfObject.invoke(obj));
			}
		}
		return bean;
	}

	private static List<String> createStringMethodNameList(Method[] methods) {
		List<String> simpleFieldMethodList = new ArrayList<String>();
		for (Method method : methods) {
			simpleFieldMethodList.add(method.getName());
		}
		return simpleFieldMethodList;
	}
}
