package net.kronosjp.util.bean;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.LazyDynaClass;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * HttpServletRequestからパラメータを取り出しBeanオブジェクトにセットします。
 * パッケージorg.apache.commons.beanutils、org.apache.commons.loggingが必要。
 * 
 * @author Norihiko Yoshida
 * @version 3.0
 */
public class BeanCreator {

	private static final String[] NULLEXCLUSION = null;
	private static final Map<String, FieldType> NULLSETFIEIDDATATYPE = null;

	/**
	 * Beanのフィールドのデータ型の指定用
	 * 
	 */
	public enum FieldType {
		Byte, Double, Float, Integer, Long, Short
	}

	/**
	 * パラメータ名とフィールド名が一致していれば、paramMapのValueをT型のBeanオブジェクトのフィールドにセットし返す。 Beanのセッターに渡される値はストリング型になる。
	 * 
	 * @param <T>
	 *            Serializableを実装したBeanのタイプ。
	 * @param paramNameEnum
	 *            BeanにセットしたいString型のフィールド名を格納したEnumerationオブジェクト。
	 * @param paramMap
	 * 
	 * BeanにセットしたいString型のフィールドの名をKeyに、セットする値をString[]のindex[0]に格納したValueを持つMapブジェクト。
	 * @param klass
	 *            パラメータをセットしたいBeanのClass。
	 * @param exclusionFields
	 *          Beanのへのセットを除外したいフィールド名を格納した配列。
	 * @return パラメータをセットしたT型のBeanオブジェクト。
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 */
	public static <T extends Serializable> T createBean(
			Enumeration<String> paramNameEnum, Map<String, String[]> paramMap,
			Class<T> klass, String... exclusionFields) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException {

		Constructor<T> constructor = klass.getConstructor();
		T baen = constructor.newInstance();
		Field[] fields = klass.getDeclaredFields();

		String paramName = null;
		while (paramNameEnum.hasMoreElements()) {
			paramName = paramNameEnum.nextElement();

			if (isFieldNameMachParamName(fields, paramName)
					&& isNotExclusionParam(paramName, exclusionFields)) {

				PropertyUtils.setProperty(baen, paramName, paramMap
						.get(paramName)[0]);
			}
		}
		return baen;
	}

	/**
	 * パラメータ名とフィールド名が一致していれば、paramMapのValueをT型のBeanオブジェクトのフィールドにセットし返す。 Beanのセッターに渡される値はストリング型になる。
	 * 
	 * @param <T>
	 *            Serializableを実装したBeanのタイプ。
	 * @param paramNameEnum
	 *            BeanにセットしたいString型のフィールド名を格納したEnumerationオブジェクト。
	 * @param paramMap
	 * 
	 * BeanにセットしたいString型のフィールドの名をKeyに、セットする値をString[]のindex[0]に格納したValueを持つMapブジェクト。
	 * @param klass
	 *            パラメータをセットしたいBeanのClass。
	 * @return パラメータをセットしたT型のBeanオブジェクト。
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 */
	public static <T extends Serializable> T createBean(
			Enumeration<String> paramNameEnum, Map<String, String[]> paramMap,
			Class<T> klass) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException {

		return createBean(paramNameEnum, paramMap, klass, NULLEXCLUSION);
	}

	/**
	 * 
	 * HttpServletRequestからパラメータを取り出し、パラメータ名とフィールド名が一致していれば、T型のBeanオブジェクトのフィールドにセットし返す。
	 * Beanのセッターに渡される値はストリング型になる。
	 * 
	 * 
	 * @param <T>
	 *            Serializableを実装したBeanのタイプ。
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest。
	 * @param klass
	 *            パラメータをセットしたいBeanのClass。
	 * @param exclusionFields
	 *            Beanのへのセットを除外したいフィールド名を格納した配列。
	 * @return パラメータをセットしたT型のBeanオブジェクト。
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 */
	public static <T extends Serializable> T createBean(
			HttpServletRequest request, Class<T> klass, String... exclusionFields)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, IllegalArgumentException,
			InstantiationException {

		Enumeration<String> paramNameEnum = request.getParameterNames();
		Map<String, String[]> paramMap = request.getParameterMap();

		return createBean(paramNameEnum, paramMap, klass, exclusionFields);
	}

	/**
	 * HttpServletRequestからパラメータを取り出し、パラメータ名とフィールド名が一致していれば、T型のBeanオブジェクトのフィールドにセットし返す。
	 * Beanのセッターに渡される値はストリング型になる。
	 * 
	 * @param <T>
	 *            Serializableを実装したBeanのタイプ。
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest。
	 * @param klass
	 *            パラメータをセットしたいBeanのClass。
	 * @return パラメータをセットしたT型のBeanオブジェクト。
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 */
	public static <T extends Serializable> T createBean(
			HttpServletRequest request, Class<T> klass)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, IllegalArgumentException,
			InstantiationException {

		return createBean(request, klass, NULLEXCLUSION);
	}

	/**
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * setFieldDataTypeによって指定のないBeanのフィールドのデータ型はString型なる。
	 * 
	 * @param paramNameEnum
	 *            BeanにセットしたいString型のフィールド名を格納したEnumerationオブジェクト。
	 * @param paramMap
	 *            BeanにセットしたいString型のフィールドの名をKeyに、セットする値をString[]のindex[0]に格納したValueを持つMapブジェクト。
	 * @param setFieldDataType
	 *            String型のフィールドの名をKeyに、そのフィールドに指定したいデータ型をValueに持つMapブジェクト。フィールドのデータ型はFieldTypeから選択。
	 * @param exclusionParamName
	 *            フィールドの設定を除外したいパラメータ名を格納した配列。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 * 
	 * 
	 */
	public static DynaBean createBean(Enumeration<String> paramNameEnum,
			Map<String, String[]> paramMap,
			Map<String, FieldType> setFieldDataType, String... exclusionParamName) {

		LazyDynaClass dynaClass = new LazyDynaClass();
		BasicDynaBean bean = new BasicDynaBean(dynaClass);
		String paramName = null;

		while (paramNameEnum.hasMoreElements()) {
			paramName = paramNameEnum.nextElement();
			if (setFieldDataType != null
					&& setFieldDataType.containsKey(paramName)
					&& isNotExclusionParam(paramName, exclusionParamName)) {

				switch (setFieldDataType.get(paramName)) {
				case Integer:
					bean
							.set(paramName, new Integer(
									paramMap.get(paramName)[0]));
					break;
				case Double:
					bean.set(paramName, new Double(paramMap.get(paramName)[0]));
					break;
				case Long:
					bean.set(paramName, new Long(paramMap.get(paramName)[0]));
					break;
				case Float:
					bean.set(paramName, new Float(paramMap.get(paramName)[0]));
					break;
				case Byte:
					bean.set(paramName, new Byte(paramMap.get(paramName)[0]));
					break;
				case Short:
					bean.set(paramName, new Short(paramMap.get(paramName)[0]));
					break;
				}

			} else if (isNotExclusionParam(paramName, exclusionParamName)) {
				bean.set(paramName, paramMap.get(paramName)[0]);
			}
		}
		return bean;
	}

	/**
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * 
	 * @param paramNameEnum
	 *            BeanにセットしたいString型のフィールド名を格納したEnumerationオブジェクト。
	 * @param paramMap
	 *            BeanにセットしたいString型のフィールドの名をKeyに、セットする値をString[]のindex[0]に格納したValueを持つMapブジェクト。
	 * @param exclusionParamName
	 *            フィールドの設定を除外したいパラメータ名を格納した配列。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 */
	public static DynaBean createBean(Enumeration<String> paramNameEnum,
			Map<String, String[]> paramMap, String... exclusionParamName) {
		return createBean(paramNameEnum, paramMap, NULLSETFIEIDDATATYPE,
				exclusionParamName);
	}

	/**
	 * 
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * setValueTypeによって指定のないBeanのフィールドのデータ型はString型なる。
	 * 
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest
	 * @param setFieldDataType
	 *            String型のフィールドの名をKeyに、そのフィールドに指定したいデータ型をValueに持つMapブジェクト。フィールドのデータ型はFieldTypeから選択。
	 * @param exclusionParamName
	 *            フィールドの設定を除外したいパラメータ名を格納した配列。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 * 
	 */
	public static DynaBean createBean(HttpServletRequest request,
			Map<String, FieldType> setFieldDataType, String... exclusionParamName) {

		Enumeration<String> paramNameEnum = request.getParameterNames();
		Map<String, String[]> paramMap = request.getParameterMap();

		return createBean(paramNameEnum, paramMap, setFieldDataType, exclusionParamName);
	}

	/**
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * Beanのフィールドのデータ型はString型なる。
	 * 
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest。
	 * @param exclusionParamName
	 *            フィールドの設定を除外したいパラメータ名を格納した配列。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 */
	public static DynaBean createBean(HttpServletRequest request,
			String... exclusionParamName) {

		Enumeration<String> paramNameEnum = request.getParameterNames();
		Map<String, String[]> paramMap = request.getParameterMap();

		return createBean(paramNameEnum, paramMap, exclusionParamName);
	}

	/**
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * setValueTypeによって指定のないBeanのフィールドのデータ型はString型なる。
	 * 
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest。
	 * @param setFieldDataType
	 *            String型のフィールドの名をKeyに、そのフィールドに指定したいデータ型をValueに持つMapブジェクト。フィールドのデータ型はFieldTypeから選択。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 * 
	 */
	public static DynaBean createBean(HttpServletRequest request,
			Map<String, FieldType> setFieldDataType) {

		return createBean(request, setFieldDataType, NULLEXCLUSION);
	}

	/**
	 * HttpServletRequestからパラメータを取り出しフィールドをセットしたDynaBean型のBeanオブジェクトを返す。
	 * Beanのフィールドのデータ型はString型なる。
	 * 
	 * @param request
	 *            パラメータを取り出したいHttpServletRequest。
	 * @return フィールドを設定したDynaBean型のBeanオブジェクト。
	 */
	public static DynaBean createBean(HttpServletRequest request) {

		return createBean(request, NULLEXCLUSION);
	}

	/**
	 * 除外したいパラメータを検証。
	 * 
	 * @param paramName
	 *            比較するパラメータ。
	 * @param exclusionParamName
	 *            除外したいパラメータを格納した配列。
	 * @return
	 */
	private static boolean isNotExclusionParam(String paramName,
			String[] exclusionParamName) {

		if (exclusionParamName != null) {
			for (String str : exclusionParamName) {
				if (str.equals(paramName)) {
					return false;
				}
			}
		}
		return true;
	}

	private static boolean isFieldNameMachParamName(Field[] fields,
			String paramName) {

		for (Field field : fields) {
			if (field.getName().equals(paramName)) {
				return true;
			}
		}
		return false;
	}

}
