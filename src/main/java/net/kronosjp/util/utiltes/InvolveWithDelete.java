package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.dao.EmployeeMasterDao;
import net.kronosjp.db.dao.OrderDao;
import net.kronosjp.exception.SystemErrorException;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 更新関係の処理を担うクラス
 * 
 * @author scott
 * 
 */
public class InvolveWithDelete extends DaoBase {

	/**
	 * 共通の削除処理
	 * 
	 * @param <T>
	 * 
	 * @param request
	 * @param action
	 * @return
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> String doDeretePre(
			HttpServletRequest request, String action)
			throws SystemErrorException {

		String initialOfAction = ActionCharacter.actionsWordChange(action);

		// 削除選択時
		if ("true".equals(request.getParameter("send"))) {
			List<T> deleteList = (List<T>) request.getSession().getAttribute("deleteList");
			if (deleteList.size() == 0) {
				request.setAttribute("headMessage", "社員が選択されていません。");
				return initialOfAction + "_deletePre.do";
			}
			doDelete(request, action);
			return initialOfAction + "_deletePre.do";
		}
		// 取消選択時
		if ("false".equals(request.getParameter("send"))) {
			deleteFromDeleteList(request, action);
			return action;
		}
		String[] key = request.getParameterValues("check");
		if (key != null) {

			T bean = (T) ActionCharacter.getBeanListClassFromAction(request,
					action).get(0);

			List<T> deleteList = DaoBase.getKeySearchList((Class<T>) bean, key,
					request);
			request.getSession().setAttribute("deleteList", deleteList);

		} else {
			request.setAttribute("headMessage", "社員が選択されていません。");
			request.removeAttribute("success");
			return initialOfAction + "_deletePre.do";
		}
		return action;
	}

	/**
	 * 実際の削除処理
	 * 
	 * @param <T>
	 * @param request
	 * @param initialOfAction
	 * @return
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void doDelete(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		String initialOfAction = ActionCharacter.actionsWordChange(action);
		String[] newWords = ActionCharacter
				.getTableNameAndKeyName(initialOfAction);
		String primaryKeyName = newWords[1];

		if ("admin".equals(initialOfAction)) {
			EmployeeMasterDao.delete(request);
			return;
		}

		// ▽受注取消///////////////////////////////////////////////////////////////////////////ここから▽
		if ("order".equals(initialOfAction)) {
			if ("10".equals(request.getParameter("oIStatus")))
				;
			OrderDao.deleteOrderIndex(request);
			return;
		}
		// △受注取消///////////////////////////////////////////////////////////////////////////ここまで△
		StringBuilder successMessage = new StringBuilder();

		String[] checkParam = request.getParameterValues("check");

		List<T> beforeList = (List<T>) Arrays.asList(checkParam);
		// ////////////////////////////////////////////////////////////////////////////////////////////

		System.out.println(beforeList
				+ "*************************************beforeList");

		// ///////////////////////////////////////////////////////////////////////////////////////////
		List<String> delCodeList = new ArrayList<String>();

		Session session = null;
		Transaction transaction = null;
		String deleteCode = null;
		try {
			session = createSession(request);
			transaction = session.beginTransaction();

			for (T before : beforeList) {
				try {

					// /////////////////////////////////////////////////////////////////////////////

					System.out.println(before
							+ "**************************************before");

					System.out.println(ActionCharacter
							.getTableNameAndKeyName(initialOfAction)[1]);

					// ////////////////////////////////////////////////////////////////////////////

					deleteCode = (String) PropertyUtils
							.getProperty(before, ActionCharacter
									.getTableNameAndKeyName(initialOfAction)[1]);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
				successMessage.append(deleteCode + "、");
				delCodeList.add(deleteCode);
			}
			// ////////////////////////////////////////////////////////////////////////////////////////////////////

			System.out
					.println(delCodeList
							+ "************************************************delCodeList");

			// /////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new SystemErrorException(e);
		} finally {
			closeSession(request);
		}

		try {
			List<String> delList = new ArrayList<String>();
			T bean = (T) ActionCharacter.getBeanListClassFromAction(request,
					action).get(0);
			bean = null;
			session = createSession(request);
			transaction = session.beginTransaction();
			for (String delCode : delCodeList) {
				System.out.println(delCode);

				bean = (T) session.get((Class) ActionCharacter
						.getBeanListClassFromAction(request, action).get(0),
						delCode);

				System.out.println("d" + bean);

				session.delete(bean);
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw new SystemErrorException(e);
		} finally {
			closeSession(request);
		}

		successMessage.deleteCharAt(successMessage.length() - 1);
		successMessage.append("は、正常に削除されました。");
		request.setAttribute("headMessage", successMessage.toString());
	}

	/**
	 * キャンセル確定処理
	 * 
	 * @param request
	 * @param initialOfAction
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void deleteFromDeleteList(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		String initialOfAction = ActionCharacter.actionsWordChange(action);
		String[] cancelKey = request.getParameterValues("check");

		if (cancelKey == null) {
			request.setAttribute("headMessage", "社員が選択されていません。");
			request.removeAttribute("success");
			return;
		}
		T bean = (T) ActionCharacter
				.getBeanListClassFromAction(request, action).get(0);
		String[] keys = ActionCharacter.getTableNameAndKeyName(initialOfAction);

		List<T> deleteCancelList = DaoBase.getKeySearchList((Class) bean,
				cancelKey, request);
		List<T> deleteList = (List<T>) request.getSession().getAttribute(
				"deleteList");
		List<T> newDeleteList = new ArrayList<T>();
		lable: for (T deleteBean : deleteList) {
			for (T cancelBean : deleteCancelList) {
				try {
					if (PropertyUtils.getProperty(deleteBean, keys[1]).equals(
							PropertyUtils.getProperty(cancelBean, keys[1]))) {
						continue lable;
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
			newDeleteList.add(deleteBean);
		}
		for (T param : newDeleteList) {
		}
		request.getSession().setAttribute("deleteList", newDeleteList);
	}
}