package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderDetailsPK;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.PK;
import net.kronosjp.exception.InvalidInputException;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.util.bean.BeanCreator;
import net.kronosjp.util.validator.InputValidator;

/**
 * 更新関係の処理を担うクラス
 * 
 * @author scott
 * 
 */
public class InvolveWithUpdate {

	/**
	 * 共通の更新処理
	 * 
	 * @param <T>
	 * 
	 * @param action
	 * @param request
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> String doUpdatePre(
			HttpServletRequest request, String action)
			throws SystemErrorException {

		request.removeAttribute("success");
		request.removeAttribute("errorMessage");
		request.removeAttribute("errorMessageMap");
		request.removeAttribute("headMessage");
		String initialOfAction = ActionCharacter.actionsWordChange(action);
		// 更新選択時
		if ("true".equals(request.getParameter("send"))) {
			action = doUpdate(request, action);
			return action;
		}

		if ("orderDetail".equals(initialOfAction)
				&& (request.getParameter("radio") == null || StringUtils
						.isBlank(request.getParameter("ordIndexNo")))) {

			if (StringUtils.isBlank(request.getParameter("ordIndexNo"))) {
				request.setAttribute("headMessage", "先に受注Noを検索して下さい");
			} else if ((request.getParameter("radio") == null)) {
				request.setAttribute("headMessage", "受注詳細を選択してください。");
			}
			return initialOfAction + "_updatePre.do";

		} else if ("orderDetail".equals(initialOfAction)
				&& request.getParameter("radio") != null
				&& request.getParameter("send") == null) {

			OrderDetailsPK orderDetailsPK = new OrderDetailsPK();
			orderDetailsPK.setOrdDetRowNo(new Short(request
					.getParameter("radio")));
			orderDetailsPK.setOrdIndexNo(request.getParameter("ordIndexNo"));
			request.setAttribute("orderDetails", DaoBase.keySearch(
					OrderDetails.class, orderDetailsPK, request));

			InvolveWithGetEntityClass.getClassNecessaryForCreate(request,
					action);

			return initialOfAction + "_update.do";
		}
		
		if (request.getParameter("update") != null) {

			OrderIndex orderIndex = new OrderIndex();
			orderIndex.setCliCode(request.getParameter("cliCode"));
			orderIndex.setEmpCode(request.getParameter("empCode"));
			orderIndex.setoIStatus(request.getParameter("oIStatus"));
			try {
				orderIndex.setOrdDay(DaoBase.getFormatedDate(request
						.getParameter("e4")));

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			orderIndex.setOrdIndAdhibition(request
					.getParameter("ordIndAdhibition"));
			orderIndex.setOrdIndexNo(request.getParameter("ordIndexNo"));

			DaoBase.update(orderIndex, request);
			request.setAttribute("headMessage", "更新成功しました。");

			return initialOfAction + "_updatePre.do";

		}

		if (StringUtils.contains(action, "orderCaption_update")) {
			if (request.getParameter("radio") == null) {
				request.setAttribute("headMessage", "受注見出しを選択してください。");
				return initialOfAction + "_updatePre.do";
			}
			
			OrderIndex orderIndex = DaoBase.keySearch(OrderIndex.class, request
					.getParameter("radio"), request);

			request.setAttribute("date", StringUtils.replace(orderIndex
					.getOrdDay().toString(), "-", "/"));
			request.setAttribute("order", orderIndex);

		}

		if ("admin".equals(initialOfAction)) {
			String key = request.getParameter("radio");
			if (key != null) {

				T bean = (T) DaoBase.keySearch((Class<T>) ActionCharacter
						.getBeanListClassFromAction(request, action).get(0),
						request.getParameter("radio"), request);
				request.setAttribute("beanMaster", bean);

				// 社員更新画面移動時：rollLevelの設定
				InvolveWithSupport.doUpdateItemSearch(request, bean);

			} else {
				request.setAttribute("headMessage", "社員が選択されていません。");
			}
		}
		if ("orderDetail".equals(initialOfAction)) {
			InvolveWithGetEntityClass.getClassNecessaryForCreate(request,
					action);
		}

		return initialOfAction + "_update.do";
	}

	/**
	 * 更新送信ボタンを押した際の実際の更新処理
	 * 
	 * @param <T>
	 * @param request
	 * @param initialOfAction
	 * @return
	 * @throws SystemErrorException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable & PK> String doUpdate(
			HttpServletRequest request, String action)
			throws SystemErrorException {

		String initialOfAction = ActionCharacter.actionsWordChange(action);

		// ▽受注明細修正/////////////////////////////////////////////////////////ここから▽

		if (StringUtils.contains(action, "orderDetail")) {

			OrderDetailsPK orderDetailsPK = new OrderDetailsPK();
			orderDetailsPK.setOrdDetRowNo(new Short(request
					.getParameter("ordDetRowNo")));
			orderDetailsPK.setOrdIndexNo(request.getParameter("ordIndexNo"));
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setOrderDetailsPK(orderDetailsPK);
			orderDetails.setoDStatus("10");
			orderDetails.setGoodsCode(request.getParameter("goodsCode"));
			orderDetails.setOrdNumberOfOrder(new Integer(request
					.getParameter("ordNumberOfOrder")));
			try {
				orderDetails.setOrdHopeDeliveryDate(DaoBase
						.getFormatedDate(request
								.getParameter("ordHopeDeliveryDate")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			DaoBase.update(orderDetails, request);

			return initialOfAction + "_updatePre.do";

		}
		// △受注明細修正/////////////////////////////////////////////////////////ここまで△

		// ▽社員情報修正？/////////////////////////////////////////////////////////ここから▽
		StringBuilder successMessage = new StringBuilder();
		successMessage.append("社員コード");
		String rollId = null;
		try {
			// Beanクラスの作成
			T bean = (T) BeanCreator.createBean(request,
					(Class<T>) ActionCharacter.getBeanListClassFromAction(
							request, action).get(0));
			String[] newWords = ActionCharacter
					.getTableNameAndKeyName(initialOfAction);
			String primaryKeyName = newWords[1];
			// 社員更新：更新情報の設定
			if ("admin".equals(initialOfAction)) {
				((EmployeeMaster) bean).setEmpPass(DaoBase.keySearch(
						EmployeeMaster.class,
						((EmployeeMaster) bean).getEmpCode(), request)
						.getEmpPass());
				rollId = InvolveWithSupport.createRollId(request);
				((EmployeeMaster) bean).setRollId(rollId);
				if (rollId == null) {
					request.setAttribute("errorMessage", "権限が指定されていません。");
					InputValidator.validate(request, ActionCharacter
							.getValidateClass(initialOfAction));
					return "admin_update.do";
				}
			}

			InputValidator.validate(request, ActionCharacter
					.getValidateClass(initialOfAction));
			DaoBase.update(bean, request);
			successMessage.append(PropertyUtils.getProperty(bean,
					primaryKeyName));

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			System.out.println("method such to property doesn't exist. ");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (InvalidInputException e) {
			e.printStackTrace();
			return initialOfAction + "_update.do";
		}
		successMessage.append("は、正常に更新されました。");
		request.setAttribute("success", successMessage.toString());
		// △社員情報修正？/////////////////////////////////////////////////////////ここまで△

		return initialOfAction + "_updatePre.do";
	}
}
