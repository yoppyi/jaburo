package net.kronosjp.util.utiltes;

import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.exception.SystemErrorException;

/**
 * コンポーネントクラス
 * 
 * @author scott
 */
public class Utiltes {

	/**
	 * 最新のPrimaryKeyを取得（3桁でない場合0で埋める）
	 * 
	 * @param request
	 * @param action
	 * @throws SystemErrorException
	 * @throws NumberFormatException
	 */
	public static void getValue(HttpServletRequest request, String action)
			throws NumberFormatException, SystemErrorException {

		String[] newWords = ActionCharacter.getTableNameAndKeyName(action);
		String tableName = newWords[0];
		String primaryKeyName = newWords[1];

		// // 登録成功時の現時点でのpuraimariCodeNameのデータ削除
		// request.removeAttribute(primaryCodeName);

		Integer primaryKeyInt = Integer.parseInt(DaoBase.getMaxValueOfColumn(
				tableName, primaryKeyName, request).toString());

		primaryKeyInt += 1;
		// 【1→001】
		String primaryKeyValue = primaryKeyInt.toString();
		while (primaryKeyValue.length() < 3) {
			primaryKeyValue = "0" + primaryKeyValue;
		}

		request.setAttribute(primaryKeyName, primaryKeyValue);
	}

	/**
	 * 一意のuser_PASS【8桁】を作成する
	 * 
	 * @param
	 * @return String
	 */
	public static String getPass() {

		Random random = new Random();
		// ランダム数値→右端にACCOUNT_NUMBERを追加→
		// ハッシュコード変換→16進数変換→大文字変換
		String Pass = Integer.toHexString(new Integer(random.nextInt())
				.toString().hashCode());

		// user_PASSが8桁未満の場合、削除後再生成
		if (Pass.length() != 8) {
			Pass = null;
			getPass();
		}
		return Pass;
	}

	public static String paramCheck(HttpServletRequest request) throws SystemErrorException {
		
		String rollId = null;
		rollId = InvolveWithSupport.createRollId(request);
		
		if (rollId == null) {
			
			request.setAttribute("errorMessage", "権限が指定されていません。");
			request.removeAttribute("success");
		}
		return rollId;
	}
}
