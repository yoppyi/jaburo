package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.dao.OrderDao;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderDetailsPK;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.PK;
import net.kronosjp.db.entity.ShipmentDetails;
import net.kronosjp.db.entity.ShipmentDetailsPK;
import net.kronosjp.exception.InvalidInputException;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.util.bean.BeanCreator;
import net.kronosjp.util.validator.InputValidator;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * 登録関係の処理を持つクラス
 * 
 * @author scott
 * 
 */
public class InvolveWithCreate {

	/**
	 * 共通の登録処理
	 * 
	 * @param action
	 * @param request
	 * @throws SystemErrorException
	 */
	public static void doCreatePre(HttpServletRequest request, String action)
			throws SystemErrorException {

		// 処理する際のパッケージ名を取得
		String initialOfAction = ActionCharacter.actionsWordChange(action);

		// 入金処理paramチェックエラー
		if (request.getParameter("monRecAmount") == "") {
			request.setAttribute("headMessage", "金額が入力されていません。");
			request.removeAttribute("success");

			// 登録選択時
		}
		
		if ("true".equals(request.getParameter("send"))) {

			System.out.println("共通実際登録処理");
			
			if (action.startsWith("admin")) {

				doCreate(request, action);
			}
			if (action.startsWith("order_")) {

				int result = InvolveWithOrder.errorCheck(request);
				if (result != 0) {
					doCreate(request, action);
				}
			}
		}

		if (action.startsWith("claim")) {
			return;
		}

		if (!"admin".equals(initialOfAction)) {
			InvolveWithGetEntityClass.getClassNecessaryForCreate(request,
					action);
		}
		Utiltes.getValue(request, initialOfAction);
	}

	/**
	 * 実際の登録処理
	 * 
	 * @param <T>
	 * @param initialOfAction
	 * @param request
	 * @throws SystemErrorException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> void doCreate(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		String initialOfAction = ActionCharacter.actionsWordChange(action);
		String rollId = null;
		int counter = 0;
		int result = 0;
		List<Serializable> necessaryForCreateList = new ArrayList<Serializable>();

		if ("orderShipment".equals(initialOfAction)) {
			request.removeAttribute("success");

			OrderIndex orderIndex = DaoBase.keySearch(OrderIndex.class, request
					.getParameter("ordIndexNo"), request);

			if (request.getParameter("ordIndexNo").equals(
					orderIndex.getOrdIndexNo())) {
				// OrderDetails orderDetails = DaoBase.keySearch(
				// OrderDetails.class, request.getParameter("ordIndexNo"),
				// request);
				List<OrderDetails> orderDetailsList = DaoBase.search(
						OrderDetails.class, "ordIndexNo", request
								.getParameter("ordIndexNo"), request);
				ShipmentDetails shipmentDetails = null;
				ShipmentDetailsPK shipmentDetailsPK = null;
				for (OrderDetails orderDetails : orderDetailsList) {
					shipmentDetails = new ShipmentDetails();
					shipmentDetailsPK = new ShipmentDetailsPK();
					OrderDetailsPK detailsPK = orderDetails.getPK();
					shipmentDetailsPK
							.setOrdDetRowNo(detailsPK.getOrdDetRowNo());
					shipmentDetailsPK.setOrdIndexNo(detailsPK.getOrdIndexNo());
					shipmentDetailsPK.setShipInstallmentsNo(request
							.getParameter("ordIndexNo"));

					shipmentDetails.setShipmentDetailsPK(shipmentDetailsPK);

					shipmentDetails.setShipHeadNo(request
							.getParameter("ordIndexNo"));
					shipmentDetails.setShipHeadNo("001");
					try {
						System.out.println(DaoBase.getFormatedDate(request
								.getParameter("e4")));
						shipmentDetails.setShipScheduledDay(DaoBase
								.getFormatedDate(request.getParameter("e4")));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					shipmentDetails.setShipScheduleAmount(1);
					shipmentDetails.setShipScheduleWareHouseCode("001");
					shipmentDetails.setSalesSourceNo("001");
					shipmentDetails.setShipStatus("20");
					orderDetails.setODStatus("20");
					DaoBase.insert(shipmentDetails, request);
					DaoBase.update(orderDetails, request);
				}
				orderIndex.setOIStatus("20");
				DaoBase.update(orderIndex, request);

				request.setAttribute("success", "登録完了しました");
			} else {
				request.setAttribute("success", "その受注Noは存在しません");
			}
			return;
		}

		try {

			// 登録するBeanクラスの作成
			necessaryForCreateList = ActionCharacter
					.getBeanListClassFromAction(request, action);

			for (Serializable necessaryForCreate : necessaryForCreateList) {

				necessaryForCreateList.set(counter++, BeanCreator.createBean(
						request, (Class<T>) necessaryForCreate));
			}

			// 社員登録：rollIdの取得
			if ("admin".equals(initialOfAction)) {

				rollId = Utiltes.paramCheck(request);

				PropertyUtils.setProperty(necessaryForCreateList.get(0),
						"rollId", rollId);
				PropertyUtils.setProperty(necessaryForCreateList.get(0),
						"empPass", Utiltes.getPass());
			}

			// 受注登録：
			if ("order".equals(initialOfAction)) {
				try {
					result = OrderDao.orderCreate(request);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			// 入金登録：
			if ("money".equals(initialOfAction)) {
				try {
					result = OrderDao.orderCreate(request);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			Class<T> validateClass = ActionCharacter
					.getValidateClass(initialOfAction);

			if (validateClass != null) {

				InputValidator.validate(request, ActionCharacter
						.getValidateClass(initialOfAction));
			}
			try {
				DaoBase.insert(necessaryForCreateList.get(0), request);
			} catch (SystemErrorException e) {
				request.removeAttribute("success");
				return;

			}

			request.setAttribute("success", "登録完了しました。");

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			System.out.println("method such to property doesn't exist. ");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (InvalidInputException e) {
			System.out.println("valid Error");
		}
	}
}