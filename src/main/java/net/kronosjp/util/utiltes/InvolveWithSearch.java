package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.ShipmentHeadline;
import net.kronosjp.exception.SystemErrorException;

/**
 * 検索関係の処理を担うクラス
 * 
 * @author scott
 * 
 */
public class InvolveWithSearch {

	/**
	 * 共通の検索処理
	 * 
	 * @param <T>
	 * 
	 * @param request
	 * @param action
	 * @return
	 * @throws SystemErrorException
	 */
	public static String doSearchPre(HttpServletRequest request, String action)
			throws SystemErrorException {

		String initialOfAction = ActionCharacter.actionsWordChange(action);

		if ("true".equals(request.getParameter("send"))) {
			
			if (request.getParameter("primary") == "") {
				
				request.setAttribute("headMessage", "No.が入力されていません。");
				request.removeAttribute("success");
				
			} else {
				
				doSearch(request, action);
			}
		}
		
		InvolveWithGetEntityClass.getClassNecessaryForCreate(request, action);
		return action;
	}

	/**
	 * 実際の検索処理
	 * 
	 * @param <T>
	 * @param request
	 * @param newAction
	 * @return
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void doSearch(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		
		if (action.startsWith("shipment")) {
			
			int choice = Integer.parseInt(request.getParameter("search"));
			String choiStr = null;
			ShipmentHeadline shipmentHeadline = null;
			List<ShipmentHeadline> shipmentHeadlineList = new ArrayList<ShipmentHeadline>();
			
			switch (choice) {
				
			case 0 :
				
				
				
				
				break;
			case 1 :
				
				choiStr = request.getParameter("shipDesCode");
				shipmentHeadline = DaoBase.keySearch(ShipmentHeadline.class, choiStr, request);
				shipmentHeadlineList.add(shipmentHeadline);
				request.setAttribute("searchedList", shipmentHeadlineList);
				
				break;
			case 2 :
				choiStr = request.getParameter("shipDesCode");
				shipmentHeadline = DaoBase.keySearch(ShipmentHeadline.class, choiStr, request);
				shipmentHeadlineList.add(shipmentHeadline);
				request.setAttribute("searchedList", shipmentHeadlineList);
				
				break;
			}
			
			
			
			
		}
		
//		try {
////			InvolveWithGetEntityClass.getClassNecessaryaForCreate(request);
//		} catch (ServletException e) {
//			e.printStackTrace();
//		}
	}
}
