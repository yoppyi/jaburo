package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import net.kronosjp.db.entity.BillAddressMaster;
import net.kronosjp.db.entity.ClientMaster;
import net.kronosjp.db.entity.CustomerMaster;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.GoodsMaster;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.ShipmentDetails;
import net.kronosjp.db.entity.ShipmentHeadline;
import net.kronosjp.db.entity.WareHouseMaster;
import net.kronosjp.tags.bean.StockSearcher;
import net.kronosjp.util.validator.bean.EmployeeRegistry;
import net.kronosjp.util.validator.bean.OrderRegistry;

/**
 * action内文字列から情報を得る為の変換クラス
 * 
 * @author scott
 * 
 */
public class ActionCharacter {

	/**
	 * action内文字列から中分類処理文字列を取得 例：admin_create.do の【admin】
	 * 
	 * @param action
	 * @return String
	 */
	public static String actionsWordChange(String action) {
		String newActionsWord = action.substring(0, action.indexOf("_"));
		return newActionsWord;
	}

	/**
	 * newAction内文字列から目的とするtable名とPrimaryCode名を取得 例：admin(社員登録)のEmployeeMaster,
	 * empCode
	 * 
	 * @param action
	 * @return String[]
	 */
	public static String[] getTableNameAndKeyName(String initialOfAction) {

		// 社員関係
		if ("admin".equals(initialOfAction)) {
			return new String[] { "EmployeeMaster", "empCode" };
		}
		// 受注関係
		if ("order".equals(initialOfAction)) {
			return new String[] { "OrderIndex", "ordIndexNo" };
		}
		// 在庫関係
		if ("stock".equals(initialOfAction)) {
			return new String[] { "OrderDetails", "ordIndexNo" };
		}
		// 出荷関係
		if ("orderShipment".equals(initialOfAction)) {
			return new String[] { "OrderIndex", "ordIndexNo" };
		}
		// 出荷関係
		if ("orderShipmentAssign".equals(initialOfAction)) {
			return new String[] { "ShipmentHeadline", "shipHeadNo" };
		}
		// 出荷関係
		if ("shipment".equals(initialOfAction)) {
			return new String[] { "OrderIndex", "ordIndexNo" };
		}
		// 入金登録
		if ("money".equals(initialOfAction)) {
			return new String[] { "MoneyReceivedHeadLine", "monRecNo" };
		}
		return null;
	}

	/**
	 * newAction内文字列から目的とするValidateクラスを取得 例：admin(社員登録)のEmployeeRegistry
	 * 
	 * @param initialOfAction
	 * @return String[]
	 */
	public static <T extends Serializable> Class<T> getValidateClass(
			String initialOfAction) {

		// 社員登録
		if ("admin".equals(initialOfAction)) {
			return (Class<T>) EmployeeRegistry.class;
		}

		// 受注登録
		if ("order".equals(initialOfAction)) {
//			return (Class<T>) OrderRegistry.class;
		}
		return null;
	}

	/**
	 * newAction内文字列から目的とするBeanクラスを取得 例：admin(社員登録)のEmployeeMaster
	 * 
	 * @param action
	 * @return List<T>
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> List<Serializable> getBeanListClassFromAction(
			HttpServletRequest request, String action) {

		List<Serializable> necessaryForCreateList = new ArrayList<Serializable>();
		System.out.println("Action内文字列" + action);
		
		if (action.startsWith("admin")) {
			System.out.println("■■■社員管理□□□");
			if (StringUtils.contains(action, "create")) {
				System.out.println("■社員登録□");
				necessaryForCreateList.add(EmployeeMaster.class);
				return necessaryForCreateList;
			}
			System.out.println("■社員登録以外□");
			necessaryForCreateList.add(EmployeeMaster.class);
			return necessaryForCreateList;
		}

		if (action.startsWith("order")) {
			System.out.println("■■■受注管理□□□");
			if (StringUtils.contains(action, "create")) {
				System.out.println("■受注登録□");
				necessaryForCreateList.add(ClientMaster.class);
				necessaryForCreateList.add(EmployeeMaster.class);
				necessaryForCreateList.add(GoodsMaster.class);
				necessaryForCreateList.add(WareHouseMaster.class);
				return necessaryForCreateList;
			}
			if (StringUtils.contains(action, "search") || StringUtils.contains(action, "delete")) {
				System.out.println("■受注検索 + 削除□");
				necessaryForCreateList.add(ClientMaster.class);
				return necessaryForCreateList;
			}
			if (StringUtils.contains(action, "Detail")) {
				System.out.println("■受注明細□");
				necessaryForCreateList.add(OrderIndex.class);
				necessaryForCreateList.add(ClientMaster.class);
				necessaryForCreateList.add(EmployeeMaster.class);
				necessaryForCreateList.add(GoodsMaster.class);
				necessaryForCreateList.add(WareHouseMaster.class);
				
				return necessaryForCreateList;
			}
			if (StringUtils.contains(action, "Shipment")) {
				System.out.println("■■■出荷登録□□□");
				necessaryForCreateList.add(OrderIndex.class);
				necessaryForCreateList.add(ShipmentDetails.class);
				necessaryForCreateList.add(ShipmentHeadline.class);
				return necessaryForCreateList;
			}
			if (StringUtils.contains(action, "Caption")) {
				// 受注見出修正画面
				System.out.println("■受注見出修正画面□");
				necessaryForCreateList.add(OrderIndex.class);
				necessaryForCreateList.add(ClientMaster.class);
				return necessaryForCreateList;
			}
		}
		
		
		
		// 出荷関係
		if (action.startsWith("shipment")) {
			System.out.println(" ■　　出荷管理　　□ ");

			
			if (StringUtils.contains(action, "search")) {
				System.out.println(" ■　　出荷検索　　□ ");
				necessaryForCreateList.add(ShipmentHeadline.class);
				necessaryForCreateList.add(ShipmentDetails.class);

				System.out.println("necessaryForCreateListの中身" + necessaryForCreateList);
				
				return necessaryForCreateList;
			}
			
			if (StringUtils.contains(action, "update")) {
				System.out.println("　■　　出荷更新　　□");
				necessaryForCreateList.add(ShipmentHeadline.class);
				necessaryForCreateList.add(ShipmentDetails.class);
				
				System.out.println("necessaryForCreateListの中身" + necessaryForCreateList);
				
				return necessaryForCreateList;
			}
		}
		
		
		
		
		// 在庫関係
		if ("stock".equals(action)) {
			System.out.println("■■■在庫検索□□□");
			necessaryForCreateList.add(OrderIndex.class);
			return necessaryForCreateList;
		}

		// 入金関係
		if ("money".equals(action)) {
			necessaryForCreateList.add(CustomerMaster.class);
			necessaryForCreateList.add(BillAddressMaster.class);
			return necessaryForCreateList;

		}

		// 売上関係
		if (action.startsWith("sales")) {
			necessaryForCreateList.add(CustomerMaster.class);
			return necessaryForCreateList;
		}
		return null;
	}

	public static <T extends Serializable> Class<T> getBeanList(String initialOfAction) {
		
		if ("stock".equals(initialOfAction)) {
			return (Class<T>)StockSearcher.class;
		}
		return null;
	}
}
