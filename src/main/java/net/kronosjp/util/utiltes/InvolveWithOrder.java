package net.kronosjp.util.utiltes;

import javax.servlet.http.HttpServletRequest;

public class InvolveWithOrder {

	public static int errorCheck(HttpServletRequest request) {

		if (request.getParameter("goodsCode1") == "") {
			request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
			return 0;
		}
		if (request.getParameter("goodsCode1") != "") {
			if (request.getParameter("ordNumberOfOrder1") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode2") == "") {
			if (request.getParameter("ordNumberOfOrder2") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode3") == "") {
			if (request.getParameter("ordNumberOfOrder3") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode4") == "") {
			if (request.getParameter("ordNumberOfOrder4") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode5") == "") {
			if (request.getParameter("ordNumberOfOrder5") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode6") == "") {
			if (request.getParameter("ordNumberOfOrder6") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		if (request.getParameter("goodsCode7") == "") {
			if (request.getParameter("ordNumberOfOrder7") == "") {
				request.setAttribute("errorMessage", "商品の入力項目に誤りがあります");
				return 0;
			}
		}
		request.setAttribute("success", "受注登録が完了しました");
		return 1;
	}
}
