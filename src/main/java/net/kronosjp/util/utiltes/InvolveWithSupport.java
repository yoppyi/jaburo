package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.RollMaster;
import net.kronosjp.exception.SystemErrorException;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * 汎用性を持たないウンコクラス
 */

public class InvolveWithSupport {

	/**
	 * 更新選択データの抽出
	 * 
	 * @param <T>
	 * @param request
	 * @param bean
	 * @return action
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void doUpdateItemSearch(
			HttpServletRequest request, T bean) throws SystemErrorException {

		int rollLevelInt = 0;

		RollMaster rm;
		try {
			rm = (RollMaster) PropertyUtils.getProperty(bean, "rollMaster");
			rollLevelInt = Integer.parseInt(rm.getRollLevel());

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		String binaryRoolLevel = madeTreble(Integer
				.toBinaryString(rollLevelInt));

		char[] charArrayRoolLevel = binaryRoolLevel.toCharArray();

		int counter = 1;
		String[] categoryNameArray = { "c0", "c1", "c2", "c3" };
		String[] categoryValueArray = { "15", "2", "4", "8" };

		if (charArrayRoolLevel[3] == '1') {
			request.setAttribute(categoryNameArray[0], categoryValueArray[0]);
		} else {
			for (char charRollLevel : charArrayRoolLevel) {
				if (charRollLevel == '1') {

					request.setAttribute(categoryNameArray[counter],
							categoryValueArray[counter]);
				}
				counter++;
			}
		}
	}

	/**
	 * 2進数（4桁でない場合0で埋める）
	 * 
	 * @param binaryRoolLevel
	 * @return
	 */
	private static String madeTreble(String binaryRoolLevel) {

		while (binaryRoolLevel.length() < 4) {
			binaryRoolLevel = "0" + binaryRoolLevel;
		}
		return binaryRoolLevel;
	}

	/**
	 * パラメータからのRollLevelを元にRollIdを作成。
	 * RollLevelが選択されていない場合java.lang.String型のNullを返す。
	 * equest.setAttribute("errorMessage"…Nullの際のコメント設定
	 * 
	 * @param request
	 * @return String
	 * @throws SystemErrorException
	 */
	public static String createRollId(HttpServletRequest request)
			throws SystemErrorException {

		Integer rollLevel = 0;
		String rollId = null;
		String[] rollLevelArray = { request.getParameter("c0"),
				request.getParameter("c1"), request.getParameter("c2"),
				request.getParameter("c3") };

		// RollLevelの合計
		for (String roll : rollLevelArray) {
			try {
				rollLevel += Integer.valueOf(roll);
			} catch (NumberFormatException e) {
				// nullの項目を除外
			}
		}

		List<RollMaster> rollList = DaoBase.getTable(RollMaster.class, request);

		for (RollMaster rm : rollList) {
			// RollLevelの合計を元にマッチするRollIdの割出
			if (rm.getRollLevel().equals(rollLevel.toString())) {
				rollId = rm.getRollId();
				break;
			}
		}
		return rollId;
	}
}