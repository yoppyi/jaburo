package net.kronosjp.util.utiltes;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.dao.OrderDao;
import net.kronosjp.db.entity.CustomerMaster;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderDetailsPK;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.SalesDetails;
import net.kronosjp.db.entity.SalesHeadline;
import net.kronosjp.db.entity.StockAccordingWareHouse;
import net.kronosjp.db.entity.StockAccordingWareHousePK;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.tags.bean.Customer;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.tags.bean.StockSearcher;
import net.kronosjp.tags.bean.util.CustomerListCreator;
import net.kronosjp.tags.bean.util.OrderListCreator;
import net.kronosjp.util.bean.BeanUtils;

import org.apache.commons.lang.StringUtils;

/**
 * EntityClassを取得する関係の処理を担うクラス
 * 
 * @author scott
 * 
 */
public class InvolveWithGetEntityClass {

	/**
	 * 検索結果等テーブル内容を表示
	 * 
	 * @param <T>
	 * 
	 * @param request
	 * @param initialOfAction
	 * @return
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> void getClassNecessaryForDisplay(
			HttpServletRequest request, String action) throws ServletException {

		String initialOfAction = ActionCharacter.actionsWordChange(action);

		if (action.startsWith("order_")) {

			getClassNecessaryForCreate(request, action);
			OrderDao.search(request);
		}
		if (action.startsWith("order")) {

			if (StringUtils.contains(action, "delete")) {

				request.setAttribute("searchedList", OrderListCreator
						.createListForOderDelete(request));
				return;
			} else if (StringUtils.contains(action, "Caption")) {
				// /////////////////////////////////////////////////////////////////////////////////////////////
				orderSearchOfCaption(request, action);
				// /////////////////////////////////////////////////////////////////////////////////////////////
				getClassNecessaryForCreate(request, action);
				return;
			} else if (StringUtils.contains(action, "Detail")) {

				orderSearch(request, action);
				return;
			} else if (action.startsWith("shipment")) {
				return;
			}
			if (action.startsWith("claim")) {
				return;
			}

			getClassNecessaryForCreate(request, action);
		}
		if (action.startsWith("sales")) {
			salesSearch(request, action);// 下にメソッド書いてあるよん
			getClassNecessaryForCreate(request, action);
			return;
		}
		if (action.startsWith("shipment")) {
			getClassNecessaryForCreate(request, action);
			
			if ("true".equals(request.getParameter("send"))) {
				
				InvolveWithSearch.doSearch(request, action);
			}
			
			return;
		}
		getSearchedList(request, action);
	}

	public static <T extends Serializable> void getSearchedList(
			HttpServletRequest request, String action)
			throws SystemErrorException {

		String initialOfAction = ActionCharacter.actionsWordChange(action);
		List<T> searchedList = (List<T>) DaoBase.getTable(
				(Class<T>) ActionCharacter.getBeanListClassFromAction(request,
						action).get(0), request);

		request.setAttribute("searchedList", searchedList);
	}

	/**
	 * DB接続後、登録に必要となる選択項目Tableの取得
	 * 
	 * @param <T>
	 * @param request
	 * @param initialOfAction
	 * @throws SystemErrorException
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> void getClassNecessaryForCreate(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		String initialOfAction = ActionCharacter.actionsWordChange(action);
		String beanListName = null;
		if ("money".equals(initialOfAction)) {

			List<CustomerMaster> customerList = DaoBase.getTable(
					CustomerMaster.class, request);
			request.setAttribute("customerList", customerList);
		}

		List<Serializable> necessaryForCreateList = ActionCharacter
				.getBeanListClassFromAction(request, action);

		for (Serializable bean : necessaryForCreateList) {

			String beanClassName = bean.toString().substring(
					(bean.toString().lastIndexOf(".") + 1));
			try {
				beanListName = StringUtils.uncapitalize(beanClassName)
						.substring(0, beanClassName.lastIndexOf("Master"))
						.concat("List");
			} catch (StringIndexOutOfBoundsException e) {
				beanListName = StringUtils.uncapitalize(beanClassName).concat(
						"List");
				System.out.println(beanListName);
			}
			List<Serializable> scrollList = (List<Serializable>) DaoBase
					.getTable((Class<T>) bean, request);
			System.out.println(scrollList);
			request.setAttribute(beanListName, scrollList);
		}
	}

	/**
	 * @param <T>
	 * @param request
	 * @throws SystemErrorException
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> void getClassNecessaryaForCreate(
			HttpServletRequest request) throws SystemErrorException {

		String key = request.getParameter("primary");

		List<StockSearcher> stockSearcherList = new ArrayList<StockSearcher>();
		OrderIndex orderIndex = DaoBase.keySearch(OrderIndex.class, key,
				request);
		Set<OrderDetails> orderDetailsSet = orderIndex.getOrderDetailsSet();
		Iterator<OrderDetails> iterator = orderDetailsSet.iterator();
		OrderDetails orderDetails = null;
		Date ordHopeDeliveryDate = null;
		String goodsCode = null;

		StockSearcher stockSearcher = null;
		StockAccordingWareHousePK stockAccordingWareHousePK = null;
		StockAccordingWareHouse stockAccordingWarehouse = null;

		while (iterator.hasNext()) {
			orderDetails = iterator.next();
			ordHopeDeliveryDate = orderDetails.getOrdHopeDeliveryDate();
			goodsCode = orderDetails.getGoodsCode();

			stockAccordingWareHousePK = new StockAccordingWareHousePK();

			stockAccordingWareHousePK.setGoodsCode(orderDetails.getGoodsCode());
			stockAccordingWareHousePK.setWareHouseCode(orderDetails
					.getWareHouseCode());

			stockAccordingWarehouse = DaoBase.keySearch(
					StockAccordingWareHouse.class, stockAccordingWareHousePK,
					request);

			stockSearcher = new StockSearcher();
			stockSearcher.setGoodsCode(goodsCode);
			stockSearcher.setNumberThatHasBeenDrawn(stockAccordingWarehouse
					.getNumberThatHasBeenDrawn());
			stockSearcher.setOrdHopeDeliveryDate(ordHopeDeliveryDate);
			stockSearcher.setOrdNumberOfOrder(orderDetails
					.getOrdNumberOfOrder());
			stockSearcher.setWarehouseCode(orderDetails.getWareHouseCode());
			stockSearcher.setOrdIndexNo(orderDetails.getOrderDetailsPK()
					.getOrdIndexNo());
			stockSearcherList.add(stockSearcher);
		}

		request.setAttribute("searchedList", stockSearcherList);
	}

	// 売上管理のDB処理
	private static <T extends Serializable> void salesSearch(
			HttpServletRequest request, String action) throws ServletException {
		String flag = request.getParameter("search");// ボタンを押してるか否か

		List<T> searchedList = null;
		// ▽売上見出照会/////////////////////////////////////////////////////////ここから▽
		if (StringUtils.contains(action, "Caption")) {
			if (flag != null) {
				searchedList = (List<T>) DaoBase.search(SalesHeadline.class,
						"salCustomerCode", request.getParameter("cusCode"),
						request);
			} else {
				searchedList = (List<T>) DaoBase.getTable(SalesHeadline.class,
						request);
			}
		}
		// △売上見出照会/////////////////////////////////////////////////////////ここまで△

		// ▽売上明細照会/////////////////////////////////////////////////////////ここから▽
		if (StringUtils.contains(action, "Detail")) {
			List<SalesHeadline> salesHeadlineList = null;
			Set<SalesDetails> salesDetailsSet = null;
			if (flag != null) {
				searchedList = (List<T>) new ArrayList<SalesDetails>();
				salesHeadlineList = DaoBase.search(SalesHeadline.class,
						"salCustomerCode", request.getParameter("cusCode"),
						request);
				Iterator<SalesDetails> iterator = null;
				for (SalesHeadline salesHeadline : salesHeadlineList) {
					salesDetailsSet = salesHeadline.getSalesDetailsSet();
					if (salesDetailsSet != null) {
						iterator = salesDetailsSet.iterator();
						while (iterator.hasNext()) {
							searchedList.add((T) iterator.next());
						}
					}
				}
			} else {
				searchedList = (List<T>) DaoBase.getTable(SalesDetails.class,
						request);
			}
		}
		// △売上明細照会/////////////////////////////////////////////////////////ここまで△

		// ▽顧別総額照会/////////////////////////////////////////////////////////ここから▽
		if (StringUtils.contains(action, "Client")) {
			String check = request.getParameter("check");
			Customer customer = null;
			List<SalesHeadline> salesHeadlineList = null;
			if (flag != null && check.equals("cus")) {// ///////企業検索

				searchedList = (List<T>) CustomerListCreator.createList(DaoBase
						.search(SalesHeadline.class, "salCustomerCode", request
								.getParameter("cusCode"), request), request);

			} else if (flag != null && check.equals("salHeadNo")) {// ///////売上No検索

				searchedList = (List<T>) CustomerListCreator.createList(DaoBase
						.search(SalesHeadline.class, "salHeadNo", request
								.getParameter("salHeadNo"), request), request);
			} else {
				searchedList = (List<T>) CustomerListCreator.createList(DaoBase
						.getTable(SalesHeadline.class, request), request);
			}
			// BeanUtils.compareAndSetBean(Customer.class, o1, o2)
		}
		// △顧別総額照会/////////////////////////////////////////////////////////ここまで△
		request.setAttribute("searchedList", searchedList);
	}

	// 受注のDB処理
	private static <T extends Serializable> void orderSearch(
			HttpServletRequest request, String action) throws ServletException {
		String flag = request.getParameter("search");// ボタンを押してるか否か

		List<T> searchedList = null;
		// ▽受注明細照会 と 受注明細修正一覧////////////////////////////////////ここから▽
		if (StringUtils.contains(action, "search")
				|| (StringUtils.contains(action, "update"))
				&& request.getParameter("radio") == null) {
			if (flag != null) {
				searchedList = (List<T>) OrderListCreator
						.createListForOrderDetailOfSearch(request);

				if (searchedList.isEmpty()) {
					request.setAttribute("headMessage", "その受注は存在しません。");
				}
				if (StringUtils.isBlank(request.getParameter("ordIndexNo"))) {
					request.setAttribute("headMessage", "何も入力されていません。");
				}
				request.setAttribute("ordIndexNo", request
						.getParameter("ordIndexNo"));

			} else {
				searchedList = (List<T>) OrderListCreator
						.createListForOrderDetail(request);
			}
		}
		// △受注明細照会 と 受注明細修正一覧////////////////////////////////////ここまで△
		request.setAttribute("searchedList", searchedList);
	}

	// 受注見出修正用検索
	private static <T extends Serializable> void orderSearchOfCaption(
			HttpServletRequest request, String action)
			throws SystemErrorException {
		String flag = request.getParameter("search");
		List<T> searchedList = null;
		// ▽受注明細照会 と 受注明細修正一覧////////////////////////////////////ここから▽
		if ((StringUtils.contains(action, "Pre"))
				&& request.getParameter("radio") == null) {
			if (flag != null) {
				searchedList = (List<T>) OrderListCreator
						.createListForOderCaption(OrderListCreator
								.createSearchedListForOderCaption(request),
								request);

				if (searchedList.isEmpty()) {
					request.setAttribute("headMessage", "その受注は存在しません。");
				}
				if (StringUtils.isBlank(request.getParameter(flag))) {
					request.setAttribute("headMessage", "何も入力されていません。");
				}
				request.setAttribute("ordIndexNo", request
						.getParameter("ordIndexNo"));

			} else {
				searchedList = (List<T>) OrderListCreator
						.createListForOderCaption(request);
			}
			// △受注明細照会 と 受注明細修正一覧////////////////////////////////////ここまで△
			request.setAttribute("searchedList", searchedList);
		}
	}
}