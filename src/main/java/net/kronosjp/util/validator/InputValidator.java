package net.kronosjp.util.validator;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.validator.ClassValidator;
import org.hibernate.validator.InvalidValue;

import net.kronosjp.exception.InvalidInputException;
import net.kronosjp.util.bean.BeanCreator;

/**
 * 入力の妥当性検証
 * 
 * @author Norihiko Yoshida
 * 
 * @version 1.0
 * 
 */
public class InputValidator {

	/**
	 * 入力を検証した結果を、request.setAttribute("errorMessageMap", errorMassageMap);する。
	 * errorMassageMapはjava.util.HashMap<key,value>である。
	 * key、valueはjava.lang.Stirng型であり、それぞれパラメータの名前、エラーメッセージに当たる。
	 * 
	 * @param <T>検証したいクラスの型
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param klass
	 *            検証したいクラス
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws InvalidInputException
	 *             入力が妥当で無い場合
	 */
	public static <T extends Serializable> void validate(
			HttpServletRequest request, Class<T> klass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			InstantiationException, InvalidInputException {

		T bean = BeanCreator.createBean(request, klass);

		ClassValidator<T> classValidator = new ClassValidator<T>(klass);
		InvalidValue[] validationMessages = classValidator
				.getInvalidValues(bean);

		Field[] fields = klass.getDeclaredFields();

		Map<String, String> errorMassageMap = new HashMap<String, String>();
		if (validationMessages.length != 0) {
			String[] message = null;
			String fieldName = null;
			for (InvalidValue invalidValue : validationMessages) {
				message = invalidValue.getMessage().split(":");
				for (Field field : fields) {
					fieldName = field.getName();
					if (message[0].matches(fieldName)) {
						if(errorMassageMap.containsKey(fieldName)){
							message[1]= message[1]+ errorMassageMap.get(fieldName);
						}
						errorMassageMap.put(fieldName, (message[1]));
					}
				}
			}
			request.setAttribute("errorMessageMap", errorMassageMap);
			throw new InvalidInputException();
		}
		request.setAttribute("errorMessageMap", errorMassageMap);
	}
}
