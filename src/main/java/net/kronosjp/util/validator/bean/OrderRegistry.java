package net.kronosjp.util.validator.bean;

import java.io.Serializable;

import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.Pattern;
import org.hibernate.validator.Patterns;

/**
 * 社員登録関係validator用Bean
 * @author Norihiko Yoshida
 * @version
 */
public class OrderRegistry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3923910574834366604L;
	@Length(max=10,message="社員名は20字までで登録してください。")
	@NotEmpty(message = "empName:社員名が入力されていません。")
	@Patterns( { @Pattern(regex = "[^\\p{Punct}！”＃＄％＆’（）＊＋，−．／：；＜＝＞？＠［￥］＾＿｀｛｜｝〜、。]*", message = "empName:不正な文字が入力されました。") })
	private String empName;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}
}