package net.kronosjp.util.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Norihiko Yoshida
 *
 * @version 1.0
 *
 */
public class CollectionUtils {
	/**
	 * @param <T>
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static <T> List<T> createComparedList(List<T> list1, List<T> list2) {
		List<T> comparedList = new ArrayList<T>();
		for (T t1 : list1) {

			for (T t2 : list2) {
				if (t1.equals(t2)) {
					comparedList.add(t1);
				}
			}
		}
		return comparedList;
	}

	/**
	 * @param <T>
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static <T> List<T> createComparedList(T[] array1, T[] array2) {
		List<T> list1 = Arrays.asList(array1);
		List<T> list2 = Arrays.asList(array2);
		return createComparedList(list1, list2);
	}

//	public static <T> T[] createComparedArray(T[] array1, T[] array2) {
//		List<T> list1 = Arrays.asList(array1);
//		List<T> list2 = Arrays.asList(array2);
//		return (T[]) createComparedList(list1, list2).toArray();
//	}
//
//	public static <T> T[] createComparedArray(List<T> list1, List<T> list2) {
//		return (T[]) createComparedList(list1, list2).toArray();
//	}

}
