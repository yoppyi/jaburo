package net.kronosjp.util;


import java.io.FileNotFoundException;
import java.io.IOException;

import net.kronosjp.util.properties.PropertiesFileReader;

/**
 * プロパティファイルからエラーメッセージを取得
 * 
 * @author Norihiko Yoshida
 * @version 1.0
 * 
 */
public class ErrorMessageGetter {

	private static PropertiesFileReader reader;

	/**
	 * プロパティファイルからエラーメッセージを取得
	 * 
	 * @param key
	 *            プロパティファイルのエラーメッセージに関連づけられたキー
	 * @return エラーメッセージ
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String getErroerMassage(String key)
			throws FileNotFoundException, IOException {
		
		if (reader == null) {
			reader = new PropertiesFileReader("errorMessage.properties");
		}

		return reader.read(key);
	}
}
