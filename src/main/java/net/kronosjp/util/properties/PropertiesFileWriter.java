package net.kronosjp.util.properties;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * プロパティファイルの書き込み
 * 
 * @author Norihiko Yoshida
 * @version 1.0
 */
public class PropertiesFileWriter {

	private String propertiesFileName;

	/**
	 * @param propertiesFileName
	 *            書き込みたいプロパティファイルの名前
	 */
	public PropertiesFileWriter(String propertiesFileName) {
		this.propertiesFileName = propertiesFileName;
	}

	
	/**
	 * プロパティファイルにプロパティを書き込む
	 * 
	 * @param key
	 *            書き込みたいプロパティのkey
	 * @param value
	 *            書き込みたいプロパティの値
	 * @param description
	 *            書き込みたいプロパティのヘッダー（説明）
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void Write(String key, String value, String description)
			throws FileNotFoundException, IOException {
		Properties properties = new Properties();
		properties.setProperty(key, value);
		FileOutputStream stream = new FileOutputStream(propertiesFileName);
		properties.store(stream, description);
		stream.close();
	}
}
