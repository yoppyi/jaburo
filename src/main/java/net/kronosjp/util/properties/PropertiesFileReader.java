package net.kronosjp.util.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * プロパティファイルの読み込み
 * 
 * @author Norihiko Yoshida
 * 
 * @version 1.0
 * 
 */
public class PropertiesFileReader {

	private String propertiesFileName;

	/**
	 * @param propertiesFileName
	 *            読み込みたいプロパティファイル名
	 */
	public PropertiesFileReader(String propertiesFileName) {
		this.propertiesFileName = propertiesFileName;

	}

	/**
	 * プロパティファイルからプロパティを読み込む
	 * 
	 * @param key
	 *            読み込みたいプロパティのkey
	 * @return 読み込んだプロパティの値
	 * @throws IOException
	 */
	public String read(String key) throws IOException {
		Properties properties = new Properties();
		FileInputStream stream = new FileInputStream(propertiesFileName);
		properties.load(stream);

		return properties.getProperty(key);
	}

}
