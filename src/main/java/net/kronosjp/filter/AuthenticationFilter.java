package net.kronosjp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.kronosjp.db.entity.EmployeeMaster;

public class AuthenticationFilter implements Filter {

	private static String rollId; // このフィルタでアクセスを許可するアクセス権限

	public void init(FilterConfig config) throws ServletException {
		rollId = config.getInitParameter("rollId");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession(false);
		if (session != null) {
			EmployeeMaster em = (EmployeeMaster) session.getAttribute("EmployeeMaster");
			if (em != null && em.getRollId() == rollId) {
				chain.doFilter(httpRequest, httpResponse);
				return;
			}
		}
		request.setAttribute("rollId", rollId);
	}

	public void destroy() {
	}
}