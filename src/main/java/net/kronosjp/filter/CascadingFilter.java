package net.kronosjp.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;


import net.kronosjp.db.entity.EmployeeMaster;

public class CascadingFilter implements Filter {

	public void init(FilterConfig config) throws ServletException {
	}

	@SuppressWarnings("unchecked")
	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		Map<String, String[]> paramMap = request.getParameterMap();
		Enumeration paramNameEnum = request.getParameterNames();
		System.out.println("フィルターかかってるよ");

		String key = null;
		String[] valueArray = new String[paramMap.size()];
		String value = null;

		try {
			while (paramNameEnum.hasMoreElements()) {

				key = paramNameEnum.nextElement().toString();

				if ("empName".equals(key)) {
					valueArray = paramMap.get(key);
					value = valueArray[0];

					System.out.println(key);
					System.out.println(value);
					if (StringUtils.contains(value, "<")) {
						value = StringUtils.replace(value, "<", "&lt;");
					}
					if (StringUtils.contains(value, ">")) {
						value = StringUtils.replace(value, ">", "&gt;");
					}
					if (StringUtils.contains(value, "&")) {
						value = StringUtils.replace(value, "&", "&amp;");
					}
					if (StringUtils.contains(value, "\"")) {
						value = StringUtils.replace(value, "\"", "&quot;");
					}
					if (StringUtils.contains(value, "\'")) {
						value = StringUtils.replace(value, "\'", "&#39;");
					}
				}
			}
			request.setAttribute("param", value);
		} catch (ClassCastException e) {
			e.printStackTrace();
		}

		chain.doFilter(request, response);
	}

	public void destroy() {
	}
}