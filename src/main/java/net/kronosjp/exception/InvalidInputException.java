package net.kronosjp.exception;

/**
 * 入力の妥当性検証エラー
 * 
 * @author Norihiko Yoshida
 * @version 1.0
 *
 */
public class InvalidInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 153125813041610909L;

}
