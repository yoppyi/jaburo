package net.kronosjp.exception;

import javax.servlet.ServletException;

/**
 * 
 * @author Norihiko Yoshida
 * @version
 */
public class SystemErrorException extends ServletException {
	public SystemErrorException() {
		// TODO Auto-generated constructor stub
	}

	public SystemErrorException(Throwable casus) {
		super(casus);
	}

}
