package net.kronosjp.controller;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.kronosjp.account.AuthenticationChecker;
import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.util.utiltes.ActionCharacter;
import net.kronosjp.util.utiltes.InvolveWithCreate;
import net.kronosjp.util.utiltes.InvolveWithDelete;
import net.kronosjp.util.utiltes.InvolveWithGetEntityClass;
import net.kronosjp.util.utiltes.InvolveWithSearch;
import net.kronosjp.util.utiltes.InvolveWithUpdate;

/**
 * 【Jaburo】の【Front Controller】 画面遷移処理の振り分け
 * 
 * @param <T>
 */
public class FrontServlet<T> extends HttpServlet implements Servlet {

	@Override
	public void init() throws ServletException {
		DaoBase.createSessionFactory();
		super.init();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * リクエストのURIによっての処理の振り分け
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("windows-31j");
		// JSPによってリクエストされたURIから次のアクションを決定するアクション文字列を取得
		String action = request.getRequestURI();// リクエストURI
		action = action.substring(action.lastIndexOf("/") + 1);
		
		///////////////////////////////////////////////////////////////////
				System.out.println("-------------" + action + "----------------");
		///////////////////////////////////////////////////////////////////
		
		// ログインする際の共通処理
		if ("authenticationCheck.do".equals(action)) {

			action = AuthenticationChecker.authenticationCheck(request,
					response);
		}
		
		// ---登録する際の共通処理---
		if (action.endsWith("create.do")) {
			System.out.println("共通登録処理");
			
			InvolveWithCreate.doCreatePre(request, action);
		}

		
		// ---検索する際の共通処理---
		if (action.endsWith("oneSearchPre.do")) {
			System.out.println("共通検索処理");
			String initialOfAction = ActionCharacter.actionsWordChange(action);
			InvolveWithGetEntityClass.getClassNecessaryForCreate(request, initialOfAction);
		}

		// ---削除する際の共通処理---
		if (action.endsWith("delete.do")) {
			System.out.println("共通削除処理");

			action = InvolveWithDelete.doDeretePre(request, action);
		}

		// ---更新する際の共通処理---
		if (action.endsWith("update.do")) {
			System.out.println("共通更新処理");

			action = InvolveWithUpdate.doUpdatePre(request, action);
		}
		
		// ---検索選択項目の共通処理---
		if (action.endsWith("searchPre.do")) {
			System.out.println("共通検索選択項目処理");

			InvolveWithSearch.doSearchPre(request, action);
		}
		
		// 検索画面表示する際の共通処理
		if (action.endsWith("search.do") || action.endsWith("deletePre.do") || action.endsWith("updatePre.do")) {
			System.out.println("共通検索画面表示処理");
			
			InvolveWithGetEntityClass.getClassNecessaryForDisplay(request, action);
		}
		
		///////////////////////////////////////////////////////////////////
				System.out.println("現在のURI：" + action);
		///////////////////////////////////////////////////////////////////
		
		request.getRequestDispatcher(getInitParameter(action)).forward(request,
				response);
	}
}