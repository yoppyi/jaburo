package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * �󒍖���
 * 
 * @author Masato Ejima
 * @version
 */

public class OrderDetails implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8638223032234019555L;
	private String ordIndexNo;
	private Short ordDetRowNo;
	private String goodsCode;
	private String wareHouseCode;
	private Integer ordNumberOfOrder;
	private Date ordHopeDeliveryDate;
	private String oDStatus;
	private OrderDetailsPK orderDetailsPK;
	private Set<ShipmentDetails> shipmentDetailsSet;
	private Set<OrderIndex> orderIndexSet;
	private Set<StockAccordingWareHouse> stockAccordingWarehouseSet;

	public OrderDetailsPK getPK() {
		return orderDetailsPK;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getWareHouseCode() {
		return wareHouseCode;
	}

	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}

	public Integer getOrdNumberOfOrder() {
		return ordNumberOfOrder;
	}

	public void setOrdNumberOfOrder(Integer ordNumberOfOrder) {
		this.ordNumberOfOrder = ordNumberOfOrder;
	}

	public Date getOrdHopeDeliveryDate() {
		return ordHopeDeliveryDate;
	}

	public void setOrdHopeDeliveryDate(Date ordHopeDeliveryDate) {
		this.ordHopeDeliveryDate = ordHopeDeliveryDate;
	}

	public String getoDStatus() {
		return oDStatus;
	}

	public void setoDStatus(String oDStatus) {
		this.oDStatus = oDStatus;
	}

	public Set<ShipmentDetails> getShipmentDetailsSet() {
		return shipmentDetailsSet;
	}

	public void setShipmentDetailsSet(Set<ShipmentDetails> shipmentDetailsSet) {
		this.shipmentDetailsSet = shipmentDetailsSet;
	}

	public Set<OrderIndex> getOrderIndexSet() {
		return orderIndexSet;
	}

	public void setOrderIndexSet(Set<OrderIndex> orderIndexSet) {
		this.orderIndexSet = orderIndexSet;
	}

	public Set<StockAccordingWareHouse> getStockAccordingWarehouseSet() {
		return stockAccordingWarehouseSet;
	}

	public void setStockAccordingWarehouseSet(
			Set<StockAccordingWareHouse> stockAccordingWarehouseSet) {
		this.stockAccordingWarehouseSet = stockAccordingWarehouseSet;
	}

	public OrderDetailsPK getOrderDetailsPK() {
		return orderDetailsPK;
	}

	public void setOrderDetailsPK(OrderDetailsPK orderDetailsPK) {
		this.orderDetailsPK = orderDetailsPK;
		ordDetRowNo = orderDetailsPK.getOrdDetRowNo();
		ordIndexNo = orderDetailsPK.getOrdIndexNo();
	}

	public String getODStatus() {
		return oDStatus;
	}

	public void setODStatus(String status) {
		oDStatus = status;
	}

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public Short getOrdDetRowNo() {
		return ordDetRowNo;
	}

	public void setOrdDetRowNo(Short ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}

}
