package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 請求先別月次取引サマリ
 * 
 * @author Makoto Uenishi
 * @version
 */

public class BillMonthlyTradingSummary implements Serializable  ,PK{

	/**
	 * 
	 */
	private static final long serialVersionUID = -108289364342406729L;
	private String biladdCode;
	private Date bsYearMonth;
	private Integer bsBilTotalThisMonth;
	private Integer bsLastMonthBalance;
	private Integer bsMonRecTotalThisMonth;
	private BillMonthlyTradingSummaryPK billMonthlyTradingSummaryPK;
	private Set<BillAddressMaster> billAddressMasterSet;
	
	public BillMonthlyTradingSummaryPK getPK(){
		return billMonthlyTradingSummaryPK;
	}

	public String getBiladdCode() {
		return biladdCode;
	}

	public void setBiladdCode(String biladdCode) {
		this.biladdCode = biladdCode;
	}

	public Date getbsYearMonth() {
		return bsYearMonth;
	}

	public void setbsYearMonth(Date bsYearMonth) {
		this.bsYearMonth = bsYearMonth;
	}

	public Integer getBsBilTotalThisMonth() {
		return bsBilTotalThisMonth;
	}

	public void setBsBilTotalThisMonth(Integer bsBilTotalThisMonth) {
		this.bsBilTotalThisMonth = bsBilTotalThisMonth;
	}

	public Integer getBsLastMonthBalance() {
		return bsLastMonthBalance;
	}

	public void setBsLastMonthBalance(Integer bsLastMonthBalance) {
		this.bsLastMonthBalance = bsLastMonthBalance;
	}

	public Integer getBsMonRecTotalThisMonth() {
		return bsMonRecTotalThisMonth;
	}

	public void setBsMonRecTotalThisMonth(Integer bsMonRecTotalThisMonth) {
		this.bsMonRecTotalThisMonth = bsMonRecTotalThisMonth;
	}

	public BillMonthlyTradingSummaryPK getBillMonthlyTradingSummaryPK() {
		return billMonthlyTradingSummaryPK;
	}

	public void setBillMonthlyTradingSummaryPK(
			BillMonthlyTradingSummaryPK billMonthlyTradingSummaryPK) {
		this.billMonthlyTradingSummaryPK = billMonthlyTradingSummaryPK;
	}

	public Set<BillAddressMaster> getBillAddressMasterSet() {
		return billAddressMasterSet;
	}

	public void setBillAddressMasterSet(Set<BillAddressMaster> billAddressMasterSet) {
		this.billAddressMasterSet = billAddressMasterSet;
	}

	public Date getBsYearMonth() {
		return bsYearMonth;
	}

	public void setBsYearMonth(Date bsYearMonth) {
		this.bsYearMonth = bsYearMonth;
	}

}
