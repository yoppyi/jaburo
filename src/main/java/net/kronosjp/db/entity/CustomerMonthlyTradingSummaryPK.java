package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

public class CustomerMonthlyTradingSummaryPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -543838785129027081L;
	private Date cusYearMonth;
	private String cliCode;

	public Date getCusYearMonth() {
		return cusYearMonth;
	}

	public void setCusYearMonth(Date cusYearMonth) {
		this.cusYearMonth = cusYearMonth;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

}
