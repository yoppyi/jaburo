package net.kronosjp.db.entity;

import java.io.Serializable;

public class StockTransitionAccordingToDayPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3727869906989538091L;
	private String goodsCode;
	private String sTBaseDate;

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getsTBaseDate() {
		return sTBaseDate;
	}

	public void setsTBaseDate(String baseDate) {
		sTBaseDate = baseDate;
	}

	public String getSTBaseDate() {
		return sTBaseDate;
	}

	public void setSTBaseDate(String baseDate) {
		sTBaseDate = baseDate;
	}

}
