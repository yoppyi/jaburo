package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 入金見出し
 * 
 * @author Makoto Uenishi
 * @version
 */

public class MoneyReceivedHeadLine implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7673239935504497793L;
	private String monRecNo;
	private String demAddCode;
	private String denomiNation;
	private Date monRecDay;
	private Integer monRecAmount;
	private ChargeBackDetails chargeBackDetailsSet;

	public String getPK() {
		return monRecNo;
	}

	public String getMonRecNo() {
		return monRecNo;
	}

	public void setMonRecNo(String monRecNo) {
		this.monRecNo = monRecNo;
	}

	public String getDemAddCode() {
		return demAddCode;
	}

	public void setDemAddCode(String demAddCode) {
		this.demAddCode = demAddCode;
	}

	public String getDenomiNation() {
		return denomiNation;
	}

	public void setDenomiNation(String denomiNation) {
		this.denomiNation = denomiNation;
	}

	public Date getMonRecDay() {
		return monRecDay;
	}

	public void setMonRecDay(Date monRecDay) {
		this.monRecDay = monRecDay;
	}

	public ChargeBackDetails getChargeBackDetailsSet() {
		return chargeBackDetailsSet;
	}

	public void setChargeBackDetailsSet(ChargeBackDetails chargeBackDetailsSet) {
		this.chargeBackDetailsSet = chargeBackDetailsSet;
	}

	public Integer getMonRecAmount() {
		return monRecAmount;
	}

	public void setMonRecAmount(Integer monRecAmount) {
		this.monRecAmount = monRecAmount;
	}

}
