package net.kronosjp.db.entity;

import java.io.Serializable;

public class SalesDetailsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2135651877873176614L;
	private String salHeadNo;
	private Short salRowNo;

	public String getSalHeadNo() {
		return salHeadNo;
	}

	public void setSalHeadNo(String salHeadNo) {
		this.salHeadNo = salHeadNo;
	}

	public Short getSalRowNo() {
		return salRowNo;
	}

	public void setSalRowNo(Short salRowNo) {
		this.salRowNo = salRowNo;
	}

}
