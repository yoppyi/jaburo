package net.kronosjp.db.entity;

import java.io.Serializable;

public class OrderDetailsPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4647994025660848865L;
	private String ordIndexNo;
	private Short ordDetRowNo;

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public Short getOrdDetRowNo() {
		return ordDetRowNo;
	}

	public void setOrdDetRowNo(Short ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ordDetRowNo.toString();
	}

}
