package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * 出荷先マスタ
 * 
 * @author Masato Ejima
 * @version
 */

public class ShipDestinationMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -753133071910402363L;
	private String shipDesCode;
	private String shipReceivingCheckPerson;

	public String getPK() {
		return shipDesCode;
	}

	public String getShipDesCode() {
		return shipDesCode;
	}

	public void setShipDesCode(String shipDesCode) {
		this.shipDesCode = shipDesCode;
	}

	public String getShipReceivingCheckPerson() {
		return shipReceivingCheckPerson;
	}

	public void setShipReceivingCheckPerson(String shipReceivingCheckPerson) {
		this.shipReceivingCheckPerson = shipReceivingCheckPerson;
	}
}
