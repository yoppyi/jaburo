package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * 取引先マスタ
 * 
 * @author Masato Ejima
 * @version
 * 
 */

public class CustomerMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3371093485287847811L;
	private String cusCode;
	private String cusName;
	private String cusAddress;
	private String cusTel;
	private String cusEGCode;
	private boolean cusObjectDivision;
	private boolean cusShipDivision;
	private boolean cusSupplierDivision;
	private boolean cusBilDivision;
	private boolean cusPayDivision;
	private EnterpriseGroupMaster enterpriseGroupMaster;
	private ShipDestinationMaster shipDestinationMaster;

	public String getPK() {
		return cusCode;
	}

	public ShipDestinationMaster getShipDestinationMaster() {
		return shipDestinationMaster;
	}

	public void setShipDestinationMaster(
			ShipDestinationMaster shipDestinationMaster) {
		this.shipDestinationMaster = shipDestinationMaster;
	}

	public String getCusCode() {
		return cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusAddress() {
		return cusAddress;
	}

	public void setCusAddress(String cusAddress) {
		this.cusAddress = cusAddress;
	}

	public String getCusTel() {
		return cusTel;
	}

	public void setCusTel(String cusTel) {
		this.cusTel = cusTel;
	}

	public String getCusEGCode() {
		return cusEGCode;
	}

	public void setCusEGCode(String cusEGCode) {
		this.cusEGCode = cusEGCode;
	}

	public boolean isCusObjectDivision() {
		return cusObjectDivision;
	}

	public void setCusObjectDivision(boolean cusObjectDivision) {
		this.cusObjectDivision = cusObjectDivision;
	}

	public boolean isCusShipDivision() {
		return cusShipDivision;
	}

	public void setCusShipDivision(boolean cusShipDivision) {
		this.cusShipDivision = cusShipDivision;
	}

	public boolean isCusSupplierDivision() {
		return cusSupplierDivision;
	}

	public void setCusSupplierDivision(boolean cusSupplierDivision) {
		this.cusSupplierDivision = cusSupplierDivision;
	}

	public boolean isCusBilDivision() {
		return cusBilDivision;
	}

	public void setCusBilDivision(boolean cusBilDivision) {
		this.cusBilDivision = cusBilDivision;
	}

	public boolean isCusPayDivision() {
		return cusPayDivision;
	}

	public void setCusPayDivision(boolean cusPayDivision) {
		this.cusPayDivision = cusPayDivision;
	}

	public EnterpriseGroupMaster getEnterpriseGroupMaster() {
		return enterpriseGroupMaster;
	}

	public void setEnterpriseGroupMaster(
			EnterpriseGroupMaster enterpriseGroupMaster) {
		this.enterpriseGroupMaster = enterpriseGroupMaster;
	}

}
