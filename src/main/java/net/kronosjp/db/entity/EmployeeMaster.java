package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * �Ј��}�X�^
 * 
 * @author Norihiko Yoshida
 * @version
 * 
 */
public class EmployeeMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5971101763582767219L;
	private String empCode;
	private String empName;
	private String rollId;
	private String empPass;
	private RollMaster rollMaster;
	private Set<ClientMaster> clientMasterSet;
	private Set<OrderIndex> orderIndexSet;

	public String getPK() {
		return empCode;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getRollId() {
		return rollId;
	}

	public void setRollId(String rollId) {
		this.rollId = rollId;
	}

	public String getEmpPass() {
		return empPass;
	}

	public void setEmpPass(String empPass) {
		this.empPass = empPass;
	}

	public RollMaster getRollMaster() {
		return rollMaster;
	}

	public void setRollMaster(RollMaster rollMaster) {
		this.rollMaster = rollMaster;
	}

	public Set<ClientMaster> getClientMasterSet() {
		return clientMasterSet;
	}

	public void setClientMasterSet(Set<ClientMaster> clientMasterSet) {
		this.clientMasterSet = clientMasterSet;
	}

	public Set<OrderIndex> getOrderIndexSet() {
		return orderIndexSet;
	}

	public void setOrderIndexSet(Set<OrderIndex> orderIndexSet) {
		this.orderIndexSet = orderIndexSet;
	}

}
