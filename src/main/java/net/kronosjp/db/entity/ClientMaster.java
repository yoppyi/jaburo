package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * �ڋq�}�X�^
 * 
 * @author Masato Ejima
 * @version
 */

public class ClientMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7313733034947847365L;
	private String cliCode;
	private String cliPName;
	private String cliSalesCode;
	private String cliBilCode;
	private String cliDesCode;
	private EmployeeMaster employeeMasterSet;
	private OrderIndex orderIndexSet;

	public String getPK() {
		return cliCode;
	}

	public OrderIndex getOrderIndexSet() {
		return orderIndexSet;
	}

	public void setOrderIndexSet(OrderIndex orderIndexSet) {
		this.orderIndexSet = orderIndexSet;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

	public String getCliPName() {
		return cliPName;
	}

	public void setCliPName(String cliPName) {
		this.cliPName = cliPName;
	}

	public String getCliSalesCode() {
		return cliSalesCode;
	}

	public void setCliSalesCode(String cliSalesCode) {
		this.cliSalesCode = cliSalesCode;
	}

	public String getCliBilCode() {
		return cliBilCode;
	}

	public void setCliBilCode(String cliBilCode) {
		this.cliBilCode = cliBilCode;
	}

	public String getCliDesCode() {
		return cliDesCode;
	}

	public void setCliDesCode(String cliDesCode) {
		this.cliDesCode = cliDesCode;
	}

	public EmployeeMaster getEmployeeMasterSet() {
		return employeeMasterSet;
	}

	public void setEmployeeMasterSet(EmployeeMaster employeeMasterSet) {
		this.employeeMasterSet = employeeMasterSet;
	}
}
