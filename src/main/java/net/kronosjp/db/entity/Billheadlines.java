package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * �������o
 * 
 * @author Makoto Uenishi
 * @version
 */

public class Billheadlines implements Serializable  ,PK{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1234858579250034057L;
	private String bilHeadNo;
	private String bilAddCode;
	private Date bilDate;
	private Date dateOfSummingUp;
	private Integer bilAmount;
	private Integer erasingAmount;
	private ChargeBackDetails chargeBackDetailaSet;
	private SalesHeadline salesHeadlineSet;
	
	public String getPK(){
		return bilHeadNo;
	}
	
	public String getBilHeadNo() {
		return bilHeadNo;
	}
	public void setBilHeadNo(String bilHeadNo) {
		this.bilHeadNo = bilHeadNo;
	}
	public String getBilAddCode() {
		return bilAddCode;
	}
	public void setBilAddCode(String bilAddCode) {
		this.bilAddCode = bilAddCode;
	}
	public Date getBilDate() {
		return bilDate;
	}
	public void setBilDate(Date bilDate) {
		this.bilDate = bilDate;
	}
	public Date getDateOfSummingUp() {
		return dateOfSummingUp;
	}
	public void setDateOfSummingUp(Date dateOfSummingUp) {
		this.dateOfSummingUp = dateOfSummingUp;
	}
	public Integer getBilAmount() {
		return bilAmount;
	}
	public void setBilAmount(Integer bilAmount) {
		this.bilAmount = bilAmount;
	}
	public Integer getErasingAmount() {
		return erasingAmount;
	}
	public void setErasingAmount(Integer erasingAmount) {
		this.erasingAmount = erasingAmount;
	}
	public ChargeBackDetails getChargeBackDetailaSet() {
		return chargeBackDetailaSet;
	}
	public void setChargeBackDetailaSet(ChargeBackDetails chargeBackDetailaSet) {
		this.chargeBackDetailaSet = chargeBackDetailaSet;
	}
	public SalesHeadline getSalesHeadlineSet() {
		return salesHeadlineSet;
	}
	public void setSalesHeadlineSet(SalesHeadline salesHeadlineSet) {
		this.salesHeadlineSet = salesHeadlineSet;
	}
	

	

}
