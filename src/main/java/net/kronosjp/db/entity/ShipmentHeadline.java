package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * �o�׌��o��
 * 
 * @author Makoto Uenishi
 * @version
 */

public class ShipmentHeadline implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2374162413747544195L;
	private String shipHeadNo;
	private Date shipDay;
	private String carCode;
	private String shipHeadInquiry;
	private String warHouseCode;
	private String shipDesCode;
	private Set<ShipmentDetails> shipmentDetailsSet;

	public String getPK() {
		return shipHeadNo;
	}

	public Date getShipDay() {
		return shipDay;
	}

	public void setShipDay(Date shipDay) {
		this.shipDay = shipDay;
	}

	public String getCarCode() {
		return carCode;
	}

	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	public String getShipHeadInquiry() {
		return shipHeadInquiry;
	}

	public void setShipHeadInquiry(String shipHeadInquiry) {
		this.shipHeadInquiry = shipHeadInquiry;
	}

	public String getWarHouseCode() {
		return warHouseCode;
	}

	public void setWarHouseCode(String warHouseCode) {
		this.warHouseCode = warHouseCode;
	}

	public String getShipDesCode() {
		return shipDesCode;
	}

	public void setShipDesCode(String shipDesCode) {
		this.shipDesCode = shipDesCode;
	}

	public Set<ShipmentDetails> getShipmentDetailsSet() {
		return shipmentDetailsSet;
	}

	public void setShipmentDetailsSet(Set<ShipmentDetails> shipmentDetailsSet) {
		this.shipmentDetailsSet = shipmentDetailsSet;
	}

	public String getShipHeadNo() {
		return shipHeadNo;
	}

	public void setShipHeadNo(String shipHeadNo) {
		this.shipHeadNo = shipHeadNo;
	}

}
