package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 顧客別月次取引サマリ
 * 
 * @author Makoto Uenishi
 * @version
 */

public class CustomerMonthlyTradingSummary implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7879934174802372767L;
	private String cliCode;
	private Date cusYearMonth;
	private Integer salesMonthTotal;
	private CustomerMonthlyTradingSummaryPK customerMonthlyTradingSummaryPK;
	private Set<SalesHeadline> salesHeadlineSet;
	private ClientMaster clientMasterSet;

	public CustomerMonthlyTradingSummaryPK getPK() {
		return customerMonthlyTradingSummaryPK;
	}

	public Set<SalesHeadline> getSalesHeadlineSet() {
		return salesHeadlineSet;
	}

	public void setSalesHeadlineSet(Set<SalesHeadline> salesHeadlineSet) {
		this.salesHeadlineSet = salesHeadlineSet;
	}

	public ClientMaster getClientMasterSet() {
		return clientMasterSet;
	}

	public void setClientMasterSet(ClientMaster clientMasterSet) {
		this.clientMasterSet = clientMasterSet;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

	public Date getCusYearMonth() {
		return cusYearMonth;
	}

	public void setCusYearMonth(Date cusYearMonth) {
		this.cusYearMonth = cusYearMonth;
	}

	public Integer getSalesMonthTotal() {
		return salesMonthTotal;
	}

	public void setSalesMonthTotal(Integer salesMonthTotal) {
		this.salesMonthTotal = salesMonthTotal;
	}

	public CustomerMonthlyTradingSummaryPK getCustomerMonthlyTradingSummaryPK() {
		return customerMonthlyTradingSummaryPK;
	}

	public void setCustomerMonthlyTradingSummaryPK(
			CustomerMonthlyTradingSummaryPK customerMonthlyTradingSummaryPK) {
		this.customerMonthlyTradingSummaryPK = customerMonthlyTradingSummaryPK;
		cliCode = customerMonthlyTradingSummaryPK.getCliCode();
		cusYearMonth = customerMonthlyTradingSummaryPK.getCusYearMonth();
	}

}
