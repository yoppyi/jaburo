package net.kronosjp.db.entity;

import java.io.Serializable;

public class StockAccordingWareHousePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3824417996498784858L;
	private String goodsCode;
	private String wareHouseCode;

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getWareHouseCode() {
		return wareHouseCode;
	}

	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof StockAccordingWareHousePK))
			return false;

		final StockAccordingWareHousePK stockAccordingWareHousePK = (StockAccordingWareHousePK) obj;

		if (!stockAccordingWareHousePK.getGoodsCode().equals(getGoodsCode()))
			return false;
		if (!stockAccordingWareHousePK.getWareHouseCode().equals(
				getWareHouseCode()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = getGoodsCode().hashCode();
		result = 29 * result + getWareHouseCode().hashCode();
		return super.hashCode();
	}

}
