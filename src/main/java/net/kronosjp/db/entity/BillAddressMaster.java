package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * ������}�X�^
 * 
 * @author Norihiko Yoshida
 * @version
 * 
 */
public class BillAddressMaster implements Serializable ,PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3996983821393104982L;

	private String bilAddCode;
	private Date bilClosingDate;
	private String bilPayment;
	private Integer bilPaymentSite;
	private Integer bilCreditLine;
	private BillMonthlyTradingSummary billedMonthlyTradingSummarySet;
	private PayMaster payMaster;

	public String getPK() {
		return bilAddCode;
	}

	public String getBilAddCode() {
		return bilAddCode;
	}

	public void setBilAddCode(String bilAddCode) {
		this.bilAddCode = bilAddCode;
	}

	public Date getBilClosingDate() {
		return bilClosingDate;
	}

	public void setBilClosingDate(Date bilClosingDate) {
		this.bilClosingDate = bilClosingDate;
	}

	public String getBilPayment() {
		return bilPayment;
	}

	public void setBilPayment(String bilPayment) {
		this.bilPayment = bilPayment;
	}

	public Integer getBilPaymentSite() {
		return bilPaymentSite;
	}

	public void setBilPaymentSite(Integer bilPaymentSite) {
		this.bilPaymentSite = bilPaymentSite;
	}

	public Integer getBilCreditLine() {
		return bilCreditLine;
	}

	public void setBilCreditLine(Integer bilCreditLine) {
		this.bilCreditLine = bilCreditLine;
	}

	public PayMaster getPayMaster() {
		return payMaster;
	}

	public void setPayMaster(PayMaster payMaster) {
		this.payMaster = payMaster;
	}

	public BillMonthlyTradingSummary getBilledMonthlyTradingSummarySet() {
		return billedMonthlyTradingSummarySet;
	}

	public void setBilledMonthlyTradingSummarySet(
			BillMonthlyTradingSummary billedMonthlyTradingSummarySet) {
		this.billedMonthlyTradingSummarySet = billedMonthlyTradingSummarySet;
	}

}
