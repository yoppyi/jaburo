package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * �׎p�}�X�^
 * 
 * @author Masato Ejima
 * @version
 */

public class PackingMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2035346414918429229L;
	private String pacCode;
	private String pacName;
	private Integer pacNumberOfInsertions;
	private GoodsMaster goodsMasterSet;

	public String getPK() {
		return pacCode;
	}

	public String getPacCode() {
		return pacCode;
	}

	public void setPacCode(String pacCode) {
		this.pacCode = pacCode;
	}

	public String getPacName() {
		return pacName;
	}

	public void setPacName(String pacName) {
		this.pacName = pacName;
	}

	public Integer getPacNumberOfInsertions() {
		return pacNumberOfInsertions;
	}

	public void setPacNumberOfInsertions(Integer pacNumberOfInsertions) {
		this.pacNumberOfInsertions = pacNumberOfInsertions;
	}

	public GoodsMaster getGoodsMasterSet() {
		return goodsMasterSet;
	}

	public void setGoodsMasterSet(GoodsMaster goodsMasterSet) {
		this.goodsMasterSet = goodsMasterSet;
	}

}
