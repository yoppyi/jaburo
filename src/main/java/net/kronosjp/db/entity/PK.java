package net.kronosjp.db.entity;

import java.io.Serializable;

public interface PK extends Serializable{
	public Serializable getPK();

}
