package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * @author Norihiko Yoshida
 *
 */
public class PayMasterPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7299387776532882911L;
	private String payCode;
	private String payTable;
	private String payFieldName;

	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	public String getPayTable() {
		return payTable;
	}
	public void setPayTable(String payTable) {
		this.payTable = payTable;
	}
	public String getPayFieldName() {
		return payFieldName;
	}
	public void setPayFieldName(String payFieldName) {
		this.payFieldName = payFieldName;
	}

}
