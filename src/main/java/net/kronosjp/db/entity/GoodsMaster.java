package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * ���i�}�X�^
 * 
 * @author Masato Ejima
 * @version
 */

public class GoodsMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5720065284874805045L;
	private String goodsCode;
	private String goodsName;
	private String pacCode;
	private Integer standardSalesUnitPrice;
	private StockAccordingWareHouse stockAccordingWarehouseSet;
	private Set<PackingMaster> packingMasterSet;
	private StockTransitionAccordingToDay stockTransitionAccordingToDaySet;

	public String getPK() {
		return goodsCode;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public Integer getStandardSalesUnitPrice() {
		return standardSalesUnitPrice;
	}

	public void setStandardSalesUnitPrice(Integer standardSalesUnitPrice) {
		this.standardSalesUnitPrice = standardSalesUnitPrice;
	}

	public StockAccordingWareHouse getStockAccordingWarehouseSet() {
		return stockAccordingWarehouseSet;
	}

	public void setStockAccordingWarehouseSet(
			StockAccordingWareHouse stockAccordingWarehouseSet) {
		this.stockAccordingWarehouseSet = stockAccordingWarehouseSet;
	}

	public Set<PackingMaster> getPackingMasterSet() {
		return packingMasterSet;
	}

	public void setPackingMasterSet(Set<PackingMaster> packingMasterSet) {
		this.packingMasterSet = packingMasterSet;
	}

	public String getPacCode() {
		return pacCode;
	}

	public void setPacCode(String pacCode) {
		this.pacCode = pacCode;
	}

	public StockTransitionAccordingToDay getStockTransitionAccordingToDaySet() {
		return stockTransitionAccordingToDaySet;
	}

	public void setStockTransitionAccordingToDaySet(
			StockTransitionAccordingToDay stockTransitionAccordingToDaySet) {
		this.stockTransitionAccordingToDaySet = stockTransitionAccordingToDaySet;
	}

}
