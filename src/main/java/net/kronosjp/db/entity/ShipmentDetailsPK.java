package net.kronosjp.db.entity;

import java.io.Serializable;

public class ShipmentDetailsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6589769891189397347L;

	private String shipInstallmentsNo;

	private String ordIndexNo;
	private Short ordDetRowNo;

	public String getShipInstallmentsNo() {
		return shipInstallmentsNo;
	}
	public void setShipInstallmentsNo(String shipInstallmentsNo) {
		this.shipInstallmentsNo = shipInstallmentsNo;
	}
	public String getOrdIndexNo() {
		return ordIndexNo;
	}
	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}
	public Short getOrdDetRowNo() {
		return ordDetRowNo;
	}
	public void setOrdDetRowNo(Short ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}
	
}
