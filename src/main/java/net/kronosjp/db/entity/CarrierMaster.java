package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * 運送業者マスタ
 * 
 * @author Makoto Uenishi
 * @version
 */

public class CarrierMaster implements Serializable  ,PK{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6666508433647712654L;
	private String CarCode;
	private String CarName;

	public String getPK() {
		return CarCode;
	}

	public String getCarCode() {
		return CarCode;
	}

	public void setCarCode(String carCode) {
		CarCode = carCode;
	}

	public String getCarName() {
		return CarName;
	}

	public void setCarName(String carName) {
		CarName = carName;
	}

}
