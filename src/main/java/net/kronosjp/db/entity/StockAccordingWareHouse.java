package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * �q�ɕʍ݌�
 * 
 * @author Masato Ejima
 * @version
 */

public class StockAccordingWareHouse implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 210496936996479645L;
	private String goodsCode;
	private String wareHouseCode;
	private Integer numberOfPeriodNeckStocks;
	private Integer presentInventoryGoods;
	private Integer numberThatHasBeenDrawn;

	private StockAccordingWareHousePK stockAccordingWareHousePK;

	private OrderDetails orderDetailsSet;
	private Set<GoodsMaster> goodsMasterSet;
	private Set<WareHouseMaster> WarehouseMasterSet;

	public StockAccordingWareHousePK getPK() {
		return stockAccordingWareHousePK;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getWareHouseCode() {
		return wareHouseCode;
	}

	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}

	public Integer getNumberOfPeriodNeckStocks() {
		return numberOfPeriodNeckStocks;
	}

	public void setNumberOfPeriodNeckStocks(Integer numberOfPeriodNeckStocks) {
		this.numberOfPeriodNeckStocks = numberOfPeriodNeckStocks;
	}

	public Integer getPresentInventoryGoods() {
		return presentInventoryGoods;
	}

	public void setPresentInventoryGoods(Integer presentInventoryGoods) {
		this.presentInventoryGoods = presentInventoryGoods;
	}

	public Integer getNumberThatHasBeenDrawn() {
		return numberThatHasBeenDrawn;
	}

	public void setNumberThatHasBeenDrawn(Integer numberThatHasBeenDrawn) {
		this.numberThatHasBeenDrawn = numberThatHasBeenDrawn;
	}

	public OrderDetails getOrderDetailsSet() {
		return orderDetailsSet;
	}

	public void setOrderDetailsSet(OrderDetails orderDetailsSet) {
		this.orderDetailsSet = orderDetailsSet;
	}

	public Set<GoodsMaster> getGoodsMasterSet() {
		return goodsMasterSet;
	}

	public void setGoodsMasterSet(Set<GoodsMaster> goodsMasterSet) {
		this.goodsMasterSet = goodsMasterSet;
	}

	public Set<WareHouseMaster> getWarehouseMasterSet() {
		return WarehouseMasterSet;
	}

	public void setWarehouseMasterSet(Set<WareHouseMaster> warehouseMasterSet) {
		WarehouseMasterSet = warehouseMasterSet;
	}

	public StockAccordingWareHousePK getStockAccordingWareHousePK() {
		return stockAccordingWareHousePK;
	}

	public void setStockAccordingWareHousePK(
			StockAccordingWareHousePK stockAccordingWareHousePK) {
		this.stockAccordingWareHousePK = stockAccordingWareHousePK;
	}

}
