package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 受注見出し
 * 
 * @author Masato Ejima
 * @version
 * 
 */
public class OrderIndex implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6889554118898329865L;
	/**
	 * 
	 */
	private String ordIndexNo;
	private Date ordDay;
	private String cliCode;
	private String empCode;
	private String ordIndAdhibition;
	private String oIStatus;
	private ClientMaster clientMaster;
	private EmployeeMaster employeeMaster;
	private Set<OrderDetails> orderDetailsSet;

	public String getPK() {
		return ordIndexNo;
	}

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getOrdIndAdhibition() {
		return ordIndAdhibition;
	}

	public void setOrdIndAdhibition(String ordIndAdhibition) {
		this.ordIndAdhibition = ordIndAdhibition;
	}

	public String getoIStatus() {
		return oIStatus;
	}

	public void setoIStatus(String status) {
		oIStatus = status;
	}

	public ClientMaster getClientMaster() {
		return clientMaster;
	}

	public void setClientMaster(ClientMaster clientMaster) {
		this.clientMaster = clientMaster;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Set<OrderDetails> getOrderDetailsSet() {
		return orderDetailsSet;
	}

	public void setOrderDetailsSet(Set<OrderDetails> orderDetailsSet) {
		this.orderDetailsSet = orderDetailsSet;
	}

	public Date getOrdDay() {
		return ordDay;
	}

	public void setOrdDay(Date ordDay) {
		this.ordDay = ordDay;
	}

	public String getOIStatus() {
		return oIStatus;
	}

	public void setOIStatus(String status) {
		oIStatus = status;
	}

}