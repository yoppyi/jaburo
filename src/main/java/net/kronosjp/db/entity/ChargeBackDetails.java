package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * 入金消込明細
 * 
 * @author Makoto Uenishi
 * @version
 */

public class ChargeBackDetails implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3373380935086625846L;
	private String monRecNo;
	private Integer bilHeadNo;
	private Integer erasingAmount;
	private Set<Billheadlines> BillheadlinesSet;
	private Set<MoneyReceivedHeadLine> moneyReceivedHeadLineSet;

	public ChargeBackDetails getPK() {
		return this;
	}

	public String getMonRecNo() {
		return monRecNo;
	}

	public void setMonRecNo(String monRecNo) {
		this.monRecNo = monRecNo;
	}

	public Integer getBilHeadNo() {
		return bilHeadNo;
	}

	public void setBilHeadNo(Integer bilHeadNo) {
		this.bilHeadNo = bilHeadNo;
	}

	public Integer getErasingAmount() {
		return erasingAmount;
	}

	public void setErasingAmount(Integer erasingAmount) {
		this.erasingAmount = erasingAmount;
	}

	public Set<Billheadlines> getBillheadlinesSet() {
		return BillheadlinesSet;
	}

	public void setBillheadlinesSet(Set<Billheadlines> billheadlinesSet) {
		BillheadlinesSet = billheadlinesSet;
	}

	public Set<MoneyReceivedHeadLine> getMoneyReceivedHeadLineSet() {
		return moneyReceivedHeadLineSet;
	}

	public void setMoneyReceivedHeadLineSet(
			Set<MoneyReceivedHeadLine> moneyReceivedHeadLineSet) {
		this.moneyReceivedHeadLineSet = moneyReceivedHeadLineSet;
	}

}
