package net.kronosjp.db.entity;

import java.io.Serializable;


/**
 * ���㖾��
 * 
 * @author Makoto Uenishi
 * @version
 */

public class SalesDetails implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3276578192639580769L;
	private String salHeadNo;
	private Short salRowNo;
	private Integer salProceeds;
	private String salesSourceNo;

	private SalesDetailsPK salesDetailsPK;

	private SalesHeadline salesHeadline;

	public String getSalHeadNo() {
		return salHeadNo;
	}

	public void setSalHeadNo(String salHeadNo) {
		this.salHeadNo = salHeadNo;
	}


	public SalesDetailsPK getSalesDetailsPK() {
		return salesDetailsPK;
	}

	public void setSalesDetailsPK(SalesDetailsPK salesDetailsPK) {
		this.salesDetailsPK = salesDetailsPK;
		salHeadNo = salesDetailsPK.getSalHeadNo();
		salRowNo = salesDetailsPK.getSalRowNo();
	}

	public SalesDetailsPK getPK() {
		return salesDetailsPK;
	}

	public Short getSalRowNo() {
		return salRowNo;
	}

	public void setSalRowNo(Short salRowNo) {
		this.salRowNo = salRowNo;
	}

	public Integer getSalProceeds() {
		return salProceeds;
	}

	public void setSalProceeds(Integer salProceeds) {
		this.salProceeds = salProceeds;
	}

	public SalesHeadline getSalesHeadline() {
		return salesHeadline;
	}

	public void setSalesHeadline(SalesHeadline salesHeadline) {
		this.salesHeadline = salesHeadline;
	}

	public String getSalesSourceNo() {
		return salesSourceNo;
	}

	public void setSalesSourceNo(String salesSourceNo) {
		this.salesSourceNo = salesSourceNo;
	}
}
