package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 売り上げ見出し
 * 
 * @author Makoto Uenishi
 * @version
 */

public class SalesHeadline implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7223997543039343834L;
	private String salHeadNo;
	private String salDivision;
	private String salCustomerCode;
	private Date salDay;

	private Set<SalesDetails> salesDetailsSet;

	private Set<Billheadlines> billheadlinesSet;
	private CustomerMonthlyTradingSummary customerMonthlyTradingSummary;

	private Set<Billheadlines> set;

	public String getPK() {
		return salHeadNo;
	}

	public Set<Billheadlines> getSet() {
		return set;
	}

	public void setSet(Set<Billheadlines> set) {
		this.set = set;
	}

	public Set<Billheadlines> getBillheadlinesSet() {
		return billheadlinesSet;
	}

	public void setBillheadlinesSet(Set<Billheadlines> billheadlinesSet) {
		this.billheadlinesSet = billheadlinesSet;
	}

	public String getSalHeadNo() {
		return salHeadNo;
	}

	public void setSalHeadNo(String salHeadNo) {
		this.salHeadNo = salHeadNo;
	}

	public Date getSalDay() {
		return salDay;
	}

	public void setSalDay(Date salDay) {
		this.salDay = salDay;
	}

	public String getSalDivision() {
		return salDivision;
	}

	public void setSalDivision(String salDivision) {
		this.salDivision = salDivision;
	}

	public String getSalCustomerCode() {
		return salCustomerCode;
	}

	public void setSalCustomerCode(String salCustomerCode) {
		this.salCustomerCode = salCustomerCode;
	}

	public void setSalesDetailsSet(Set<SalesDetails> salesDetailsSet) {
		this.salesDetailsSet = salesDetailsSet;
	}

	public Set<SalesDetails> getSalesDetailsSet() {
		return salesDetailsSet;
	}

	public CustomerMonthlyTradingSummary getCustomerMonthlyTradingSummary() {
		return customerMonthlyTradingSummary;
	}

	public void setCustomerMonthlyTradingSummary(
			CustomerMonthlyTradingSummary customerMonthlyTradingSummary) {
		this.customerMonthlyTradingSummary = customerMonthlyTradingSummary;
	}

}
