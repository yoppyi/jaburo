package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * ���ʍ݌ɐ���
 * 
 * @author Masato Ejima
 * @version
 */

public class StockTransitionAccordingToDay implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5729216994553819541L;
	private String goodsCode;
	private Date sTBaseDate;
	private Integer sTNumberOfShipmentScheduleTotals;
	private Integer sTNumberOfScheduleOfArrivalOfGoodsTotals;
	private StockTransitionAccordingToDayPK stockTransitionAccordingToDayPK;
	private Set<GoodsMaster> goodsMasterSet;

	public StockTransitionAccordingToDayPK getPK() {
		return stockTransitionAccordingToDayPK;
	}

	public Set<GoodsMaster> getGoodsMasterSet() {
		return goodsMasterSet;
	}

	public void setGoodsMasterSet(Set<GoodsMaster> goodsMasterSet) {
		this.goodsMasterSet = goodsMasterSet;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public Date getsTBaseDate() {
		return sTBaseDate;
	}

	public void setsTBaseDate(Date baseDate) {
		sTBaseDate = baseDate;
	}

	public Integer getsTNumberOfShipmentScheduleTotals() {
		return sTNumberOfShipmentScheduleTotals;
	}

	public void setsTNumberOfShipmentScheduleTotals(
			Integer numberOfShipmentScheduleTotals) {
		sTNumberOfShipmentScheduleTotals = numberOfShipmentScheduleTotals;
	}

	public StockTransitionAccordingToDayPK getStockTransitionAccordingToDayPK() {
		return stockTransitionAccordingToDayPK;
	}

	public void setStockTransitionAccordingToDayPK(
			StockTransitionAccordingToDayPK stockTransitionAccordingToDayPK) {
		this.stockTransitionAccordingToDayPK = stockTransitionAccordingToDayPK;
	}

	public Date getSTBaseDate() {
		return sTBaseDate;
	}

	public void setSTBaseDate(Date baseDate) {
		sTBaseDate = baseDate;
	}

	public Integer getsTNumberOfScheduleOfArrivalOfGoodsTotals() {
		return sTNumberOfScheduleOfArrivalOfGoodsTotals;
	}

	public void setsTNumberOfScheduleOfArrivalOfGoodsTotals(
			Integer numberOfScheduleOfArrivalOfGoodsTotals) {
		sTNumberOfScheduleOfArrivalOfGoodsTotals = numberOfScheduleOfArrivalOfGoodsTotals;
	}

	public Integer getSTNumberOfShipmentScheduleTotals() {
		return sTNumberOfShipmentScheduleTotals;
	}

	public void setSTNumberOfShipmentScheduleTotals(
			Integer numberOfShipmentScheduleTotals) {
		sTNumberOfShipmentScheduleTotals = numberOfShipmentScheduleTotals;
	}

	public Integer getSTNumberOfScheduleOfArrivalOfGoodsTotals() {
		return sTNumberOfScheduleOfArrivalOfGoodsTotals;
	}

	public void setSTNumberOfScheduleOfArrivalOfGoodsTotals(
			Integer numberOfScheduleOfArrivalOfGoodsTotals) {
		sTNumberOfScheduleOfArrivalOfGoodsTotals = numberOfScheduleOfArrivalOfGoodsTotals;
	}

}
