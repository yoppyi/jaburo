package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * 企業グループマスタ
 * 
 * @author Masato Ejima
 * @version
 * 
 */

public class EnterpriseGroupMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5733783850820282523L;
	private String eGCode;
	private String eGName;
	private Set<CustomerMaster> customerMasterSet;

	public String getPK() {
		return eGCode;
	}

	public String getEGCode() {
		return eGCode;
	}

	public void setEGCode(String code) {
		eGCode = code;
	}

	public String getEGName() {
		return eGName;
	}

	public void setEGName(String name) {
		eGName = name;
	}

	public Set<CustomerMaster> getCustomerMasterSet() {
		return customerMasterSet;
	}

	public void setCustomerMasterSet(Set<CustomerMaster> customerMasterSet) {
		this.customerMasterSet = customerMasterSet;
	}
}
