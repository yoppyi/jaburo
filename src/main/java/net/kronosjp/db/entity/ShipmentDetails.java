package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * �o�ז���
 * 
 * @author Makoto Uenishi
 * @version
 */

public class ShipmentDetails implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3352569461577935466L;
	private String ordIndexNo;
	private Short ordDetRowNo;
	private String shipInstallmentsNo;
	private String shipHeadNo;
	private Date shipScheduledDay;
	private Integer shipScheduleAmount;
	private Integer shipNumberOfAmount;
	private String shipScheduleWareHouseCode;
	private String shipNumberOfWareHouseCode;
	private String salesSourceNo;
	private String ShipStatus;

	private ShipmentDetailsPK shipmentDetailsPK;

	private ShipmentHeadline shipmentHeadlineSet;
	private OrderDetails orderDetailsSet;

	public ShipmentDetailsPK getPK() {
		return shipmentDetailsPK;
	}

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public Short getOrdDetRowNo() {
		return ordDetRowNo;
	}

	public void setOrdDetRowNo(Short ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}

	public String getShipInstallmentsNo() {
		return shipInstallmentsNo;
	}

	public void setShipInstallmentsNo(String shipInstallmentsNo) {
		this.shipInstallmentsNo = shipInstallmentsNo;
	}

	public String getShipHeadNo() {
		return shipHeadNo;
	}

	public void setShipHeadNo(String shipHeadNo) {
		this.shipHeadNo = shipHeadNo;
	}

	public Integer getShipScheduleAmount() {
		return shipScheduleAmount;
	}

	public void setShipScheduleAmount(Integer shipScheduleAmount) {
		this.shipScheduleAmount = shipScheduleAmount;
	}

	public Integer getShipNumberOfAmount() {
		return shipNumberOfAmount;
	}

	public void setShipNumberOfAmount(Integer shipNumberOfAmount) {
		this.shipNumberOfAmount = shipNumberOfAmount;
	}

	public String getShipStatus() {
		return ShipStatus;
	}

	public void setShipStatus(String shipStatus) {
		ShipStatus = shipStatus;
	}

	public ShipmentDetailsPK getShipmentDetailsPK() {
		return shipmentDetailsPK;
	}

	public void setShipmentDetailsPK(ShipmentDetailsPK shipmentDetailsPK) {
		this.shipmentDetailsPK = shipmentDetailsPK;
		ordDetRowNo = shipmentDetailsPK.getOrdDetRowNo();
		ordIndexNo = shipmentDetailsPK.getOrdIndexNo();
		shipInstallmentsNo = shipmentDetailsPK.getShipInstallmentsNo();
	}

	public ShipmentHeadline getShipmentHeadlineSet() {
		return shipmentHeadlineSet;
	}

	public void setShipmentHeadlineSet(ShipmentHeadline shipmentHeadlineSet) {
		this.shipmentHeadlineSet = shipmentHeadlineSet;
	}

	public OrderDetails getOrderDetailsSet() {
		return orderDetailsSet;
	}

	public void setOrderDetailsSet(OrderDetails orderDetailsSet) {
		this.orderDetailsSet = orderDetailsSet;
	}

	public Date getShipScheduledDay() {
		return shipScheduledDay;
	}

	public void setShipScheduledDay(Date shipScheduledDay) {
		this.shipScheduledDay = shipScheduledDay;
	}

	public String getShipScheduleWareHouseCode() {
		return shipScheduleWareHouseCode;
	}

	public void setShipScheduleWareHouseCode(String shipScheduleWareHouseCode) {
		this.shipScheduleWareHouseCode = shipScheduleWareHouseCode;
	}
	

	public String getShipNumberOfWareHouseCode() {
		return shipNumberOfWareHouseCode;
	}

	public void setShipNumberOfWareHouseCode(String shipNumberOfWareHouseCode) {
		this.shipNumberOfWareHouseCode = shipNumberOfWareHouseCode;
	}

	public String getSalesSourceNo() {
		return salesSourceNo;
	}

	public void setSalesSourceNo(String salesSourceNo) {
		this.salesSourceNo = salesSourceNo;
	}


}
