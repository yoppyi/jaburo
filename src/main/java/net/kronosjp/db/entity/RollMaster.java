package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * ロールマスタ
 * 
 * @author Norihiko Yoshida
 * @version
 * 
 */
public class RollMaster implements Serializable, PK {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8381279525728025644L;
	private String rollId;
	private String rollName;
	private String rollLevel;
	private Set<EmployeeMaster> employeeMasterSet;

	public String getPK() {
		return rollId;
	}

	public String getRollId() {
		return rollId;
	}

	public void setRollId(String rollId) {
		this.rollId = rollId;
	}

	public String getRollName() {
		return rollName;
	}

	public void setRollName(String rollName) {
		this.rollName = rollName;
	}

	public String getRollLevel() {
		return rollLevel;
	}

	public void setRollLevel(String rollLevel) {
		this.rollLevel = rollLevel;
	}

	public Set<EmployeeMaster> getEmployeeMasterSet() {
		return employeeMasterSet;
	}

	public void setEmployeeMasterSet(Set<EmployeeMaster> employeeMasterSet) {
		this.employeeMasterSet = employeeMasterSet;
	}

}
