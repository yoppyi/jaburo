package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * �敪�}�X�^
 * 
 * @author Norihiko Yoshida
 * @version
 * 
 */
public class PayMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7277321482800110580L;

	private String payTable;
	private String payCode;
	private String payDivName;
	private String payFieldName;
	private PayMasterPK payMasterPK;

	public PayMasterPK getPK() {
		return payMasterPK;
	}

	public String getPayTable() {
		return payTable;
	}

	public void setPayTable(String payTable) {
		this.payTable = payTable;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getPayDivName() {
		return payDivName;
	}

	public void setPayDivName(String payDivName) {
		this.payDivName = payDivName;
	}

	public PayMasterPK getPayMasterPK() {
		return payMasterPK;
	}

	public void setPayMasterPK(PayMasterPK payMasterPK) {
		this.payMasterPK = payMasterPK;
	}

	public String getPayFieldName() {
		return payFieldName;
	}

	public void setPayFieldName(String payFieldName) {
		this.payFieldName = payFieldName;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}
