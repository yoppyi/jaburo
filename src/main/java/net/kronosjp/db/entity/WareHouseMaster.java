package net.kronosjp.db.entity;

import java.io.Serializable;

/**
 * �q�Ƀ}�X�^
 * 
 * @author Masato Ejima
 * @version
 * 
 */

public class WareHouseMaster implements Serializable, PK {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6424982063759498195L;
	private String warHouseCode;
	private String warName;
	private String warAddress;
	private StockAccordingWareHouse stockAccordingWarehouseSet;

	public String getPK() {
		return warHouseCode;
	}

	public String getWarHouseCode() {
		return warHouseCode;
	}

	public void setWarHouseCode(String warHouseCode) {
		this.warHouseCode = warHouseCode;
	}

	public String getWarName() {
		return warName;
	}

	public void setWarName(String warName) {
		this.warName = warName;
	}

	public String getWarAddress() {
		return warAddress;
	}

	public void setWarAddress(String warAddress) {
		this.warAddress = warAddress;
	}

	public StockAccordingWareHouse getStockAccordingWarehouseSet() {
		return stockAccordingWarehouseSet;
	}

	public void setStockAccordingWarehouseSet(
			StockAccordingWareHouse stockAccordingWarehouseSet) {
		this.stockAccordingWarehouseSet = stockAccordingWarehouseSet;
	}
}
