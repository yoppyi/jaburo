package net.kronosjp.db.entity;

import java.io.Serializable;
import java.util.Date;

public class BillMonthlyTradingSummaryPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1793702384263142535L;
	private Date bSYearMonth;
	private String bilAddCode;

	public Date getbSYearMonth() {
		return bSYearMonth;
	}

	public void setbSYearMonth(Date yearMonth) {
		bSYearMonth = yearMonth;
	}

	public String getBilAddCode() {
		return bilAddCode;
	}

	public void setBilAddCode(String bilAddCode) {
		this.bilAddCode = bilAddCode;
	}

	public Date getBSYearMonth() {
		return bSYearMonth;
	}

	public void setBSYearMonth(Date yearMonth) {
		bSYearMonth = yearMonth;
	}

}
