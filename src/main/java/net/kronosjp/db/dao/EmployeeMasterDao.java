package net.kronosjp.db.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import net.kronosjp.db.entity.ClientMaster;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.util.bean.BeanCreator;
import net.kronosjp.util.utiltes.InvolveWithCreate;

/**
 * EmployeeMaster用Dao
 * 
 * @author Norihiko Yoshida
 * @version 1.0
 */
public class EmployeeMasterDao extends DaoBase {

	public static void update(HttpServletRequest request, String rollId)
			throws SystemErrorException {
		StringBuilder successMessage = new StringBuilder();
		successMessage.append("社員コード");

		EmployeeMaster em = null;

		try {

			em = BeanCreator.createBean(request, EmployeeMaster.class);

			em.setEmpPass(keySearch(EmployeeMaster.class, em.getEmpCode(), request)
					.getEmpPass());
			em.setRollId(rollId);

			update(em, request);
			successMessage.append(em.getEmpCode());

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		successMessage.append("は、正常に更新されました。");
		request.setAttribute("headMessage", successMessage.toString());

	}

	/**
	 * EmployeeMaster削除処理
	 * 
	 * @param request
	 * @throws SystemErrorException
	 */
	public static void delete(HttpServletRequest request)
			throws SystemErrorException {

		StringBuilder successMessage = new StringBuilder();
		successMessage.append("社員コード");

		List<EmployeeMaster> empList = (List<EmployeeMaster>) request
				.getSession().getAttribute("deleteList");

		System.out.println(empList);
		List<String> delEmpCodeList = new ArrayList<String>();

		Session session = null;
		Transaction transaction = null;
		String empCode = null;
		try {
			session = createSession(request);
			transaction = session.beginTransaction();

			for (EmployeeMaster emp : empList) {
				empCode = emp.getEmpCode();
				successMessage.append(empCode + "、");
				delEmpCodeList.add(empCode);
			}
			System.out.println(delEmpCodeList);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new SystemErrorException(e);
		} finally {
			closeSession(request);
		}

		try {
			List<String> delEmpList = new ArrayList<String>();
			EmployeeMaster employeeMaster = null;
			session = createSession(request);
			transaction = session.beginTransaction();
			for (String delEmpCode : delEmpCodeList) {
				System.out.println(delEmpCode);

				employeeMaster = (EmployeeMaster) session.get(
						EmployeeMaster.class, delEmpCode);

				System.out.println("d" + employeeMaster);

				session.delete(employeeMaster);
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw new SystemErrorException(e);
		} finally {
			closeSession(request);
		}

		successMessage.deleteCharAt(successMessage.length() - 1);
		successMessage.append("は、正常に削除されました。");
		request.setAttribute("headMessage", successMessage.toString());
	}

	/**
	 * ClientMaster、OrderIndexを持っていないかチェックし、Map<Key,
	 * value>（keyにempCode、valueにエラーメッセージ）をrequest.setAttribute("errorMessageMap",
	 * errorMessageMap)する。
	 * 
	 * @param request
	 * @throws SystemErrorException
	 */
	public static void errorCheack(HttpServletRequest request)
			throws SystemErrorException {
		Map<String, String> errorMessageMap = new HashMap<String, String>();
		EmployeeMaster employeeMaster = null;
		Set<ClientMaster> clientMasterSet = null;
		Set<OrderIndex> orderIndexSet = null;
		String[] empCodes = request.getParameterValues("check");
		for (String empCode : empCodes) {
			String errorMessageOfClient = null;
			String errorMessageOfOrder = null;
			employeeMaster = keySearch(EmployeeMaster.class, empCode, request);

			clientMasterSet = employeeMaster.getClientMasterSet();
			orderIndexSet = employeeMaster.getOrderIndexSet();

			if (!clientMasterSet.isEmpty()) {
				errorMessageOfClient = "現在、営業担当先があります。";
			}
			if (!orderIndexSet.isEmpty()) {
				errorMessageOfOrder = "現在、受注を担当しています。";
			}

			if (errorMessageOfClient != null | errorMessageOfOrder != null) {
				errorMessageMap.put(empCode, errorMessageOfClient
						+ errorMessageOfOrder);
			}
		}
		request.getSession().setAttribute("errorMessageMap", errorMessageMap);
	}
}
