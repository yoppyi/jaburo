package net.kronosjp.db.dao;

import java.io.Serializable;

import org.hibernate.Session;

public class SerializableSession implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5037889372657700776L;
	private Session session;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
