package net.kronosjp.db.dao;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.PK;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.util.bean.BeanUtils;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * シンプルなHibernateのCRUD処理。
 * 
 * @author Norihiko Yoshida
 * 
 */
public class DaoBase implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7510847918841271869L;
	protected static final SimpleDateFormat DATEFORMAT = new SimpleDateFormat(
			"yyyy/MM/dd");
	private static SessionFactory sessionFactory = null;

	/**
	 * SessionFactoryの作成。シングルトン
	 * 
	 * @return セッションファクトリー
	 */
	public static SessionFactory createSessionFactory() {

		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure()
					.buildSessionFactory();
		}
		return sessionFactory;
	}

	/**
	 * DBのPRIMARY KEYで検索
	 * 
	 * @param <T>
	 * @param klass
	 * @param key
	 * @return
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> T keySearch(Class<T> klass,
			Serializable key, HttpServletRequest request)
			throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		T bean = null;
		try {
			transaction = session.beginTransaction();
			bean = (T) session.get(klass, key);
		} catch (HibernateException e) {
			closeSession(request);
			throw new SystemErrorException(e);
		}
		return bean;

	}

	public static <T extends Serializable> List<T> search(Class<T> klass,
			String columnName, String searchValue, HttpServletRequest request)
			throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		List<T> list = null;
		String className = klass.getSimpleName();
		String uncapitalizeClassName = StringUtils.uncapitalize(className);
		String hql = "from " + className + " as " + uncapitalizeClassName
				+ " where " + uncapitalizeClassName + "." + columnName + "='"
				+ searchValue + "'";
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery(hql);
			list = query.list();
		} catch (HibernateException e) {
			closeSession(request);
			throw new SystemErrorException(e);
		}
		return list;
	}

	public static <T extends Serializable> List<T> search(Class<T> klass,
			String[] columnAndName, HttpServletRequest request)
			throws SystemErrorException {

		String columnName = columnAndName[0];
		String searchValue = columnAndName[1];

		return search(klass, columnName, searchValue, request);
	}

	/**
	 * 登録処理 Returnの際１は成功、２は失敗
	 * 
	 * @param <T>
	 * @param bean
	 * @return int
	 * @throws SystemErrorException 
	 */
	public static <T extends Serializable> void insert(T bean,
			HttpServletRequest request) throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			closeSession(request);
			throw new SystemErrorException(e);
			// なにか検索失敗の動作を作成
		}
	}

	/**
	 * 更新処理
	 * 
	 * @param <T>
	 * @param bean
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable & PK> void update(T bean,
			HttpServletRequest request) throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();

			Class<T> klass = (Class<T>) bean.getClass();
			T cheack = (T) session.get(klass, bean.getPK());
			try {
				cheack = BeanUtils.compareAndSetBean(cheack, bean);
			} catch (Exception e) {
				throw new SystemErrorException(e);
			}

			// session.update(cheack);
			session.flush();
			// transaction.commit();

			// session.flush();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			closeSession(request);
			throw new SystemErrorException(e);
		}
	}

	// /**
	// * 更新処理
	// *
	// * @param <T>
	// * @param bean
	// * @throws SystemErrorException
	// */
	// public static <T extends PK & Serializable, T2 extends Serializable> void
	// update(
	// T bean, Class<T2> keyKlass, HttpServletRequest request)
	// throws SystemErrorException {
	// Session session = createSession(request);
	// Transaction transaction = null;
	// try {
	// transaction = session.beginTransaction();
	// Class<T> klass = (Class<T>) bean.getClass();
	//
	// T2 pk = (T2) bean.getPK();
	// bean = (T) session.get(klass, pk);
	// session.update(bean);
	// // session.flush();
	// transaction.commit();
	// } catch (HibernateException e) {
	// transaction.rollback();
	// closeSession(request);
	// throw new SystemErrorException(e);
	// }
	// }

	/**
	 * DBのテーブルデータ全てををBeanの入ったListとして取得
	 * 
	 * @param <T>
	 * @param s
	 * @param key
	 * @return
	 * @throws SystemErrorException
	 * @throws SystemErrorException
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> List<T> getTable(Class<T> klass,
			HttpServletRequest request) throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		List<T> tableList = null;
		try {
			transaction = session.beginTransaction();
			tableList = (List<T>) session.createCriteria(klass).list();
		} catch (HibernateException e) {
			closeSession(request);
			throw new SystemErrorException(e);
		}
		return tableList;
	}

	/**
	 * 削除
	 * 
	 * @param <T>
	 * @param bean
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void delete(T bean,
			HttpServletRequest request) throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.evict(bean);
			session.delete(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			closeSession(request);
			throw new SystemErrorException(e);
		}
	}

	/**
	 * Listを削除
	 * 
	 * @param <T>
	 * @param delList
	 * @param request
	 * @throws SystemErrorException
	 */
	public static <T extends Serializable> void listDelete(List<T> delList,
			HttpServletRequest request) throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			for (T bean : delList) {
				session.evict(bean);
				session.delete(bean);
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			closeSession(request);
			throw new SystemErrorException(e);
		}
	}

	/**
	 * テーブルのカラムの最大値を求める。
	 * 
	 * @param <T>
	 * @param className
	 *            求めたいテーブルに対応するentityクラス名
	 * @param fieldName
	 *            求めたいカラムに対応するentityのフィールド名
	 * @return カラムのデータ型に対応したオブジェクトので帰ってくるはず。
	 * @throws SystemErrorException
	 */
	public static Object getMaxValueOfColumn(String className,
			String fieldName, HttpServletRequest request)
			throws SystemErrorException {
		Session session = createSession(request);
		Transaction transaction = null;
		String uncapitalizeClassName = StringUtils.uncapitalize(className);
		String hql = "select  max(" + uncapitalizeClassName + "." + fieldName
				+ ") from " + className + " as " + uncapitalizeClassName;
		Object maxValue = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery(hql);
			maxValue = query.uniqueResult();
		} catch (HibernateException e) {
			closeSession(request);
			throw new SystemErrorException(e);
		}
		return maxValue;
	}

	public static <T extends Serializable> List<T> getKeySearchList(
			Class<T> klass, String[] keyArray, HttpServletRequest request)
			throws SystemErrorException {
		List<T> keySearchList = new ArrayList<T>();
		Session session = createSession(request);
		Transaction transaction = null;
		T bean = null;
		try {
			transaction = session.beginTransaction();
			for (String key : keyArray) {
				bean = (T) session.get(klass, key);
				keySearchList.add(bean);
			}
		} catch (HibernateException e) {
			transaction.rollback();
			closeSession(request);
			throw new SystemErrorException(e);
		}
		return keySearchList;
	}

	public static void closeSession(HttpServletRequest request) {
		SerializableSession serializableSession = (SerializableSession) request
				.getSession().getAttribute("session");
		Session session = null;
		if (serializableSession != null) {
			session = serializableSession.getSession();
			if (session.isOpen()) {
				session.close();
			}
			request.getSession().removeAttribute("session");
		}
	}

	public static Session createSession(HttpServletRequest request) {
		SerializableSession serializableSession = (SerializableSession) request
				.getSession().getAttribute("session");
		Session session = null;
		if (serializableSession != null) {
			session = serializableSession.getSession();
			if (!session.isOpen()) {
				// session = sessionFactory.openSession();
				session = sessionFactory.getCurrentSession();
				serializableSession.setSession(session);
				request.getSession().setAttribute("session",// ここを考える
						serializableSession);
			}
			return session;
		}
		session = sessionFactory.getCurrentSession();
		serializableSession = new SerializableSession();
		serializableSession.setSession(session);
		request.getSession().setAttribute("session", serializableSession);
		return session;
	}

	public static Date getFormatedDate(String parameter) throws ParseException {
		// TODO Auto-generated method stub
		
		return DATEFORMAT.parse(parameter);
	}

}
