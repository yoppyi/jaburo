package net.kronosjp.db.dao;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderDetailsPK;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.tags.bean.util.OrdersList;
import net.kronosjp.util.bean.BeanCreator;

public class OrderDao extends DaoBase {

	/**
	 * �P����OrderIndex�̓o�^
	 * 
	 * @param request
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws ParseException
	 * @throws SystemErrorException 
	 */
	public static void orderIndexInsert(HttpServletRequest request)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			InstantiationException, ParseException, SystemErrorException {

		OrderIndex orderIndex = BeanCreator.createBean(request,
				OrderIndex.class, "ordDay");
		orderIndex.setoIStatus(request.getParameter("oIStatus"));

		Date date = DATEFORMAT.parse(request.getParameter("e4"));
		orderIndex.setOrdDay(date);
		insert(orderIndex, request);
	}

	/**
	 * �P����OrderDetails�̓o�^
	 * 
	 * @param request
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws ParseException
	 * @throws SystemErrorException 
	 */
	public static void orderDetailsInsert(HttpServletRequest request)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			InstantiationException, ParseException, SystemErrorException {

		OrderDetailsPK orderDetailsPK = BeanCreator.createBean(request,
				OrderDetailsPK.class, "ordDetRowNo");
		Short ordDetRowNo = Short.parseShort(request
				.getParameter("ordDetRowNo"));
		orderDetailsPK.setOrdDetRowNo(ordDetRowNo);

		OrderDetails orderDetails = BeanCreator.createBean(request,
				OrderDetails.class, "ordNumberOfOrder", "ordHopeDeliveryDate",
				"ordIndexNo", "ordDetRowNo");

		Integer ordNumberOfOrder = Integer.parseInt(request
				.getParameter("ordNumberOfOrder"));
		Date ordHopeDeliveryDate = DATEFORMAT.parse(request
				.getParameter("ordHopeDeliveryDate"));
		orderDetails.setOrdNumberOfOrder(ordNumberOfOrder);
		orderDetails.setOrdHopeDeliveryDate(ordHopeDeliveryDate);
		orderDetails.setOrderDetailsPK(orderDetailsPK);
		insert(orderDetails, request);
	}

	public static int extentionOrderDetailsInsert(HttpServletRequest request)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			InstantiationException, ParseException, SystemErrorException {
		Enumeration<String> enumeration = request.getParameterNames();
		List<String> paramNameOfordDetRowNoList = new ArrayList<String>();
		List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
		OrderDetailsPK orderDetailsPK = null;
		OrderDetails orderDetails = null;
		String paramNameOfOrdDetRowNo = null;
		Short ordDetRowNo = null;
		String goodsCode = null;
		int result = 0;

		while (enumeration.hasMoreElements()) {
			paramNameOfOrdDetRowNo = enumeration.nextElement();
			if (paramNameOfOrdDetRowNo.matches("ordDetRowNo.*")) {
				paramNameOfordDetRowNoList.add(paramNameOfOrdDetRowNo);

			}
		}

		for (String paramNameOfordDetRowNoTemp : paramNameOfordDetRowNoList) {
			ordDetRowNo = Short.valueOf(StringUtils.remove(
					paramNameOfordDetRowNoTemp, "ordDetRowNo"));
			goodsCode = request.getParameter("goodsCode" + ordDetRowNo);

			if (!goodsCode.equals("000")) {

				orderDetailsPK = BeanCreator.createBean(request,
						OrderDetailsPK.class);
				orderDetailsPK.setOrdDetRowNo(ordDetRowNo);

				orderDetails = new OrderDetails();
				orderDetails.setOrderDetailsPK(orderDetailsPK);

				Integer ordNumberOfOrder = Integer.parseInt(request
						.getParameter("ordNumberOfOrder" + ordDetRowNo));
				Date ordHopeDeliveryDate = DATEFORMAT.parse(request
						.getParameter("e5"));
				orderDetails.setOrdNumberOfOrder(ordNumberOfOrder);
				orderDetails.setOrdHopeDeliveryDate(ordHopeDeliveryDate);

				orderDetails.setGoodsCode(goodsCode);
				orderDetails.setWareHouseCode(request
						.getParameter("warHouseCode" + ordDetRowNo));
				orderDetails.setoDStatus(request.getParameter("oIStatus"));

				orderDetailsList.add(orderDetails);

			}
		}

		for (OrderDetails details : orderDetailsList) {
			insert(details, request);
		}
		return result;
	}

	public static int orderCreate(HttpServletRequest request)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			InstantiationException, ParseException, SystemErrorException {
		orderIndexInsert(request);
		return extentionOrderDetailsInsert(request);
	}

	public static void search(HttpServletRequest request)
			throws SystemErrorException {
		String flag = request.getParameter("search");
		String keyWord = null;
		String hql = "from OrderIndex as oi where oi.";
		Session session = createSession(request);
		session.beginTransaction();
		List<OrderIndex> orderIndexList = getTable(OrderIndex.class, request);
		Query query = null;

		if (flag != null) {

			if (flag.equals("ordIndexNo")) {
				keyWord = request.getParameter("ordIndexNo");
				hql = hql + "ordIndexNo='" + keyWord + "'";
				System.out.println(hql);
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}

			if (flag.equals("cliCode")) {
				keyWord = request.getParameter("cliCode");
				hql = hql + "cliCode='" + keyWord + "'";
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}

			if (flag.equals("e4")) {
				keyWord = StringUtils.replace(request.getParameter("e4"), "/",
						"-");
				hql = hql + "ordDay='" + keyWord + "'";
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}
		}
		try {
			request.setAttribute("orderList", OrdersList.add(orderIndexList));

		} catch (Exception e) {
			throw new SystemErrorException(e);
		}

	}

	public static void deleteOrderIndex(HttpServletRequest request)
			throws SystemErrorException {
		String[] ordIndexNos = request.getParameterValues("check");
		OrderIndex orderIndex = null;
		List<OrderDetails> orderDetailsList = null;
		for (String ordIndexNo : ordIndexNos) {
			orderIndex = keySearch(OrderIndex.class, ordIndexNo, request);
			orderDetailsList = search(OrderDetails.class, "ordIndexNo",
					orderIndex.getOrdIndexNo(), request);
			listDelete(orderDetailsList, request);
			delete(orderIndex, request);
		}

	}
}
