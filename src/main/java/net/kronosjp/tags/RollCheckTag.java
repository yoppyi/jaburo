package net.kronosjp.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import net.kronosjp.db.entity.EmployeeMaster;

public class RollCheckTag extends TagSupport {

	private String jobName;
	private boolean check;
	private JspWriter out;
	private int index;
	
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	@Override
	public int doStartTag() throws JspException {

		try {
			out = pageContext.getOut();
			EmployeeMaster em = (EmployeeMaster) pageContext.getSession()
					.getAttribute("EmployeeMaster");
			int rollLevel = Integer.parseInt(em.getRollMaster().getRollLevel());
			
			String binaryRoolLevel = binarySizeArrayLength(Integer.toBinaryString(rollLevel));
			out.println(setURI(binaryRoolLevel));
		} catch (IOException e) {
			throw new JspException(e);
		}
		if (check) {
			return EVAL_BODY_INCLUDE;
		}
		return SKIP_BODY;
	}

	private String binarySizeArrayLength(String binaryRoolLevel) {
		
		while (binaryRoolLevel.length() < 4) {
			binaryRoolLevel = "0" + binaryRoolLevel;
		}
		return binaryRoolLevel;
	}

	private String setURI(String binaryRoolLevel) {

		StringBuilder html = new StringBuilder();
		char[] charArrayRoolLevel = binaryRoolLevel.toCharArray();

		html.append("<td width=\"130\"><div align=\"center\">");

		if (charArrayRoolLevel[index] == '1') {
			check = true;
		} else {
			html.append("<div align=\"center\" title=\"権限がありません\">");
			check = false;
		}
		return html.toString();
	}

	@Override
	public int doEndTag() throws JspException {
		
		StringBuilder html = new StringBuilder();
		html.append("> <font color=\"white\">");
		html.append(jobName); // 飛先
		html.append("</font>");
		if (!check) {
			html.append("</div>");
		}
		html.append("</a></div></td>");
		try {
			out.println(html);
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_PAGE;
	}
}
