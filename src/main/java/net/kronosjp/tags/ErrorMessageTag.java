package net.kronosjp.tags;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 入力検証エラーメッセージ表示用カスタムタグ 。タグの属性keyにtextboxの属性名を入れる
 * 
 * @author Norihiko Yoshida
 * @version 1.0
 * 
 */
public class ErrorMessageTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6856414057411540518L;

	private String key;
	private String errorMessage;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public int doStartTag() throws JspException {
		setErrorMessage();
		if (errorMessage != null) {
			try {
				JspWriter out = pageContext.getOut();
				out.println(errorMessage);

			} catch (IOException e) {
				throw new JspException(e);
			}
		}
		errorMessage = null;
		return super.doStartTag();
	}

	private void setErrorMessage() {

		ServletRequest request = pageContext.getRequest();
		Map<String, String> errorMessageMap = (Map<String, String>) request
				.getAttribute("errorMessageMap");
		if (errorMessageMap != null) {
			this.errorMessage = errorMessageMap.get(key);
		}
	}

}
