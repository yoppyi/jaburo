package net.kronosjp.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class SustainerTag extends BodyTagSupport {

	private String categorys;
	private String categoryNames;
	private String categoryValues;
	private String id;
	private String method;
	private String[] categoryArray;
	private String[] categoryNameArray;
	private String[] categoryValueArray;

	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}

	public void setCategoryNames(String categoryNames) {
		this.categoryNames = categoryNames;
	}

	public void setCategoryValues(String categoryValues) {
		this.categoryValues = categoryValues;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public int doStartTag() throws JspException {

		JspWriter out = pageContext.getOut();
		if (categorys != null) {
			categoryArray = categorys.split(",");
		}
		if (categoryNames != null) {
			categoryNameArray = categoryNames.split(",");
		}
		if (categoryValues != null) {
			categoryValueArray = categoryValues.split(",");
		}
		try {
			// content = bodyContent.getString();
			List<String> paramList = new ArrayList<String>();
			Enumeration<String> e1 = pageContext.getRequest()
					.getParameterNames();
			HttpServletRequest request = (HttpServletRequest) pageContext
			.getRequest();
			Enumeration<String> e2 = request.getAttributeNames();
			while (e1.hasMoreElements()) {
				paramList.add(e1.nextElement());
			}
			while (e2.hasMoreElements()) {
				String a = e2.nextElement();
				paramList.add(a);
			}
			String[] paramArray = (String[]) paramList.toArray(new String[0]);

			out.print(createHtml(paramArray, request));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	private String createHtml(String[] paramArray, HttpServletRequest request) {

		StringBuilder html = new StringBuilder();
		int counter = 0;

		html.append("<table>");
		for (String category : categoryArray) {
			html.append("<tr><td width=\"100pix\">");
			html.append(category);
			html.append("</td><td><div align=\"right\"><input name=\"");
			html.append(categoryNameArray[counter]);
			html.append("\" ");
			if (id != null && counter == 0) {
				html.append("id=\"" + id + "\" ");
				html.append("type=\"checkbox\" ");
				html.append("onclick=\"" + method + "\" ");
			} else {
				html.append("type=\"checkbox\" ");
			}
			html.append("value=\"" + categoryValueArray[counter] + "\"");
			for (String param : paramArray) {
				if (categoryNameArray[counter].equals(param) && !(request.getAttribute("success") != null)) {
					html.append(" checked ");
				}
			}
			html.append("/></div></td></tr>");
			counter++;
		}
		html.append("</table>");
		return html.toString();
	}
}
