package net.kronosjp.tags;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class DateTag extends TagSupport {
	private String format;
	private String addDate;
	private String designatedDate;

	@Override
	public int doStartTag() throws JspException {

		JspWriter out = pageContext.getOut();
		try {
			out.println(createDate());
		} catch (IOException e) {
			throw new JspException(e);
		}
		return super.doStartTag();
	}

	@Override
	public int doAfterBody() throws JspException {
		format = null;
		addDate = null;
		designatedDate = null;
		return super.doAfterBody();
	}

	private String createDate() throws JspException {
		Date date = null;
		SimpleDateFormat dateFormat = null;

		if (format == null) {
			dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		} else {
			dateFormat = new SimpleDateFormat(format);
		}
		if (designatedDate == null) {
			date = new Date();
		} else {

			try {

				date = dateFormat.parse(designatedDate);

			} catch (ParseException e) {
				throw new JspException(e);
			}
		}

		if (addDate == null) {
			return dateFormat.format(date);
		}

		int addDateInt = Integer.parseInt(addDate);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, addDateInt);

		return dateFormat.format(calendar.getTime());

	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public String getDesignatedDate() {
		return designatedDate;
	}

	public void setDesignatedDate(String designatedDate) {
		this.designatedDate = designatedDate;
	}

}
