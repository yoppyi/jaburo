package net.kronosjp.tags;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;

public class LoopListCreateTableTag extends TagSupport {

	private String tableList;
	private String inclusions;
	private String category;
	private String text;
	private String error;
	private int errorInt;
	private String errorComment;
	private List<? extends Serializable> list;

	public void setTableList(String tableList) {
		this.tableList = tableList;
	}

	public void setInclusions(String inclusions) {
		this.inclusions = inclusions;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public int doStartTag() throws JspException {

		try {
			JspWriter out = pageContext.getOut();
			HttpServletRequest request;
			request = (HttpServletRequest) pageContext.getRequest();
			if (request.getSession(false).getAttribute(tableList) != null) {
				list = (List<? extends Serializable>) request.getSession(false)
						.getAttribute(tableList);
			} else {
				list = (List<? extends Serializable>) request
						.getAttribute(tableList);
			}
			out.println(createHtml(list));
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY;
	}

	private <T extends Serializable> String createHtml(List<T> list) {

		if (list == null) {
			return "";
		}
		String[] inclusionsArray = inclusions.split(",");
		String[] target = null;
		StringBuilder html = new StringBuilder();
		html.append("<tr>");
		for (T emp : list) {
			int counter = 0;
			Method[] methods = emp.getClass().getMethods();
			if (target == null) {
				target = new String[inclusionsArray.length];
			}
			lable: for (String inclusion : inclusionsArray) {
				inclusion = StringUtils.capitalize(inclusion);
				for (Method method : methods) {
					String methodName = method.getName();
					if (methodName.matches("get.*")) {
						if (inclusion.equals(methodName.substring(3))) {
							try {
								html.append("<td><div  align=\"right\">");
								if (method.invoke(emp) != null) {
									target[counter] = (String) method.invoke(
											emp).toString();
								} else {
									target[counter] = " ";
								}

								html.append(target[counter++]);
								html.append("</div></td>");
								continue lable;
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}

			if ("true".equals(text)) {
				html.append("<td><input type=\"text\" name=\"text\" value=\"");
				html.append(target[0] + "\" /></td>");
			}
			if ("checkbox".equals(category)) {
				html
						.append("<td><input type=\"checkbox\" name=\"check\" value=\"");
				html.append(target[0] + "\" /></td>");
			}
			if ("radio".equals(category)) {
				html
						.append("<td><input type=\"radio\" name=\"radio\" value=\"");
				html.append(target[0] + "\" /></td>");
			}

			try {
				if (error != null) {
					errorInt = Integer.parseInt(error);
					Map<String, String> errorMessageMap = (Map<String, String>) pageContext
							.getRequest().getAttribute("errorMessageMap");
					errorComment = errorMessageMap.get(target[errorInt]);
					System.out.println(errorComment);
					if (errorComment != null) {
						html.append("<td>" + errorComment);
						html.append("</td>");
					}
				}
			} catch (NullPointerException e) {
			}
			html.append("</tr>");
		}
		html.append("</table>");
		pageContext.getRequest().removeAttribute(tableList);
		return html.toString();
	}
}