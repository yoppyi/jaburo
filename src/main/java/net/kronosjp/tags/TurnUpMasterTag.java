package net.kronosjp.tags;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

public class TurnUpMasterTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5482625127843510469L;
	private String tableList;
	private String turnUpKey;
	private String bodyKey;
	private String nameValue;

	@Override
	public int doStartTag() throws JspException {

		try {

			JspWriter out = pageContext.getOut();
			List<? extends Serializable> list = (List<? extends Serializable>) pageContext
					.getRequest().getAttribute(tableList);
			out.println(createHtml(list));
		} catch (IOException e) {
			throw new JspException(e);
		}
		nameValue = null;
		return SKIP_BODY;
	}

	private <T extends Serializable> String createHtml(List<T> list) {
		StringBuilder html = new StringBuilder();

		try {
			String capitalizedTurnUpKey = StringUtils.capitalize(turnUpKey);
			String capitalizedBodyKey = StringUtils.capitalize(bodyKey);

			if (nameValue == null) {
				nameValue = turnUpKey;
			}

			String turnUpValue = null;
			String bodyValue = null;
			String methodName = null;
			Method methodForTurnUpKey = null;
			Method methodForbodyKey = null;
			html.append("<td><select name=\"" + nameValue + "\">");
			Class<T> klass = null;
			Method[] methods = null;

			for (T bean : list) {

				klass = (Class<T>) bean.getClass();
				methods = klass.getMethods();

				for (Method method : methods) {
					methodName = method.getName();

					if (methodName.equals("get" + capitalizedTurnUpKey)) {
						methodForTurnUpKey = method;
					}
					if (methodName.endsWith("get" + capitalizedBodyKey)) {
						methodForbodyKey = method;
					}
				}

				turnUpValue = (String) methodForTurnUpKey.invoke(bean);
				bodyValue = (String) methodForbodyKey.invoke(bean);

				html.append("<option value=\"" + turnUpValue + "\">"
						+ bodyValue + "</option> ");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		html.append("</select></td>");
		return html.toString();
	}

	public String getTableList() {
		return tableList;
	}

	public void setTableList(String tableList) {
		this.tableList = tableList;
	}

	public String getTurnUpKey() {
		return turnUpKey;
	}

	public void setTurnUpKey(String turnUpKey) {
		this.turnUpKey = turnUpKey;
	}

	public String getBodyKey() {
		return bodyKey;
	}

	public void setBodyKey(String bodyKey) {
		this.bodyKey = bodyKey;
	}

	public String getNameValue() {
		return nameValue;
	}

	public void setNameValue(String nameValue) {
		this.nameValue = nameValue;
	}
}
