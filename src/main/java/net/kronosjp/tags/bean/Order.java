package net.kronosjp.tags.bean;

import java.io.Serializable;
import java.util.Date;

import net.kronosjp.db.entity.OrderDetailsPK;

public class Order implements Serializable, Comparable<Order> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5466752788079851046L;
	private String ordIndexNo;
	private Date ordDay;
	private String cliCode;
	private String cliPName;
	private String empName;
	private String empCode;
	private String ordIndAdhibition;
	private String oIStatus;
	private String ordIndecNo;
	private Short ordDetRowNo;
	private String goodsCode;
	private String wareHouseCode;
	private Integer ordNumberOfOrder;
	private Date ordHopeDeliveryDate;
	private String oDStatus;
	private OrderDetailsPK orderDetailsPK;
	private String goodsName;
	private String warHouseCode;
	private String warName;

	// {
	// setOrdDetRowNo(orderDetailsPK.getOrdDetRowNo());
	//
	// }

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public Date getOrdDay() {
		return ordDay;
	}

	public void setOrdDay(Date ordDay) {
		this.ordDay = ordDay;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getOrdIndAdhibition() {
		return ordIndAdhibition;
	}

	public void setOrdIndAdhibition(String ordIndAdhibition) {
		this.ordIndAdhibition = ordIndAdhibition;
	}

	public String getOIStatus() {
		return oIStatus;
	}

	public void setOIStatus(String status) {
		oIStatus = status;
	}

	public String getOrdIndecNo() {
		return ordIndecNo;
	}

	public void setOrdIndecNo(String ordIndecNo) {
		this.ordIndecNo = ordIndecNo;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getWareHouseCode() {
		return wareHouseCode;
	}

	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}

	public Integer getOrdNumberOfOrder() {
		return ordNumberOfOrder;
	}

	public void setOrdNumberOfOrder(Integer ordNumberOfOrder) {
		this.ordNumberOfOrder = ordNumberOfOrder;
	}

	public Date getOrdHopeDeliveryDate() {
		return ordHopeDeliveryDate;
	}

	public void setOrdHopeDeliveryDate(Date ordHopeDeliveryDate) {
		this.ordHopeDeliveryDate = ordHopeDeliveryDate;
	}

	public String getODStatus() {
		return oDStatus;
	}

	public void setODStatus(String status) {
		oDStatus = status;
	}

	public OrderDetailsPK getOrderDetailsPK() {
		return orderDetailsPK;
	}

	public void setOrderDetailsPK(OrderDetailsPK orderDetailsPK) {
		this.orderDetailsPK = orderDetailsPK;
	}

	public Short getOrdDetRowNo() {
		return ordDetRowNo;
	}

	public void setOrdDetRowNo(Short ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}

	public String getCliPName() {
		return cliPName;
	}

	public void setCliPName(String cliPName) {
		this.cliPName = cliPName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getWarHouseCode() {
		return warHouseCode;
	}

	public void setWarHouseCode(String warHouseCode) {
		this.warHouseCode = warHouseCode;
	}

	public String getWarName() {
		return warName;
	}

	public void setWarName(String warName) {
		this.warName = warName;
	}

	@Override
	public int compareTo(Order arg0) {
		// TODO Auto-generated method stub
		return this.ordIndexNo.compareTo(arg0.getOrdIndexNo());
	}
}
