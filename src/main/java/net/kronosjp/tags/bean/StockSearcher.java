package net.kronosjp.tags.bean;

import java.io.Serializable;
import java.util.Date;

public class StockSearcher implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 207556302714490590L;
	private String ordIndexNo;
	private String goodsCode;
	private String warehouseCode;
	private Integer ordNumberOfOrder;
	private Date ordHopeDeliveryDate;
	private Integer numberThatHasBeenDrawn;
	private String goodsName;

	public String getOrdIndexNo() {
		return ordIndexNo;
	}

	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}





	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	@Override
	public String toString() {
		String temp = ordIndexNo + goodsName + ordNumberOfOrder
				+ numberThatHasBeenDrawn + ordHopeDeliveryDate;
		return temp;
	}

	public Date getOrdHopeDeliveryDate() {
		return ordHopeDeliveryDate;
	}

	public void setOrdHopeDeliveryDate(Date ordHopeDeliveryDate) {
		this.ordHopeDeliveryDate = ordHopeDeliveryDate;
	}

	public Integer getOrdNumberOfOrder() {
		return ordNumberOfOrder;
	}

	public void setOrdNumberOfOrder(Integer ordNumberOfOrder) {
		this.ordNumberOfOrder = ordNumberOfOrder;
	}

	public void setNumberThatHasBeenDrawn(Integer numberThatHasBeenDrawn) {
		this.numberThatHasBeenDrawn = numberThatHasBeenDrawn;
	}

	public Integer getNumberThatHasBeenDrawn() {
		return numberThatHasBeenDrawn;
	}
}
