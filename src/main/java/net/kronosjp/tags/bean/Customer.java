package net.kronosjp.tags.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Sale用のビーン！
 * 
 * @author うえにしまこと
 * 
 */
public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8005547855740152928L;
	// CustomerMonthlyTradingSummary SaleDetails
	// <td>得意先Code</td>
	// <td>売上No</td>
	// <td>売上区分</td>
	// <td>売上日</td>
	// <td>売上金額</td>
	//	
	private String cliCode;
	private String salHeadNo;
	private String salDivision;
	private Date salDay;
	private Integer salesMonthTotal;
	private Date cusYearMonth;

	public String getSalHeadNo() {
		return salHeadNo;
	}

	public void setSalHeadNo(String salHeadNo) {
		this.salHeadNo = salHeadNo;
	}

	public String getSalDivision() {
		return salDivision;
	}

	public void setSalDivision(String salDivision) {
		this.salDivision = salDivision;
	}

	public Date getSalDay() {
		return salDay;
	}

	public void setSalDay(Date salDay) {
		this.salDay = salDay;
	}

	public Integer getSalesMonthTotal() {
		return salesMonthTotal;
	}

	public void setSalesMonthTotal(Integer salesMonthTotal) {
		this.salesMonthTotal = salesMonthTotal;
	}

	public String getCliCode() {
		return cliCode;
	}

	public void setCliCode(String cliCode) {
		this.cliCode = cliCode;
	}

	public Date getCusYearMonth() {
		return cusYearMonth;
	}

	public void setCusYearMonth(Date cusYearMonth) {
		this.cusYearMonth = cusYearMonth;
	}

}
