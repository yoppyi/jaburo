package net.kronosjp.tags.bean;

import java.io.Serializable;

public class MoneyCreater implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7565070044422368479L;
	private String monRecNo;
	private String demAddCode;
	private String cusName;
	private String denomination;
	private String monRecDay;
	private Integer monRecAmount;
	
	
	public String getMonRecNo() {
		return monRecNo;
	}
	public void setMonRecNo(String monRecNo) {
		this.monRecNo = monRecNo;
	}
	public String getDemAddCode() {
		return demAddCode;
	}
	public void setDemAddCode(String demAddCode) {
		this.demAddCode = demAddCode;
	}
	public String getCusName() {
		return cusName;
	}
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	public String getDenomination() {
		return denomination;
	}
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
	public String getMonRecDay() {
		return monRecDay;
	}
	public void setMonRecDay(String monRecDay) {
		this.monRecDay = monRecDay;
	}
	public Integer getMonRecAmount() {
		return monRecAmount;
	}
	public void setMonRecAmount(Integer monRecAmount) {
		this.monRecAmount = monRecAmount;
	}
}
