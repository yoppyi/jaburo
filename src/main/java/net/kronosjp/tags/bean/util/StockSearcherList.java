package net.kronosjp.tags.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.StockAccordingWareHouse;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.tags.bean.StockSearcher;
import net.kronosjp.util.bean.BeanUtils;

public class StockSearcherList {

	public static List<StockSearcher> add(List<StockAccordingWareHouse> stockAccordingWarehouseList)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		
		List<StockSearcher> list = new ArrayList<StockSearcher>();
		Set<OrderDetails> orderDetailsSet = null;
		OrderDetails orderDetails = null;
		StockSearcher orderStockMaster = null;
		
		// 
		for (StockAccordingWareHouse stoAccWarList : stockAccordingWarehouseList) {// 送られたリスト → オーダーインデックス
			orderDetailsSet = (Set<OrderDetails>) stoAccWarList.getOrderDetailsSet();
			Iterator<OrderDetails> iterator = orderDetailsSet.iterator();
			
			
			while (iterator.hasNext()) {
				orderDetailsSet = (Set<OrderDetails>) iterator.next();
				orderStockMaster = BeanUtils.compareAndSetBean(StockSearcher.class, stoAccWarList,
						orderDetails);
				list.add(orderStockMaster);
			}
		}
		return list;
	}

}
