package net.kronosjp.tags.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.ClientMaster;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.util.bean.BeanUtils;

public class OrdersList {

	public static List<Order> add(List<OrderIndex> odindexlist)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		List<Order> list = new ArrayList<Order>();

		Set<OrderDetails> orderDetailsSet = null;// オーダーディティール型のSetを呼び出し
		OrderDetails orderDetails = null;// オーダーディティール型を宣言
		Order order = null;//
		ClientMaster clientMaster = null;

		for (OrderIndex orderIndex : odindexlist) {// 送られたリスト → オーダーインデックス
			orderDetailsSet = orderIndex.getOrderDetailsSet();
			if (odindexlist != null) {
				Iterator<OrderDetails> iterator = orderDetailsSet.iterator();

				while (iterator.hasNext()) {
					orderDetails = iterator.next();
					order = BeanUtils.compareAndSetBean(Order.class,
							orderDetails, orderIndex);

					order.setCliPName(orderIndex.getClientMaster()
							.getCliPName());
					list.add(order);
				}
			}
		}
		return list;
	}

}
