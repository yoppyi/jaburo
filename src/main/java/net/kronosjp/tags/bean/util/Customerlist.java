package net.kronosjp.tags.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.kronosjp.db.entity.CustomerMonthlyTradingSummary;
import net.kronosjp.db.entity.SalesHeadline;
import net.kronosjp.tags.bean.Customer;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.util.bean.BeanUtils;

public class Customerlist {

	List<Customer> list = new ArrayList<Customer>();

	Set<SalesHeadline> salesHeadlineSet = null;
	SalesHeadline salesHeadline = null;
	Customer customer = null;

	public List add(List<CustomerMonthlyTradingSummary> custlist)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		for (CustomerMonthlyTradingSummary customerMonthlyTradingSummary : custlist) {
			salesHeadlineSet = customerMonthlyTradingSummary
					.getSalesHeadlineSet();
			Iterator<SalesHeadline> iterator = salesHeadlineSet.iterator();

			while (iterator.hasNext()) {
				salesHeadline = iterator.next();
				customer = BeanUtils.compareAndSetBean(Customer.class,
						customerMonthlyTradingSummary, salesHeadline);
				// order.setCliPName(orderIndex.getClientMaster().getCliPName());
				list.add(customer);
			}
		}
		return list;
	}
}
