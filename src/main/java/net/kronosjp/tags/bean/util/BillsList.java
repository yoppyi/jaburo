package net.kronosjp.tags.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.kronosjp.db.entity.BillAddressMaster;
import net.kronosjp.db.entity.BillMonthlyTradingSummary;
import net.kronosjp.db.entity.Billheadlines;
import net.kronosjp.tags.bean.Bill;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.util.bean.BeanUtils;

/**請求用リスト作成
 * @author makoto uensihi
 * 
 *
 */
public class BillsList {

	public static void add(List<Billheadlines> maybilllist)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		List<Bill> list = new ArrayList<Bill>();
		Bill bill = null;
		BillMonthlyTradingSummary billMonthlyTradingSummary = null;
		Billheadlines billheadlines = null;

		for (int i = 0; i < maybilllist.size(); i++) {

			bill = BeanUtils.compareAndSetBean(Bill.class, billheadlines,
					billMonthlyTradingSummary);
			list.add(bill);
		}
	}
}
