package net.kronosjp.tags.bean.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.CustomerMonthlyTradingSummary;
import net.kronosjp.db.entity.CustomerMonthlyTradingSummaryPK;
import net.kronosjp.db.entity.SalesHeadline;
import net.kronosjp.tags.bean.Customer;
import net.kronosjp.util.bean.BeanUtils;

public class CustomerListCreator {
	public static List<Customer> createList(
			List<SalesHeadline> salesHeadlineList, HttpServletRequest request)
			throws ServletException {

		List<Customer> customerList = new ArrayList<Customer>();
		List<CustomerMonthlyTradingSummary> customerMonthlyTradingSummaryList = null;

		for (SalesHeadline salesHeadline : salesHeadlineList) {
			customerMonthlyTradingSummaryList = DaoBase.search(
					CustomerMonthlyTradingSummary.class, "cliCode",
					salesHeadline.getSalCustomerCode(), request);

			try {
				for (CustomerMonthlyTradingSummary customerMonthlyTradingSummary : customerMonthlyTradingSummaryList) {
					customerList.add(BeanUtils.compareAndSetBean(
							Customer.class, salesHeadline,
							customerMonthlyTradingSummary));
				}
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		return customerList;
	}
}
