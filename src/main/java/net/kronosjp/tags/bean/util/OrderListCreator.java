package net.kronosjp.tags.bean.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import net.kronosjp.db.dao.DaoBase;
import net.kronosjp.db.entity.ClientMaster;
import net.kronosjp.db.entity.EmployeeMaster;
import net.kronosjp.db.entity.GoodsMaster;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.WareHouseMaster;
import net.kronosjp.exception.SystemErrorException;
import net.kronosjp.tags.bean.Order;
import net.kronosjp.util.bean.BeanUtils;

public class OrderListCreator {
	public static List<Order> createListForOderDelete(HttpServletRequest request)
			throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();

		// OrderIndexテーブル取得////////////////////////////////////////////////////////////////////////ここから▽
		List<OrderIndex> orderIndexList = DaoBase.getTable(OrderIndex.class,
				request);
		// OrderIndexテーブル取得////////////////////////////////////////////////////////////////////////ここまで△

		List<ClientMaster> clientMasterLlist = null;

		for (OrderIndex orderIndex : orderIndexList) {

			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここから▽
			clientMasterLlist = DaoBase.search(ClientMaster.class, "cliCode",
					orderIndex.getCliCode(), request);
			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここまで△

			if (!clientMasterLlist.isEmpty()) {// *********** ぬるぽ怖いし安全のためにチェック

				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここから▽
				try {

					// テスト******************************************************ここから▽
					// System.out
					// .println(BeanUtils.compareAndSetBean(Order.class,
					// orderIndex, clientMasterLlist.get(0))
					// .getCliPName());
					// テスト******************************************************ここまで△

					orderList.add(BeanUtils.compareAndSetBean(Order.class,
							orderIndex, clientMasterLlist.get(0)));

				} catch (Exception e) {
					throw new SystemErrorException(e);
				}
				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここまで△
			}
		}
		return orderList;
	}

	public static List<Order> createListForOrderDetailOfSearch(
			HttpServletRequest request) throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();
		List<OrderDetails> orderDetailsList = DaoBase.search(
				OrderDetails.class, "ordIndexNo", request
						.getParameter("ordIndexNo"), request);
		for (OrderDetails orderDetails : orderDetailsList) {

			try {
				orderList.add(BeanUtils.compareAndSetBean(Order.class,
						orderDetails, DaoBase.keySearch(GoodsMaster.class,
								orderDetails.getGoodsCode(), request), DaoBase
								.keySearch(WareHouseMaster.class, orderDetails
										.getWareHouseCode(), request)));
			} catch (Exception e) {
				throw new SystemErrorException(e);
			}
		}
		return orderList;
	}

	public static List<Order> createListForOrderDetail(
			HttpServletRequest request) throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();
		List<OrderDetails> orderDetailsList = DaoBase.getTable(
				OrderDetails.class, request);
		for (OrderDetails orderDetails : orderDetailsList) {

			try {
				orderList.add(BeanUtils.compareAndSetBean(Order.class,
						orderDetails, DaoBase.keySearch(GoodsMaster.class,
								orderDetails.getGoodsCode(), request), DaoBase
								.keySearch(WareHouseMaster.class, orderDetails
										.getWareHouseCode(), request)));

				// /////////////////////////////////////////////////////////////////////////////////////
				//				
				// System.out.println(BeanUtils.compareAndSetBean(
				// Order.class,
				// orderDetails,
				// DaoBase.keySearch(GoodsMaster.class, orderDetails
				// .getGoodsCode(), request),
				// DaoBase.keySearch(WareHouseMaster.class, orderDetails
				// .getWareHouseCode(), request)).getOrdNumberOfOrder()
				// + "*****************************************");
				//
				// /////////////////////////////////////////////////////////////////////////////////////
			} catch (Exception e) {
				throw new SystemErrorException(e);
			}
		}
		return orderList;
	}

	public static List<Order> createListForOrderDetailOfUpdate(
			HttpServletRequest request, String ordIndexNo)
			throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();
		List<OrderDetails> orderDetailsList = DaoBase.search(
				OrderDetails.class, "ordIndexNo", request
						.getParameter("ordIndexNo"), request);
		for (OrderDetails orderDetails : orderDetailsList) {

			try {
				orderList.add(BeanUtils.compareAndSetBean(Order.class,
						orderDetails, DaoBase.keySearch(GoodsMaster.class,
								orderDetails.getGoodsCode(), request), DaoBase
								.keySearch(WareHouseMaster.class, orderDetails
										.getWareHouseCode(), request)));
			} catch (Exception e) {
				throw new SystemErrorException(e);
			}
		}
		return orderList;
	}

	public static List<Order> createListForOderCaption(
			HttpServletRequest request) throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();

		// OrderIndexテーブル取得////////////////////////////////////////////////////////////////////////ここから▽
		List<OrderIndex> orderIndexList = DaoBase.getTable(OrderIndex.class,
				request);
		// OrderIndexテーブル取得////////////////////////////////////////////////////////////////////////ここまで△

		List<ClientMaster> clientMasterLlist = null;
		List<EmployeeMaster> employeeMasterList = null;
		for (OrderIndex orderIndex : orderIndexList) {

			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここから▽
			clientMasterLlist = DaoBase.search(ClientMaster.class, "cliCode",
					orderIndex.getCliCode(), request);
			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここまで△
			employeeMasterList = DaoBase.search(EmployeeMaster.class,
					"empCode", orderIndex.getEmpCode(), request);

			if (!clientMasterLlist.isEmpty() && !employeeMasterList.isEmpty()) {// ***********
				// ぬるぽ怖いし安全のためにチェック

				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここから▽
				try {

					// テスト******************************************************ここから▽
					// System.out
					// .println(BeanUtils.compareAndSetBean(Order.class,
					// orderIndex, clientMasterLlist.get(0))
					// .getCliPName());
					// テスト******************************************************ここまで△

					orderList.add(BeanUtils.compareAndSetBean(Order.class,
							orderIndex, clientMasterLlist.get(0),
							employeeMasterList.get(0)));

				} catch (Exception e) {
					throw new SystemErrorException(e);
				}
				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここまで△
			}
		}
		return orderList;
	}

	public static List<OrderIndex> createSearchedListForOderCaption(
			HttpServletRequest request) throws SystemErrorException {

		String flag = request.getParameter("search");
		String keyWord = null;
		String hql = "from OrderIndex as oi where oi.";
		Session session = DaoBase.createSession(request);
		session.beginTransaction();
		List<OrderIndex> orderIndexList = DaoBase.getTable(OrderIndex.class,
				request);
		Query query = null;

		if (flag != null) {

			if (flag.equals("ordIndexNo")) {
				keyWord = request.getParameter("ordIndexNo");
				hql = hql + "ordIndexNo='" + keyWord + "'";
				System.out.println(hql);
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}

			if (flag.equals("cliCode")) {
				keyWord = request.getParameter("cliCode");
				hql = hql + "cliCode='" + keyWord + "'";
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}

			if (flag.equals("e4")) {
				keyWord = StringUtils.replace(request.getParameter("e4"), "/",
						"-");
				hql = hql + "ordDay='" + keyWord + "'";
				query = session.createQuery(hql);
				orderIndexList = (List<OrderIndex>) query.list();
			}
		}

		return orderIndexList;
	}

	public static List<Order> createListForOderCaption(List<OrderIndex> orderIndexList,
			HttpServletRequest request) throws SystemErrorException {
		List<Order> orderList = new ArrayList<Order>();

		List<ClientMaster> clientMasterLlist = null;
		List<EmployeeMaster> employeeMasterList = null;
		for (OrderIndex orderIndex : orderIndexList) {

			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここから▽
			clientMasterLlist = DaoBase.search(ClientMaster.class, "cliCode",
					orderIndex.getCliCode(), request);
			// ClientMasterをcliCodeで検索//////////////////////////////////////////////////////////////////ここまで△
			employeeMasterList = DaoBase.search(EmployeeMaster.class,
					"empCode", orderIndex.getEmpCode(), request);

			if (!clientMasterLlist.isEmpty() && !employeeMasterList.isEmpty()) {// ***********
				// ぬるぽ怖いし安全のためにチェック

				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここから▽
				try {

					// テスト******************************************************ここから▽
					// System.out
					// .println(BeanUtils.compareAndSetBean(Order.class,
					// orderIndex, clientMasterLlist.get(0))
					// .getCliPName());
					// テスト******************************************************ここまで△

					orderList.add(BeanUtils.compareAndSetBean(Order.class,
							orderIndex, clientMasterLlist.get(0),
							employeeMasterList.get(0)));

				} catch (Exception e) {
					throw new SystemErrorException(e);
				}
				// OrderにOrderIndexとClientMasterの値をつめる//////////////////////////////////////////////////ここまで△
			}
		}
		return orderList;
	}

}
