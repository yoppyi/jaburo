package net.kronosjp.tags.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.kronosjp.db.entity.OrderDetails;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.SalesDetails;
import net.kronosjp.db.entity.ShipmentDetails;
import net.kronosjp.db.entity.ShipmentHeadline;
import net.kronosjp.tags.bean.ShipOrderdetail;
import net.kronosjp.util.bean.BeanUtils;

/**出荷明細用Beanエンティティ
 * 起動しない可能性もあり・・・
 * 
 * 
 * @author 上西　誠人
 *
 */
public class ShipOrderDetaillist {

	List<ShipOrderdetail> list = new ArrayList<ShipOrderdetail>();
	ShipOrderdetail ship = null;
	ShipmentDetails shipmentDetails = null;
	Set<ShipmentDetails> shipmentDetailsSet = null;
	ShipmentHeadline shipmentHeadline = null;

	/**もらったリストからビーンユーティルを作成するメソッド
	 * 現時点若干の問題あり。
	 * 正しく起動しない可能性あり。
	 * 
	 * @param List<Orderdetails> sshiplist
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void add(List<OrderDetails> sshiplist) throws SecurityException,
			IllegalArgumentException, NoSuchMethodException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException {

		for (OrderDetails orderdetails : sshiplist) {
			shipmentDetailsSet = orderdetails.getShipmentDetailsSet();
			
			Iterator<ShipmentDetails> iterator = shipmentDetailsSet.iterator();
				while (iterator.hasNext()) {
					shipmentDetails = iterator.next();
					ship = BeanUtils.compareAndSetBean(ShipOrderdetail.class,
							shipmentHeadline, shipmentDetails);
					list.add(ship);
				}

				
				//				for (shipmentHeadline :orderdetails) {
//					shipmentDetailsSet = shipmentHeadline.getShipmentDetailsSet();
//
//				}
		}
	}
}
