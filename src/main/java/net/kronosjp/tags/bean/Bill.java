package net.kronosjp.tags.bean;

import java.io.Serializable;
import java.util.Date;

public class Bill implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5660469523076750556L;
	private String bilHeadNo;
	private String bilAddCode;
	private Date bilDate;
	private Date dateOfSummingUp;
	private Integer bilAmount;
	private Integer erasingAmount;
	private Date bsYearMonth;
	private Integer bsBilTotalThisMonth;
	private Integer bsLastMonthBalance;
	private Integer bsMonRecTotalThisMonth;
	
	
	public String getBilHeadNo() {
		return bilHeadNo;
	}
	public void setBilHeadNo(String bilHeadNo) {
		this.bilHeadNo = bilHeadNo;
	}
	public String getBilAddCode() {
		return bilAddCode;
	}
	public void setBilAddCode(String bilAddCode) {
		this.bilAddCode = bilAddCode;
	}
	public Date getBilDate() {
		return bilDate;
	}
	public void setBilDate(Date bilDate) {
		this.bilDate = bilDate;
	}
	public Date getDateOfSummingUp() {
		return dateOfSummingUp;
	}
	public void setDateOfSummingUp(Date dateOfSummingUp) {
		this.dateOfSummingUp = dateOfSummingUp;
	}
	public Integer getBilAmount() {
		return bilAmount;
	}
	public void setBilAmount(Integer bilAmount) {
		this.bilAmount = bilAmount;
	}
	public Integer getErasingAmount() {
		return erasingAmount;
	}
	public void setErasingAmount(Integer erasingAmount) {
		this.erasingAmount = erasingAmount;
	}
	public Date getBsYearMonth() {
		return bsYearMonth;
	}
	public void setBsYearMonth(Date bsYearMonth) {
		this.bsYearMonth = bsYearMonth;
	}
	public Integer getBsBilTotalThisMonth() {
		return bsBilTotalThisMonth;
	}
	public void setBsBilTotalThisMonth(Integer bsBilTotalThisMonth) {
		this.bsBilTotalThisMonth = bsBilTotalThisMonth;
	}
	public Integer getBsLastMonthBalance() {
		return bsLastMonthBalance;
	}
	public void setBsLastMonthBalance(Integer bsLastMonthBalance) {
		this.bsLastMonthBalance = bsLastMonthBalance;
	}
	public Integer getBsMonRecTotalThisMonth() {
		return bsMonRecTotalThisMonth;
	}
	public void setBsMonRecTotalThisMonth(Integer bsMonRecTotalThisMonth) {
		this.bsMonRecTotalThisMonth = bsMonRecTotalThisMonth;
	}

	
}
