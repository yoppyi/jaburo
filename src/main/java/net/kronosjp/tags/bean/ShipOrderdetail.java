package net.kronosjp.tags.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import net.kronosjp.db.entity.OrderDetailsPK;
import net.kronosjp.db.entity.OrderIndex;
import net.kronosjp.db.entity.ShipmentDetails;
import net.kronosjp.db.entity.StockAccordingWareHouse;

public class ShipOrderdetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7362668398573476191L;
	// ShipmentDetails（出荷明細）テーブルより
	private String ordIndexNo;
	private String ordDetRowNo;
	private String shipInstallmentsNo;
	private String shipHeadNo;
	private Date shipScheduledDay;
	private Integer shipScheduleAmount;
	private Integer shipNumberOfAmount;
	private String ShipScheduleWareHouseCode;
	private String ShipNumberOfWareHouseCode;
	private String salesSourceNo;
	private String ShipStatus;
	
	// ShipmentHeadline（出荷見出）テーブルより
	private Date shipDay;
	private String carCode;
	private String shipHeadInquiry;
	private String warHouseCode;
	private String shipDesCode;
	
	
	private Set<ShipmentDetails> shipmentDetailsSet;
	
	// OrderDetails（受注明細）テーブルより
	private String ordIndecNo;
	private String goodsCode;
	private String wareHouseCode;
	private Integer ordNumberOfOrder;
	private Date ordHopeDeliveryDate;
	private String oDStatus;
	
	
	private OrderDetailsPK orderDetailsPK;
	private Set<OrderIndex> orderIndexSet;
	private Set<StockAccordingWareHouse> stockAccordingWarehouseSet;
	
	public String getOrdIndexNo() {
		return ordIndexNo;
	}
	public void setOrdIndexNo(String ordIndexNo) {
		this.ordIndexNo = ordIndexNo;
	}
	public String getOrdDetRowNo() {
		return ordDetRowNo;
	}
	public void setOrdDetRowNo(String ordDetRowNo) {
		this.ordDetRowNo = ordDetRowNo;
	}
	public String getShipInstallmentsNo() {
		return shipInstallmentsNo;
	}
	public void setShipInstallmentsNo(String shipInstallmentsNo) {
		this.shipInstallmentsNo = shipInstallmentsNo;
	}
	public String getShipHeadNo() {
		return shipHeadNo;
	}
	public void setShipHeadNo(String shipHeadNo) {
		this.shipHeadNo = shipHeadNo;
	}
	public Date getShipScheduledDay() {
		return shipScheduledDay;
	}
	public void setShipScheduledDay(Date shipScheduledDay) {
		this.shipScheduledDay = shipScheduledDay;
	}
	public Integer getShipScheduleAmount() {
		return shipScheduleAmount;
	}
	public void setShipScheduleAmount(Integer shipScheduleAmount) {
		this.shipScheduleAmount = shipScheduleAmount;
	}
	public Integer getShipNumberOfAmount() {
		return shipNumberOfAmount;
	}
	public void setShipNumberOfAmount(Integer shipNumberOfAmount) {
		this.shipNumberOfAmount = shipNumberOfAmount;
	}
	public String getShipScheduleWareHouseCode() {
		return ShipScheduleWareHouseCode;
	}
	public void setShipScheduleWareHouseCode(String shipScheduleWareHouseCode) {
		ShipScheduleWareHouseCode = shipScheduleWareHouseCode;
	}
	public String getShipNumberOfWareHouseCode() {
		return ShipNumberOfWareHouseCode;
	}
	public void setShipNumberOfWareHouseCode(String shipNumberOfWareHouseCode) {
		ShipNumberOfWareHouseCode = shipNumberOfWareHouseCode;
	}
	public String getSalesSourceNo() {
		return salesSourceNo;
	}
	public void setSalesSourceNo(String salesSourceNo) {
		this.salesSourceNo = salesSourceNo;
	}
	public String getShipStatus() {
		return ShipStatus;
	}
	public void setShipStatus(String shipStatus) {
		ShipStatus = shipStatus;
	}
	public Date getShipDay() {
		return shipDay;
	}
	public void setShipDay(Date shipDay) {
		this.shipDay = shipDay;
	}
	public String getCarCode() {
		return carCode;
	}
	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}
	public String getShipHeadInquiry() {
		return shipHeadInquiry;
	}
	public void setShipHeadInquiry(String shipHeadInquiry) {
		this.shipHeadInquiry = shipHeadInquiry;
	}
	public String getWarHouseCode() {
		return warHouseCode;
	}
	public void setWarHouseCode(String warHouseCode) {
		this.warHouseCode = warHouseCode;
	}
	public String getShipDesCode() {
		return shipDesCode;
	}
	public void setShipDesCode(String shipDesCode) {
		this.shipDesCode = shipDesCode;
	}
	public Set<ShipmentDetails> getShipmentDetailsSet() {
		return shipmentDetailsSet;
	}
	public void setShipmentDetailsSet(Set<ShipmentDetails> shipmentDetailsSet) {
		this.shipmentDetailsSet = shipmentDetailsSet;
	}
	public String getOrdIndecNo() {
		return ordIndecNo;
	}
	public void setOrdIndecNo(String ordIndecNo) {
		this.ordIndecNo = ordIndecNo;
	}
	public String getGoodsCode() {
		return goodsCode;
	}
	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}
	public String getWareHouseCode() {
		return wareHouseCode;
	}
	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}
	public Integer getOrdNumberOfOrder() {
		return ordNumberOfOrder;
	}
	public void setOrdNumberOfOrder(Integer ordNumberOfOrder) {
		this.ordNumberOfOrder = ordNumberOfOrder;
	}
	public Date getOrdHopeDeliveryDate() {
		return ordHopeDeliveryDate;
	}
	public void setOrdHopeDeliveryDate(Date ordHopeDeliveryDate) {
		this.ordHopeDeliveryDate = ordHopeDeliveryDate;
	}
	public String getODStatus() {
		return oDStatus;
	}
	public void setODStatus(String status) {
		oDStatus = status;
	}
	public OrderDetailsPK getOrderDetailsPK() {
		return orderDetailsPK;
	}
	public void setOrderDetailsPK(OrderDetailsPK orderDetailsPK) {
		this.orderDetailsPK = orderDetailsPK;
	}
	public Set<OrderIndex> getOrderIndexSet() {
		return orderIndexSet;
	}
	public void setOrderIndexSet(Set<OrderIndex> orderIndexSet) {
		this.orderIndexSet = orderIndexSet;
	}
	public Set<StockAccordingWareHouse> getStockAccordingWarehouseSet() {
		return stockAccordingWarehouseSet;
	}
	public void setStockAccordingWarehouseSet(
			Set<StockAccordingWareHouse> stockAccordingWarehouseSet) {
		this.stockAccordingWarehouseSet = stockAccordingWarehouseSet;
	}

}
