CREATE
  TABLE PayMaster(
    PayTable VARCHAR(40) NOT NULL
    ,PayCode VARCHAR(3) NOT NULL
    ,PayDivName VARCHAR(50) NOT NULL
    ,PayFieldName VARCHAR(50) NOT NULL
    ,CONSTRAINT PayMasterKEY_NAME PRIMARY KEY (
      PayTable
      ,PayCode
    )
  );
  CREATE
  TABLE PackingMaster(
    PacCode VARCHAR(5) PRIMARY KEY
    ,PacName VARCHAR(20) NOT NULL
    ,PacNumberOfInsertions INTEGER NOT NULL
  );
CREATE
  TABLE RollMaster(
    RollID VARCHAR(5) PRIMARY KEY
    ,RollName VARCHAR(20) NOT NULL
    ,RollLevel VARCHAR(5) NOT NULL
  );
  CREATE
  TABLE WarehouseMaster(
    WarHouseCode VARCHAR(5) PRIMARY KEY
    ,WarName VARCHAR(20) NOT NULL
    ,WarAddress VARCHAR(100) NOT NULL
  );
  CREATE
  TABLE CarrierMaster(
    CarCode VARCHAR(5) PRIMARY KEY
    ,CarName VARCHAR(20) NOT NULL
  );
  CREATE
  TABLE EmployeeMaster(
    EmpCode VARCHAR(5) PRIMARY KEY
    ,EmpName VARCHAR(20) NOT NULL
    ,RollID VARCHAR(5) NOT NULL
    ,EmpPass VARCHAR(8) NOT NULL
    ,FOREIGN KEY (RollID) REFERENCES Rollmaster(RollID)
  );
CREATE
  TABLE EnterpriseGroupMaster(
    EGCode VARCHAR(5) PRIMARY KEY
    ,EGName VARCHAR(100) NOT NULL
  );
CREATE
  TABLE BillAddressMaster(
    BilAddCode VARCHAR(5) PRIMARY KEY
    ,BilClosingDate DATE NOT NULL
    ,BilPayment VARCHAR(3) NOT NULL references PayMaster(PayCode)
    ,BilPaymentSite INTEGER(10) NOT NULL
    ,BilCreditLine INTEGER NOT NULL
  );
CREATE
  TABLE BillHeadLines(
    BilHeadNo VARCHAR(5) PRIMARY KEY
    ,BilAddCode VARCHAR(5) NOT NULL references BillAddressMaster(BilAddCode)
    ,BilDate DATE NOT NULL
    ,DateOfSummingUp DATE NOT NULL
    ,BilAmount INTEGER NOT NULL
    ,ErasingAmount INTEGER NOT NULL
  );
CREATE
  TABLE BillMonthlyTradingSummary(
    BilAddCode VARCHAR(5) NOT NULL references BillAddressMaster(BilAddCode)
    ,BSYearMonth DATE NOT NULL
    ,BSLastMonthBalance INTEGER NOT NULL
    ,BSBilTotalThisMonth INTEGER NOT NULL
    ,BSMonRecTotalThisMonth INTEGER
    ,CONSTRAINT BillMonthlyKEY_NAME PRIMARY KEY (
      BSYearMonth
      ,BilAddCode
    )
  );

CREATE
  TABLE ChargeBackDetails(
    MonRecNo VARCHAR(5) NOT NULL
    ,BilHeadNo VARCHAR(5) NOT NULL references Billheadlines(BilHeadNo)
    ,ErasingAmount INTEGER NOT NULL
    ,CONSTRAINT ChargeBackDetailsKEY_NAME PRIMARY KEY (
      MonRecNo
      ,BilHeadNo
      ,ErasingAmount
    )
  );
CREATE
  TABLE ClientMaster(
    CliCode VARCHAR(5) PRIMARY KEY
    ,CliPName VARCHAR(20) NOT NULL
    ,CliSalesCode VARCHAR(5) NOT NULL references EmployeeMaster(EmpCode)
    ,CliBilCode VARCHAR(5) NOT NULL references BillAddressMaster(BilAddCode)
    ,CliDesCode VARCHAR(5) NOT NULL references ShipDestinationMaster(ShipDesCode)
  );
CREATE
  TABLE CustomerMaster(
    CusCode VARCHAR(5) PRIMARY KEY
    ,CusName VARCHAR(50) NOT NULL
    ,CusAddress VARCHAR(100) NOT NULL
    ,CusTel VARCHAR(11) NOT NULL
    ,CusEGCode VARCHAR(5) NOT NULL references EnterpriseGroupMaster(EGCode)
    ,CusObjectDivision bool NOT NULL
    ,CusShipDivision bool NOT NULL
    ,CusSupplierDivision bool NOT NULL
    ,CusBilDivision bool NOT NULL
    ,CusPayDivision bool NOT NULL
  );
CREATE
  TABLE CustomerMonthlyTradingSummary(
    CliCode VARCHAR(5) NOT NULL references ClientMaster(CliCode)
    ,CusYearMonth DATE NOT NULL
    ,SalesMonthTotal INTEGER NOT NULL
    ,CONSTRAINT CustomerMonthlyTradingSummaryKEY_NAME PRIMARY KEY (
      CliCode
      ,CusYearMonth
    )
  );

CREATE
  TABLE GoodsMaster(
    GoodsCode VARCHAR(10) PRIMARY KEY
    ,GoodsName VARCHAR(20) NOT NULL
    ,PacCode VARCHAR(5) NOT NULL references PackingMaster(PacCode)
    ,StandardSalesUnitPrice INTEGER NOT NULL
  );
CREATE
  TABLE MoneyReceivedHeadline(
    MonRecNo VARCHAR(5) PRIMARY KEY
    ,DemAddCode VARCHAR(5) NOT NULL references BillAddressMaster(BilAddCode)
    ,Denomination VARCHAR(3) NOT NULL references PayMaster(PayCode)
    ,MonRecDay DATE NOT NULL
    ,MonRecAmount INTEGER NOT NULL
  );
CREATE
  TABLE OrderDetails(
    OrdIndexNo VARCHAR(5) NOT NULL references OrderIndex(OrderIndexNo)
    ,OrdDetRowNo SMALLINT NOT NULL
    ,GoodsCode VARCHAR(10) NOT NULL references GoodsMaster(GoosCode)
    ,WareHouseCode VARCHAR(5) NOT NULL references WarehouseMaster(WarhouseCode)
    ,OrdNumberOfOrder INTEGER NOT NULL
    ,OrdHopeDeliveryDate DATE NOT NULL
    ,ODStatus VARCHAR(3) NOT NULL references PayMaster(PayCode)
    ,CONSTRAINT OrderDetailsKEY_NAME PRIMARY KEY (
      OrdDetRowNo
      ,OrdIndexNo
    )
  );
CREATE
  TABLE OrderIndex(
    OrdIndexNo VARCHAR(5) PRIMARY KEY
    ,OrdDay DATE NOT NULL
    ,CliCode VARCHAR(5) NOT NULL references ClientMaster(CliCode)
    ,EmpCode VARCHAR(5) NOT NULL references EmployeeMaster(EmpCode)
    ,OrdIndAdhibition VARCHAR(400) NOT NULL
    ,OIStatus VARCHAR(3) NOT NULL references PayMaster(PayCode)
  );

CREATE
  TABLE SalesDetails(
    SalHeadNo VARCHAR(5) NOT NULL references SalesHeadline(SalHeadNo)
    ,SalRowNo SMALLINT NOT NULL
    ,SalProceeds INTEGER NOT NULL
    ,SalesSourceNo VARCHAR(5) NOT NULL references ShipMentDetails(SalesSourceNo)
    ,CONSTRAINT SalesDetailsKEY_NAME PRIMARY KEY (
      SalHeadNo
      ,SalRowNo
    )
  );
CREATE
  TABLE SalesHeadline(
    SalHeadNo VARCHAR(5) PRIMARY KEY
    ,SalDivision VARCHAR(5) NOT NULL
    ,SalCustomerCode VARCHAR(5) NOT NULL references ClientMaster(CliCode)
    ,SalDay DATE NOT NULL
  );
CREATE
  TABLE ShipDestinationMaster(
    ShipDesCode VARCHAR(5) PRIMARY KEY references CustomerMaster(CusShipDivision)
    ,ShipReceivingCheckPerson VARCHAR(20) NOT NULL
  );
CREATE
  TABLE ShipmentDetails(
    OrdIndexNo VARCHAR(5) NOT NULL references OrderIndex(OrdIndexNo)
    ,OrdDetRowNo VARCHAR(5) NOT NULL references OrderDetails(OrddetRowNo)
    ,ShipInstallmentsNo VARCHAR(5) NOT NULL
    ,ShipHeadNo VARCHAR(5) NOT NULL references ShipmentHeadline(ShipHeadNo)
    ,ShipScheduledDay DATE NOT NULL
    ,ShipScheduleAmount INTEGER NOT NULL
    ,ShipNumberOfAmount INTEGER NOT NULL
    ,ShipScheduleWareHouseCode VARCHAR(5) NOT NULL references WareHouseMaster(WarHouseCode)
    ,ShipNumberOfWareHouseCode VARCHAR(5) NOT NULL references WareHouseMaster(WarHouseCode)
    ,SalesSourceNo VARCHAR(5) NOT NULL
    ,ShipStatus VARCHAR(3) NOT NULL references PayMaster(PayCode)
    ,CONSTRAINT SalesDetailsKEY_NAME PRIMARY KEY (
      OrdIndexNo
      ,OrdDetRowNo
      ,ShipInstallmentsNo
    )
  );
CREATE
  TABLE ShipmentHeadline(
    ShipHeadNo VARCHAR(5) PRIMARY KEY
    ,ShipDay DATE NOT NULL
    ,CarCode VARCHAR(5) NOT NULL references CarrierMaster(CarCode)
    ,ShipHeadInquiry VARCHAR(5) NOT NULL
    ,WarHouseCode VARCHAR(5) NOT NULL references WareHouseMaster(WarHouseCode)
    ,ShipDesCode VARCHAR(5) NOT NULL references ShipDestinationMaster(ShipDesCode)
  );
CREATE
  TABLE StockAccordingWarehouse(
    GoodsCode VARCHAR(5) NOT NULL references GoodsMaster(GoodsCode)
    ,WareHouseCode VARCHAR(5) NOT NULL references WarehouseMaster(WarHouseCode)
    ,NumberOfPeriodNeckStocks INTEGER NOT NULL
    ,PresentInventoryGoods INTEGER NOT NULL
    ,NumberThatHasBeenDrawn INTEGER NOT NULL
    ,CONSTRAINT StockAccordingWarehouseKEY_NAME PRIMARY KEY (
      WareHouseCode
      ,GoodsCode
    )
  );
CREATE
  TABLE StockTransitionAccordingToDay(
    GoodsCode VARCHAR(5) NOT NULL references GoodsMaster(GoodsCode)
    ,STBaseDate DATE NOT NULL
    ,STNumberOfShipmentScheduleTotals INTEGER NOT NULL
    ,STNumberOfScheduleOfArrivalOfGoodsTotals INTEGER NOT NULL
    ,CONSTRAINT StockAccordingWarehouseKEY_NAME PRIMARY KEY (
      STBaseDate
      ,GoodsCode
    )
  );

